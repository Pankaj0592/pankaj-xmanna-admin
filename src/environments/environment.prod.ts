// import { Environment } from '@delon/theme';


import { DelonMockModule } from '@delon/mock';
import { Environment } from '@delon/theme';
import * as MOCKDATA from '../../_mock';

export const environment = {
  production: true,
  useHash: true,
  api: {
    baseUrl: './',
    refreshTokenEnabled: true,
    refreshTokenType: 'auth-refresh',
  },
  modules: [DelonMockModule.forRoot({ data: MOCKDATA })],
} as Environment;

export const environments = {
  production: true,
  base_url : 'https://la-user.xmanna.com/',
  login_url : 'https://la-superadmin.xmanna.com/adminActions/',
  game_url : 'https://la-game.xmanna.com/',
  play_option_url: 'https://la-playoptions.xmanna.com/',
  challenge_mq_url: 'https://la-challenge-mq.xmanna.com/',
  deposit_promotions: 'https://la-ledger.xmanna.com/',
  la_support: 'https://la-support.xmanna.com/',
  la_ledger_url: 'https://la-ledger.xmanna.com/',
  location: 'https://sandbox.spendsdk.com/api/location/',
  countries: 'https://la-countries.xmanna.com/',
  multiplayer: 'https://la-multiplayer.xmanna.com/',
  multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
  avatarURL : 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  solitaire_url: 'https://la-solitaire.xmanna.com/',
  leaderboard_url: 'https://la-leaderboard.xmanna.com/',
  challenge_mq_blockChain_url: 'https://xmanna-blockchain-api.herokuapp.com/admin/getTransactionDetails/',
  useHash: true,
};
