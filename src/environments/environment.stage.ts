// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { DelonMockModule } from '@delon/mock';
import { Environment } from '@delon/theme';
import * as MOCKDATA from '../../_mock';

export const environment = {
  production: false,
  useHash: true,
  api: {
    baseUrl: './',
    refreshTokenEnabled: true,
    refreshTokenType: 'auth-refresh',
  },
  modules: [DelonMockModule.forRoot({ data: MOCKDATA })],
} as Environment;

export const environments = {
    production: false,
    login_url : 'https://la-superadmin-stage.xmanna.com/',
    base_url : 'https://la-user-stage.xmanna.com/',
    game_url : 'https://la-game-stage.xmanna.com/',
    play_option_url : 'https://la-playoptions-stage.xmanna.com/',
    challenge_mq_url: 'https://la-challenge-mq-stage.xmanna.com/',
    deposit_promotions: 'https://la-ledger-stage.xmanna.com/',
    la_support: 'https://la-support-stage.xmanna.com/',
    la_ledger_url: 'https://la-ledger-stage.xmanna.com/',
    location: 'https://sandbox.spendsdk.com/api/location/',
    countries: 'https://la-countries-stage.xmanna.com/',
    multiplayer: 'https://la-multiplayer-stage.xmanna.com/',
    multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
    avatarURL : 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    solitaire_url: 'https://la-solitaire-stage.xmanna.com/',
    leaderboard_url: 'https://la-leaderboard-stage.xmanna.com/',
    challenge_mq_blockChain_url: 'https://blockchain-dev.xmanna.com/admin/getTransactionDetails/',
    };