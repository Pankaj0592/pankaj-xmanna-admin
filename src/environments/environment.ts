// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { DelonMockModule } from '@delon/mock';
import { Environment } from '@delon/theme';
import * as MOCKDATA from '../../_mock';

export const environment = {
  production: false,
  useHash: true,
  api: {
    baseUrl: './',
    refreshTokenEnabled: true,
    refreshTokenType: 'auth-refresh',
  },
  modules: [DelonMockModule.forRoot({ data: MOCKDATA })],
} as Environment;


export const environments = {
  production: false,
  login_url : 'https://la-superadmin-dev.xmanna.com/',
  base_url : 'https://la-user-dev.xmanna.com/',
  game_url : 'https://la-game-dev.xmanna.com/',
  play_option_url : 'https://la-playoptions-dev.xmanna.com/',
  challenge_mq_url: 'https://la-challenge-mq-dev.xmanna.com/',
  la_support: 'https://la-support-dev.xmanna.com/',
  la_ledger_url: 'https://la-ledger-dev.xmanna.com/',
  deposit_promotions: 'https://la-ledger-dev.xmanna.com/',
  location: 'https://sandbox.spendsdk.com/api/location/',
  countries: 'https://la-countries-dev.xmanna.com/',
  multiplayer: 'https://la-multiplayer-dev.xmanna.com/',
  multiplayer_api_key: 'Ww0VmEvaSH0E3aGzHGSCdqu_SWtNhEsG',
  avatarURL : 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  solitaire_url: 'https://la-solitaire-dev.xmanna.com/',
  leaderboard_url: 'https://la-leaderboard-dev.xmanna.com/',
  challenge_mq_blockChain_url: 'https://xmanna-blockchain-api.herokuapp.com/admin/getTransactionDetails/',
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
