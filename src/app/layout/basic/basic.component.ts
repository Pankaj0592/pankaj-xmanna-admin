import { Component } from "@angular/core";
import { SettingsService, User } from "@delon/theme";
import { LayoutDefaultOptions } from "@delon/theme/layout-default";
import { environment } from "@env/environment";

@Component({
  selector: "layout-basic",
  template: `
    <layout-default
      [options]="options"
      [asideUser]="asideUserTpl"
      [content]="contentTpl"
    >
      <layout-default-header-item direction="left">
      </layout-default-header-item>

      <layout-default-header-item direction="middle">
        <header-search
          class="alain-default__search"
          [(toggleChange)]="searchToggleStatus"
        >
          <i nz-icon nzType="search"></i>
        </header-search>
      </layout-default-header-item>

      <layout-default-header-item direction="right">
        <header-user><i nz-icon nzType="search"></i></header-user>
      </layout-default-header-item>

      <ng-template #asideUserTpl>
        <nz-dropdown-menu #userMenu="nzDropdownMenu">
          <ul nz-menu>
            <li nz-menu-item routerLink="/pro/account/center">
              {{ "menu.account.center" | translate }}
            </li>
            <li nz-menu-item routerLink="/pro/account/settings">
              {{ "menu.account.settings" | translate }}
            </li>
          </ul>
        </nz-dropdown-menu>
      </ng-template>
      <ng-template #contentTpl>
        <router-outlet></router-outlet>
      </ng-template>
    </layout-default>
  `,


  // <setting-drawer *ngIf="showSettingDrawer"></setting-drawer>
  // <theme-btn></theme-btn>
})
export class LayoutBasicComponent {
  options: LayoutDefaultOptions = {
    logoExpanded: `./assets/logo-full.png`,
    logoCollapsed: `./assets/logo.png`,
  };
  searchToggleStatus = false;
  showSettingDrawer = !environment.production;
  get user(): User {
    return this.settings.user;
  }

  constructor(private settings: SettingsService) {}
}
