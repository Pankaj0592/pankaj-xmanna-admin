import { HttpEvent, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environments } from '@env/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';






@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorService {

    constructor(private router: Router, private msg: NzMessageService, private notify: NzNotificationService) { }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        const token = localStorage.getItem('auth_app_token');

        req = req.clone({
            setHeaders: {
                Authorization: `${token}`,
            }
        });

        return next.handle(req).pipe(

            tap((evt: any) => {
                if (evt instanceof HttpResponse) {
                    console.log('debug the request was:', req);
                    console.log('---> response:', evt);
                    if (req.url !== environments.avatarURL && req.method !== 'GET' && evt.body.responseStatus.success) {
                        if (req.method === 'POST' && !req.body.page) {
                            // this.msg.success('Added successfully');
                        } else if (req.method === 'PUT') {
                            this.msg.success('Updated successfully');
                        } else if (req.method === 'DELETE') {
                            this.msg.success('Deleted successfully');
                        }
                    }
                }
            }),

            catchError((err: HttpErrorResponse) => {
                console.log('error notifications', err);
                this.notify.error(err.status.toString(), err.error.responseStatus.errorMessage);
                if (err.error.responseStatus.errorMessage === 'jwt expired') {
                    setTimeout(() => {
                        localStorage.removeItem('token');
                        this.router.navigateByUrl('');
                    }, 2000);
                }
                throw err;
            }),
        );
    }
}
