import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environments } from '@env/environment';

interface Transaction {
  _id: string;
  amount: string;
  currency: string;
  method: string;
  status: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class TransactionsService {

  constructor(private http: HttpClient) { }

  getTransactions(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: Transaction[] }> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    filters.forEach(filter => {
      filter.value.forEach(value => {
        params = params.append(filter.key, value);
      });
    });
    return this.http.post<{ results: Transaction[] }>(`${environments.la_ledger_url + 'admin/payouts/filter'}`, { page: pageIndex, perPage: pageSize });
  }

  public filterTransaction(filterObj: any): Observable<{ results: Transaction[] }> {
    return this.http.post<{ results: Transaction[] }>(`${environments.la_ledger_url + 'admin/payouts/filter'}`, filterObj);

  }

}