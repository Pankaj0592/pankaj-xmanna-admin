import { HttpClient, HttpParams, HttpBackend } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { environments } from '@env/environment';
import { Observable, Subject } from 'rxjs';

interface Countries {
  _id: string;
  name: string;
  iso2: string;
  iso3: string;
  updatedAt: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class CountriesService {

  private subject = new Subject<any>();

  constructor(private http: HttpClient) { }

  // getTransactions(
  //   pageIndex: number,
  //   pageSize: number,
  //   sortField: string | null,
  //   sortOrder: string | null,
  //   filters: Array<{ key: string; value: string[] }>
  // ): Observable<{ results: Transaction[] }> {
  //   let params = new HttpParams()
  //     .append('page', `${pageIndex}`)
  //     .append('results', `${pageSize}`)
  //     .append('sortField', `${sortField}`)
  //     .append('sortOrder', `${sortOrder}`);
  //   filters.forEach(filter => {
  //     filter.value.forEach(value => {
  //       params = params.append(filter.key, value);
  //     });
  //   });
  //   return this.http.post<{ results: Transaction[] }>(`${environments.la_ledger_url  +  'admin/payouts/filter'}`, {page: pageIndex, perPage: pageSize});
  // }

  // public filterTransaction(filterObj: any): Observable<{ results: Transaction[] }>{
  //   return this.http.post<{ results: Transaction[] }>(`${environments.la_ledger_url  +  'admin/payouts/filter'}`, filterObj);

  // }

  public getAllCountries(): Observable<any> {
    const allCountries = this.http.get(environments.location + 'countries');
    return allCountries;
  }

  public getStateById(id: any): Observable<any> {
    const allStates = this.http.get(environments.location + 'states/' + id);
    return allStates;
  }
  public getCountries(): Observable<any> {
    const allCountries = this.http.post(environments.countries + 'admin/countries/all', {});
    return allCountries;
  }
  public addCountry(obj: any): Observable<any> {
    const allCountries = this.http.post(environments.countries + 'admin/countries', obj);
    return allCountries;
  }

  public fetchCountryWithStates(id: any): Observable<any> {
    const countryState = this.http.get(environments.countries + 'admin/countries/' + id + '/states')
    return countryState;
  }

  public updateCountry(id: any, obj: any): Observable<any> {
    const updatedCountry = this.http.put(environments.countries + 'admin/countries/' + id, obj)
    return updatedCountry;
  }
  public updateState(countryId: any, stateId: any, obj: any): Observable<any> {
    const updatedCountry = this.http.put(environments.countries + 'admin/countries/' + countryId + '/states/' + stateId, obj)
    return updatedCountry;
  }
  public deleteCountry(id: any): Observable<any> {
    const country = this.http.delete(environments.countries + 'admin/countries/' + id)
    return country;
  }

  public deleteState(countryId: string, stateId: string): Observable<any> {
    const state = this.http.delete(environments.countries + 'admin/countries/' + countryId + '/states/' + stateId)
    return state;
  }

  public addDepositOptions(): any {
    this.subject.next(true);
  }

  public getDepositOptions(): any {
    return this.subject.asObservable();
  }

}