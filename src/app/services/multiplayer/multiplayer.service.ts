import { HttpClient, HttpParams, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GamesManagementService } from '../games-management/games-management.service';
import { environments } from '@env/environment';

interface Multiplayer {
  _id: string;
  name: string;
  logo: string;
  game: string;
  status: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class MultiplayerService {

  constructor(private http: HttpClient, private httpBackend: HttpBackend) { }

  headers = new HttpHeaders({
    'X-Api-Key': environments.multiplayer_api_key
  });

  challengeAllList(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: any
  ): Observable<{ results: Multiplayer[] }> {
    const newHttpClient = new HttpClient(this.httpBackend);
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    return newHttpClient.post<{ results: Multiplayer[] }>(`${environments.multiplayer + 'admin/listGames'}`, { page: pageIndex, resPerPage: pageSize, filter: filters }, { headers: this.headers });
  }

  public filterMultiplayer(filterObj: any): Observable<{ results: Multiplayer[] }> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post<{ results: Multiplayer[] }>(`${environments.multiplayer + 'admin/listGames'}`, filterObj, { headers: this.headers });
  }

  public updateChallengeStatus(updatedObj: any): Observable<{ results: Multiplayer[] }> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post<{ results: Multiplayer[] }>(`${environments.multiplayer + 'admin/listGames'}`, updatedObj);
  }

  public transactionDetails(id: any): Observable<{ results: Multiplayer[] }> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post<{ results: Multiplayer[] }>(`${environments.multiplayer + 'transaction/'}` + id, {});
  }

}