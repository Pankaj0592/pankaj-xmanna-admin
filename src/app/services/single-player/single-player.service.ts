import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environments } from '@env/environment';

interface Challenge {
  _id: string;
  name: string;
  logo: string;
  game: string;
  status: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class SinglePlayerService {

  constructor(private http: HttpClient) { }

  challengeAllList(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    duration: string | '24h',
    // filter: any,
  ): Observable<{ results: Challenge[] }> {
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`)
      .append('duration', `${duration}`);
    return this.http.post<{ results: Challenge[] }>(`${environments.challenge_mq_url + 'challengeAdmin/all'}`, { page: pageIndex, resPerPage: pageSize, duration: duration });
  }

  public filterChallenge(filterObj: any): Observable<{ results: Challenge[] }> {
    return this.http.post<{ results: Challenge[] }>(`${environments.challenge_mq_url + 'challengeAdmin/filter'}`, filterObj);

  }

  public updateChallengeStatus(updatedObj: any): Observable<{ results: Challenge[] }> {
    return this.http.post<{ results: Challenge[] }>(`${environments.challenge_mq_url + 'challengeAdmin/update'}`, updatedObj);

  }

  public transactionDetails(id: any): Observable<{ results: Challenge[] }> {
    return this.http.post<{ results: Challenge[] }>(`${environments.challenge_mq_url + 'transaction/'}` + id, {});
  }

}