import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { environment, environments } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';

interface RandomUser {
  _id: string;
  loginType: string;
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Injectable({ providedIn: 'root' })
export class RandomUserService {

  constructor(private http: HttpClient) { }
  /** use for pass data in securityStatus change modal */
  userIdObj = new BehaviorSubject('');

  getUsers(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/all'}`, { page: pageIndex, resPerPage: pageSize, sortBy: sortField });
  }

  public filterUser(filterObj: any): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/findUsers'}`, filterObj);
  }

  public updateSecurityStatus(obj: any): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/updateSecurityStatus'}`, obj);
  }

  getUserId(): object {
    return this.userIdObj;
  }

  setUserId(id: any): void {
    this.userIdObj.next(id);
  }

}