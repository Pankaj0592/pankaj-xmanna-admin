import { TestBed } from '@angular/core/testing';

import { RestrictedLocationService } from './restricted-location.service';

describe('RestrictedLocationService', () => {
  let service: RestrictedLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestrictedLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
