import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environments } from '@env/environment';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RestrictedLocationService {

  constructor(public http: HttpClient) { }


  public allRestrictedLocation(): Observable<any> {
    const restrictedList = this.http.get(environments.base_url + 'restrictedLocationsAdmin');
    return restrictedList;
  }

  public addRestrictedLocation(obj: any): Observable<any> {
    const restrictedLocation = this.http.post(environments.base_url + 'restrictedLocationsAdmin', obj);
    return restrictedLocation;
  }

  public updateRestrictedLocation(id: string, obj: any): Observable<any> {
    const updateRestrictedLocation = this.http.put(environments.base_url + 'restrictedLocationsAdmin/' + id, obj);
    return updateRestrictedLocation;
  }

  public deleteRestrictedLocation(id: string): Observable<any> {
    const deleteRestrictedLocation = this.http.delete(environments.base_url + 'restrictedLocationsAdmin/' + id);
    return deleteRestrictedLocation;
  }

}