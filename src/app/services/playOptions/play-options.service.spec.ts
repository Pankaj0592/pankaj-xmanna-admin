import { TestBed } from '@angular/core/testing';

import { PlayOptionsService } from './play-options.service';

describe('PlayOptionsService', () => {
  let service: PlayOptionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayOptionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
