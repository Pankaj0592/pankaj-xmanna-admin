import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environments } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayOptionsService {

  constructor(public http: HttpClient) { }

  public getAllPlayOptions(pageObj: any): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/all', pageObj);
    return list;
  }

  public getAllPlayOptionsAsList(obj: any): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/filterByGameType', obj);

    return list;
  }

  //  playOptions list through gameId
  public playOptionsViaGameId(obj: any): Observable<any> {
    return this.http.post(environments.play_option_url + 'playOptionsAdmin/filter/', obj);
  }

  public createPlayOptions(obj: any): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/create', obj);
    return list;
  }

  public updateAllDetails(id: any, obj: any): Observable<any> {
    const list = this.http.put(environments.play_option_url + 'usersAdmin/' + id, obj);
    return list;
  }


  public editLogo(formData: FormData): Observable<any> {
    const list = this.http.put(environments.play_option_url + 'playOptionsAdmin/editLogo', formData);
    return list;
  }

  public editPlayOptions(id: any, obj: any): Observable<any> {
    const list = this.http.put(environments.play_option_url + 'playOptionsAdmin/edit/' + id, obj);
    return list;
  }

  public deletePlayOptions(id: any): Observable<any> {
    const list = this.http.delete(environments.play_option_url + 'playOptionsAdmin/delete/' + id);
    return list;
  }

  public playOptionsFilter(filter: any): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/filter', filter);
    return list;
  }

  public isBlockChainRegister(id: any, obj: any): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/getBCPlayOptions/' + id, obj);
    return list;
  }

  public getAllPlayOptionsWithoutPage(): Observable<any> {
    const list = this.http.post(environments.play_option_url + 'playOptionsAdmin/all', {});
    return list;
  }

}
