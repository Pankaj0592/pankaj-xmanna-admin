import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { environment, environments } from '@env/environment';
import { BehaviorSubject, Observable } from 'rxjs';

interface RandomUser {
  _id: string;
  loginType: string;
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Injectable({ providedIn: 'root' })
export class UserManagementService {

  constructor(private http: HttpClient) { }


  getUsers(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/all'}`, { page: pageIndex, resPerPage: pageSize, sortBy: sortField });
  }

  public filterUser(filterObj: any): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/findUsers'}`, filterObj);
  }

  public updateSecurityStatus(obj: any): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/updateSecurityStatus'}`, obj);
  }
  
  public getUserToken(obj: any): Observable<{ results: RandomUser[] }> {
    return this.http.post<{ results: RandomUser[] }>(`${environments.base_url + 'usersAdmin/generateToken'}`, obj);
  }
}

// {
//   "text": "Pro",
//   "i18n": "menu.pro",
//   "group": true,
//   "hideInBreadcrumb": true,
//   "children": [
//     {
//       "text": "Form Page",
//       "i18n": "menu.form",
//       "link": "/pro/form",
//       "icon": "anticon-edit",
//       "children": [
//         {
//           "text": "Basic Form",
//           "link": "/pro/form/basic-form",
//           "i18n": "menu.form.basicform",
//           "shortcut": true
//         },
//         {
//           "text": "Step Form",
//           "link": "/pro/form/step-form",
//           "i18n": "menu.form.stepform"
//         },
//         {
//           "text": "Advanced Form",
//           "link": "/pro/form/advanced-form",
//           "i18n": "menu.form.advancedform"
//         }
//       ]
//     },
//     {
//       "text": "List",
//       "i18n": "menu.list",
//       "icon": "anticon-appstore",
//       "children": [
//         {
//           "text": "Table List",
//           "link": "/pro/list/table-list",
//           "i18n": "menu.list.searchtable",
//           "shortcut": true
//         },
//         {
//           "text": "Basic List",
//           "link": "/pro/list/basic-list",
//           "i18n": "menu.list.basiclist"
//         },
//         {
//           "text": "Card List",
//           "link": "/pro/list/card-list",
//           "i18n": "menu.list.cardlist"
//         },
//         {
//           "text": "Search List",
//           "i18n": "menu.list.searchlist",
//           "children": [
//             {
//               "link": "/pro/list/articles",
//               "i18n": "menu.list.searchlist.articles"
//             },
//             {
//               "link": "/pro/list/projects",
//               "i18n": "menu.list.searchlist.projects",
//               "shortcut": true
//             },
//             {
//               "link": "/pro/list/applications",
//               "i18n": "menu.list.searchlist.applications"
//             }
//           ]
//         }
//       ]
//     },
//     {
//       "text": "Profile",
//       "i18n": "menu.profile",
//       "icon": "anticon-profile",
//       "children": [
//         {
//           "text": "Basic",
//           "link": "/pro/profile/basic",
//           "i18n": "menu.profile.basic"
//         },
//         {
//           "text": "Advanced",
//           "link": "/pro/profile/advanced",
//           "i18n": "menu.profile.advanced",
//           "shortcut": true
//         }
//       ]
//     },
//     {
//       "text": "Result",
//       "i18n": "menu.result",
//       "icon": "anticon-check-circle",
//       "children": [
//         {
//           "text": "Success",
//           "link": "/pro/result/success",
//           "i18n": "menu.result.success"
//         },
//         {
//           "text": "Fail",
//           "link": "/pro/result/fail",
//           "i18n": "menu.result.fail"
//         }
//       ]
//     },
//     {
//       "text": "Exception",
//       "i18n": "menu.exception",
//       "link": "/",
//       "icon": "anticon-exception",
//       "children": [
//         {
//           "text": "403",
//           "link": "/exception/403",
//           "i18n": "menu.exception.not-permission",
//           "reuse": false
//         },
//         {
//           "text": "404",
//           "link": "/exception/404",
//           "i18n": "menu.exception.not-find",
//           "reuse": false
//         },
//         {
//           "text": "500",
//           "link": "/exception/500",
//           "i18n": "menu.exception.server-error",
//           "reuse": false
//         }
//       ]
//     },
//     {
//       "text": "Account",
//       "i18n": "menu.account",
//       "icon": "anticon-user",
//       "children": [
//         {
//           "text": "center",
//           "link": "/pro/account/center",
//           "i18n": "menu.account.center"
//         },
//         {
//           "text": "settings",
//           "link": "/pro/account/settings",
//           "i18n": "menu.account.settings"
//         }
//       ]
//     }
//   ]
// }