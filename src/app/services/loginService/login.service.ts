import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend } from '@angular/common/http';
import { environments } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private httpBackend: HttpBackend) { }

  public login(loginFormObj: any): Observable<any> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post(`${environments.login_url + 'adminActions/login'}`, loginFormObj);
  }

  public validateSecretKey(secretKeyObj: any): Observable<any> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post(`${environments.login_url + 'adminActions/validateSecret'}`, secretKeyObj);
  }

  public resetPassword(resetPasswordObj: any): Observable<any> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post(`${environments.login_url + 'adminActions/resetPassword'}`, resetPasswordObj);
  }

  public forgotPassword(forgotPasswordObj: any): Observable<any> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post(`${environments.login_url + 'adminActions/forgotPassword'}`, forgotPasswordObj);
  }

  public validateLogin(loginFormObj: any): Observable<any> {
    const newHttpClient = new HttpClient(this.httpBackend);
    return newHttpClient.post(`${environments.login_url + 'adminActions/validateCreds'}`, loginFormObj);
  }

}
