import { HttpClient, HttpHeaders, HttpParams, HttpBackend } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { environment, environments } from '@env/environment';
import { Observable } from 'rxjs';

interface Leaderboard {
  _id: string;
  name: string;
  logo: string;
  genre: string;
  status: string;
  playerType: string;
  daily: number;
  dailyPrev: number;
  total: number;
}

@Injectable({ providedIn: 'root' })
export class LeaderboardService {

  constructor(private http: HttpClient, private httpBackend: HttpBackend) { }

  getLeaderBoard(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: any
  ): Observable<{ results: Leaderboard[] }> {
    const params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    return this.http.post<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/all'}`, { page: pageIndex, resPerPage: pageSize, sort: sortField, filter: filter });
  }

  public filterLeaderboard(filterObj: any): Observable<{ results: Leaderboard[] }> {
    return this.http.post<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/all'}`, filterObj);

  }

  public addLeaderboard(formObj: any): Observable<{ results: Leaderboard[] }> {
    return this.http.post<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/create'}`, formObj);
  }


  public editLeaderboard(id: string, obj: any): Observable<{ results: Leaderboard[] }> {
    return this.http.put<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/'}` + id, obj);
  }

  public deleteLeaderboard(id: string): Observable<{ results: Leaderboard[] }> {
    return this.http.delete<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/delete/'}` + id);
  }

  public allLeaderboard(): Observable<{ results: Leaderboard[] }> {
    return this.http.post<{ results: Leaderboard[] }>(`${environments.leaderboard_url + 'leaderBoardAdmin/all'}`, {});
  }
  // tslint:disable-next-line: eofline
}