import { HttpClient, HttpHeaders, HttpParams, HttpBackend } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { environment, environments } from '@env/environment';
import { Observable } from 'rxjs';

interface Game {
  _id: string;
  name: string;
  logo: string;
  genre: string;
  status: string;
  playerType: string;
  daily: number;
  dailyPrev: number;
  total: number;
}

@Injectable({ providedIn: 'root' })
export class GamesManagementService {

  constructor(private http: HttpClient, private httpBackend: HttpBackend) { }

  getGames(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: any
  ): Observable<{ results: Game[] }> {
    const params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    return this.http.post<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/all'}`, { page: pageIndex, resPerPage: pageSize, sort: sortField, filter: filter });
  }

  public filterGame(filterObj: any): Observable<{ results: Game[] }> {
    return this.http.post<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/all'}`, filterObj);

  }

  public addGames(formData: FormData): Observable<{ results: Game[] }> {
    const newHttpClient = new HttpClient(this.httpBackend);
    const headers = new HttpHeaders();
    headers.append('content-type', 'multipart/form-data');
    headers.append('sec-fetch-site', 'same-site');
    return newHttpClient.post<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/create'}`, formData, { headers: headers });
  }


  public editGames(id: number, obj: any): Observable<{ results: Game[] }> {
    return this.http.put<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/edit/'}` + id, obj);
  }

  public allGames(): Observable<{ results: Game[] }> {
    return this.http.post<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/all'}`, {});
  }

  public getAllGamesAsList(): Observable<{ results: Game[] }> {
    return this.http.get<{ results: Game[] }>(`${environments.game_url + 'gamesAdmin/all'}`);
  }
  // tslint:disable-next-line: eofline
}