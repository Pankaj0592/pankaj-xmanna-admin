import { HttpClient, HttpParams, HttpHeaders, HttpBackend } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GamesManagementService } from '../games-management/games-management.service';
import { environments } from '@env/environment';

interface Bets {
  _id: string;
  name: string;
  playOption: string;
  playOptions: any;
  logo: string;
  entryFee: string;
  bets: string;
  status: string;
  createdBy: string;
  updatedAt: string;
  createdAt: string;
}

@Injectable({ providedIn: 'root' })
export class BetsService {

  constructor(private http: HttpClient, private httpBackend: HttpBackend) { }

  headers = new HttpHeaders({
    'X-Api-Key': environments.multiplayer_api_key
  });

  public betsAllList(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filters: Array<{ key: string; value: string[] }>
  ): Observable<{ results: Bets[] }> {

    const newHttpClient = new HttpClient(this.httpBackend);
    let params = new HttpParams()
      .append('page', `${pageIndex}`)
      .append('results', `${pageSize}`)
      .append('sortField', `${sortField}`)
      .append('sortOrder', `${sortOrder}`);
    filters.forEach(filter => {
      filter.value.forEach(value => {
        params = params.append(filter.key, value);
      });
    });
    return newHttpClient.post<{ results: Bets[] }>(`${environments.multiplayer + 'admin/listBets'}`, { page: pageIndex, resPerPage: pageSize }, { headers: this.headers });
  }


  public filterBets(filterObj: any): Observable<{ results: Bets[] }> {
    return this.http.post<{ results: Bets[] }>(`${environments.multiplayer + 'admin/listBets'}`, filterObj, { headers: this.headers });

  }

}