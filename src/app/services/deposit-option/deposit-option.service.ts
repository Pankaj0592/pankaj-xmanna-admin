import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environments } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepositOptionsService {


  constructor(public http: HttpClient) { }


  public allDepositOptions(obj: any): Observable<any> {
    let params = new HttpParams();
    params = params.append('page', obj.page);
    params = params.append('perPage', obj.perPage);
    const depositOptionsList = this.http.get(environments.deposit_promotions + 'admin/depositOptions', { params: params });
    return depositOptionsList;
  }

  public addDepositOptions(obj: any): Observable<any> {
    const depositOptions = this.http.post(environments.deposit_promotions + 'admin/depositOptions/createMany', obj);
    return depositOptions;
  }

  public filterDepositOptions(obj: any): Observable<any> {
    let params = new HttpParams();
    params = params.append('country', obj.country);
    if (obj.state) {
      params = params.append('state', obj.state);
    }
    const depositOptionsList = this.http.get(environments.deposit_promotions + 'admin/depositOptions', { params: params });
    console.log('depositOptionsList', depositOptionsList);
    return depositOptionsList;
  }

  public updateDepositOptions(id: string, obj: any): Observable<any> {
    const updatedDepositOption = this.http.put(environments.deposit_promotions + 'admin/depositOptions/updateMany' + id, obj);
    return updatedDepositOption;
  }

  public deleteDepositOptions(id: any): Observable<any> {
    const deleteDepositOptions = this.http.delete(environments.deposit_promotions + 'admin/depositOptions/' + id);
    return deleteDepositOptions;
  }

  public bulkDeleteDepositOptions(id: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      body: { id }
    };
    const deleteDepositOptions = this.http.delete(environments.deposit_promotions + 'admin/depositOptions/deleteMany', options);
    return deleteDepositOptions;
  }

  public getAllCountries(): Observable<any> {
    const allCountries = this.http.get(environments.location + 'countries');
    return allCountries;
  }

  public getStateById(id: string): Observable<any> {
    const allStates = this.http.get(environments.location + 'states/' + id);
    return allStates;
  }

  public getCountries(): Observable<any> {
    const allCountries = this.http.get(environments.countries + 'admin/countries');
    return allCountries;
  }

  public addCountry(obj: any): Observable<any> {
    const allCountries = this.http.post(environments.countries + 'admin/countries', obj);
    return allCountries;
  }

  public fetchCountryWithStates(id: string): Observable<any> {
    const countryState = this.http.get(environments.countries + 'admin/countries/' + id + '/states');
    return countryState;
  }

  public updateCountry(id: string, obj: any): Observable<any> {
    const updatedCountry = this.http.put(environments.countries + 'admin/countries/' + id, obj);
    return updatedCountry;
  }

  public updateState(countryId: string, stateId: string, obj: any): Observable<any> {
    const updatedCountry = this.http.put(environments.countries + 'admin/countries/' + countryId + '/states/' + stateId, obj);
    return updatedCountry;
  }

  public deconsteCountry(id: string): Observable<any> {
    const country = this.http.delete(environments.countries + 'admin/countries/' + id);
    return country;
  }

  public deleteState(countryId: string, stateId: string): Observable<any> {
    const state = this.http.delete(environments.countries + 'admin/countries/' + countryId + '/states/' + stateId);
    return state;
  }

}

