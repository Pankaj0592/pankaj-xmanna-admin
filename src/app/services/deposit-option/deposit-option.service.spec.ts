import { TestBed } from '@angular/core/testing';

import { DepositOptionService } from './deposit-option.service';

describe('DepositOptionService', () => {
  let service: DepositOptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DepositOptionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
