import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { SettingsRoutingModule } from './settings-routing.module';
import { AddCountriesComponent } from './settings/add-countries/add-countries.component';
import { AddDepositOptionComponent } from './settings/add-deposit-option/add-deposit-option.component';
import { AddLocationProhibitionComponent } from './settings/add-location-prohibition/add-location-prohibition.component';
import { AddStatesComponent } from './settings/add-states/add-states.component';
import { CountriesDrawerComponent } from './settings/countries-drawer/countries-drawer.component';
import { DepositOptionDrawerComponent } from './settings/deposit-option-drawer/deposit-option-drawer.component';
import { DepositOptionsComponent } from './settings/deposit-options/deposit-options.component';
import { LocationProhibitionDrawerComponent } from './settings/location-prohibition-drawer/location-prohibition-drawer.component';
import { LocationProhibitionComponent } from './settings/location-prohibition/location-prohibition.component';
import { SettingsComponent } from './settings/settings.component';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    SettingsRoutingModule,
  ],
  declarations: [COMPONENTS, SettingsComponent, AddCountriesComponent, CountriesDrawerComponent, AddStatesComponent, DepositOptionsComponent, AddDepositOptionComponent, DepositOptionDrawerComponent, LocationProhibitionComponent, LocationProhibitionDrawerComponent, AddLocationProhibitionComponent ],
  entryComponents: [ AddCountriesComponent, CountriesDrawerComponent, AddStatesComponent, DepositOptionsComponent, AddDepositOptionComponent],
  providers: [ LocationProhibitionComponent, DepositOptionsComponent ]
})
export class SettingsModule { }
