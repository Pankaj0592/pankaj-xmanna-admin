import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { AddDepositOptionComponent } from '../add-deposit-option/add-deposit-option.component';
import { DepositOptionsService } from 'src/app/services/deposit-option/deposit-option.service';
import { DepositOptionDrawerComponent } from '../deposit-option-drawer/deposit-option-drawer.component';
import { NzBytesPipe } from 'ng-zorro-antd/pipes/public-api';



interface DepositOption {
  _id: string;
  state: string;
  country: string;
  deposits: string;
  updatedAt: string;
}
interface Countries {
  _id: string;
  name: string;
  iso2: string;
  iso3: string;
  updatedAt: string;
  createdAt: string;
}

@Component({
  selector: 'app-deposit-options',
  templateUrl: './deposit-options.component.html',
  styleUrls: ['./deposit-options.component.less']
})
export class DepositOptionsComponent implements OnInit {

  validateForm!: FormGroup;
  paginationToggle = true;
  playOptionsData: any;
  gamesData: any;
  q: any = {
    country: '',
    state: '',
    countryId: '',
    stateId: '',
    page: 1,
    perPage: 30
  };
  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  selectedTab: any = 'Countries';
  selectedIndex: number = 0;
  btnSize = '24 hours';
  countriesDefaultValue = "2,5,10,20,50";


  sortByFilter = [
    { index: 0, label: 'Country: A to Z', value: '', type: 'default', checked: false },
    { index: 1, label: 'Country: Z to A', value: 'oldestFirst', type: 'default', checked: false },
  ];

  tabs = [ 'Countries', 'Prohibition', 'Deposit Options' ];

  expandForm = false;
  size: any = '"default"';
  listOfDepositOption: DepositOption[] = [ ];
  listOfCountries: Countries[] = [ ];
  total = 0;

  pageSize = 50;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedSortBy = null;

  constructor(private countryService: CountriesService, private depositOptionService: DepositOptionsService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper) {}

  ngOnInit(): void {
    this.loading = false;
    this.getAllCountries();
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    const pageObj = {'page': this.pageIndex, 'perPage': this.pageSize};
    this.depositOptionService.allDepositOptions(pageObj).subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.total = data.body.total; // mock the total data here
      this.listOfDepositOption = data.body.items;
      console.log('challenges data', this.listOfDepositOption);

    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

  public addDepositOptions(): void{
    console.log('function call for deposit option');
    this.countryService.getCountries().subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.listOfCountries = data.body;
      console.log('challenges data', this.listOfCountries);
      this.modalHelper.open( AddDepositOptionComponent, { data: { title: 'New Deposit Option', titleI18n: 'page-name', record: this.listOfCountries }}, 'md').subscribe((res: any) => {
        console.log('modal back response', res);
        if (res) {
          this.reload();
        }
      });
  
    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });

  }

public rowClick(depositOptions: any): void{
  console.log('data', depositOptions);
  this.selectedRow = depositOptions._id;
  const obj = {'country':depositOptions.country, 'state': depositOptions.state};
  this.loading = true;
  this.depositOptionService.filterDepositOptions(obj).subscribe((response: any) => {
    if (response.responseStatus.success){
    this.loading = false;
    this.drawerHelper.create('', DepositOptionDrawerComponent, { record: depositOptions , depositOptionsArray: response.body.items, listOfCountries: this.listOfCountries }).subscribe((res) => {
      console.log('back drawer response', res);
      this.msg.info(res);
    });
  } else{
    this.loading = false;
  }

  }, (error) => {
    this.loading = false;
    console.log(error);
  });

  }


  public reload(): void {
    if (!this.filterObj || this.filterObj == null){
      // this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.paginationToggle = true;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter );
    } else{
      this.applyFilter(this.filterObj);
    }

  }

  submitFilterForm(): void {
    console.log('filter data', this.q);
    this.q.page = this.pageIndex;
    this.q.perPage = this.pageSize;
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const pageObj = { 'page': 1, 'perPage': 25};
      let formObj = filter;
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else{
      return;
    }
  }

  selectTab(name: string): any{
    console.log('display content', name);
    this.selectedTab = name;
    if (this.selectedTab === 'Bets'){

    }
  }
  sortBy(event: any): void{
    
  }

  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    if (formObj.country === undefined) { formObj.country = '' };
    this.depositOptionService.filterDepositOptions(formObj).subscribe((data: any) => {
      console.log('depositOptions filtered data', data);
      if (data.body.items.length){
        this.loading = false;
        this.filterBtnLoader = false;
        this.total = data.body.total; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfDepositOption = data.body.items;
      } else{
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log(error);
    });
  }

  addCountry(): void {
      // this.modalHelper.open( AddCountriesComponent, { data: { title: 'New Country', titleI18n: 'page-name' }}, 'sm').subscribe((res: any) => {
      //   console.log('modal back response', res);
      //   if (res) {
      //     this.reload();
      //   }
      // });
  }
  
public getAllCountries(): void{
  this.countryService.getCountries().subscribe((data: any) => {
    this.loading = false;
    this.listOfCountries = data.body;
    console.log('challenges data', this.listOfCountries);

  }, (error) => {
    this.loading = false;
    console.log('error', error);
  });
}

  reset(): void {
    this.q = {};
    this.filterObj = null;
  }
  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }
  sortByTime(value: any): void{
    console.log(value);
  }


}

