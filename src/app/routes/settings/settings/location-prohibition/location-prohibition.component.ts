import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { RestrictedLocationService } from 'src/app/services/restricted-location/restricted-location.service';
import { LocationProhibitionDrawerComponent } from '../location-prohibition-drawer/location-prohibition-drawer.component';
import { AddLocationProhibitionComponent } from '../add-location-prohibition/add-location-prohibition.component';



interface ProhibitedCountry {
  _id: string;
  state: string;
  country: string;
  gameRestriction: string;
  createdAt: string;
  updatedAt: string;
}

@Component({
  selector: 'app-location-prohibition',
  templateUrl: './location-prohibition.component.html',
  styleUrls: ['./location-prohibition.component.less']
})
export class LocationProhibitionComponent implements OnInit, OnDestroy {

  playOptionsData: any;

  q: any = {
    country: '',
    state: '',
    countryId: '',
    stateId: '',
    page: 1,
    perPage: 30
  };

  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  selectedTab: any = 'Deposit Options';
  selectedIndex: number = 2;


  sortByFilter = [
    { index: 0, label: 'Country: A to Z', value: '', type: 'default', checked: false },
    { index: 1, label: 'Country: Z to A', value: 'oldestFirst', type: 'default', checked: false },
  ];
  status = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Cash Games', value: 'cashGames', type: 'ongoing', checked: false },
    { index: 2, text: 'Card Games', value: 'cardGames', type: 'completed', checked: false },
  ];

  tabs = ['Countries', 'Prohibition', 'Deposit Options'];

  expandForm = false;
  size: any = '"default"';
  listOfProhibitedCountries: ProhibitedCountry[] = [];
  total: number = 0;
  listOfCountries: any[] = [];
  pageSize = 30;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedSortBy = null;

  constructor(private countryService: CountriesService, public msg: NzMessageService, private locationProhibitionService: RestrictedLocationService, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper) { }

  ngOnInit(): void {
    this.loading = false;
    this.fetchAllCountries();
  }

  ngOnDestroy(): void {
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.locationProhibitionService.allRestrictedLocation().subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.listOfProhibitedCountries = data.body.items;
      this.total = data.body.items.length; // mock the total data here
      console.log('prohibition data', this.listOfProhibitedCountries);

    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

  public rowClick(data: any): void {
    console.log('data', data);
    this.selectedRow = data._id;
    this.drawerHelper.create('', LocationProhibitionDrawerComponent, { record: data, listOfCountries: this.listOfCountries }).subscribe((res) => {
      console.log('back drawer response', res);
      if (res) {
        this.reload();
      }
    });
  }

  public reload(): void {
    if (!this.filterObj || this.filterObj == null) {
      // this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    } else {
      this.applyFilter(this.filterObj);
    }

  }

  submitFilterForm(): void {
    console.log('filter data', this.q);
    this.q.page = this.pageIndex;
    this.q.perPage = this.pageSize;
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const pageObj = { 'page': 1, 'perPage': 25 };
      let formObj = filter;
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else {
      return;
    }
  }

  selectTab(name: string): any {
    console.log('display content', name);
    this.selectedTab = name;
    if (this.selectedTab === 'Bets') {

    }
  }

  sortBy(event: any): void {

  }

  fetchAllCountries(): void {
    this.countryService.getCountries().subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.listOfCountries = data.body;
      console.log('challenges data', this.listOfCountries);

    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }


  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    this.loading = false;
  }

  addLocationProhibition(): void {
    this.countryService.getCountries().subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.listOfCountries = data.body;
      console.log('challenges data', this.listOfCountries);
      this.modalHelper.open(AddLocationProhibitionComponent, { data: { title: 'New Prohibition', titleI18n: 'page-name', listOfCountries: this.listOfCountries } }, 'sm').subscribe((res: any) => {
        console.log('modal back response', res);
        if (res) {
          this.reload();
        }
      });
    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });

  }

  addDepositOption(): void {
    this.countryService.addDepositOptions();
  }

  reset(): void {
    this.q = {};
    this.filterObj = null;
  }
  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }
  sortByTime(value: any): void {
    console.log(value);
  }


}

