import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationProhibitionComponent } from './location-prohibition.component';

describe('LocationProhibitionComponent', () => {
  let component: LocationProhibitionComponent;
  let fixture: ComponentFixture<LocationProhibitionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationProhibitionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationProhibitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
