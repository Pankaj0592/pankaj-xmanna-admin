import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { AddCountriesComponent } from './add-countries/add-countries.component';
import { CountriesDrawerComponent } from './countries-drawer/countries-drawer.component';
import { RestrictedLocationService } from 'src/app/services/restricted-location/restricted-location.service';
import { LocationProhibitionComponent } from './location-prohibition/location-prohibition.component';
import { DepositOptionsComponent } from './deposit-options/deposit-options.component';


interface Countries {
  _id: string;
  name: string;
  iso2: string;
  iso3: string;
  updatedAt: string;
  createdAt: string;
}

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit {

  q: any = {
    name: '',
    state: '',
    _id: '',
    stateId: '',
    page: 1,
    perPage: 30
  };
  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  selectedTab: any = 'Countries';
  selectedIndex: number = 0;
  btnSize = '24 hours';
  playOptionsData: any;
  gamesData: any;


  sortByFilter = [
    { index: 0, label: 'Country: A to Z', value: '', type: 'default', checked: false },
    { index: 1, label: 'Country: Z to A', value: 'oldestFirst', type: 'default', checked: false },
  ];

  tabs = [ 'Countries', 'Prohibition', 'Deposit Options' ];

  expandForm = false;
  size: any = '"default"';
  listOfCountries: Countries[] = [ ];
  total = 0;

  pageSize = 30;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedSortBy = null;

  constructor(private countryService: CountriesService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private prohibitonService: RestrictedLocationService, private locationProhibitionComponent: LocationProhibitionComponent, private depositOptions: DepositOptionsComponent) {
               }

  ngOnInit(): void {
    this.loading = false;
    
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    const obj ={};
    this.countryService.getCountries().subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.total = data.body.length; // mock the total data here
      this.listOfCountries = data.body;
      console.log('challenges data', this.listOfCountries);

    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

public rowClick(data: any): void{
  console.log('data', data);
  this.selectedRow = data._id;
  this.drawerHelper.create('', CountriesDrawerComponent, { record: data }).subscribe((res) => {
    console.log('back drawer response', res);
    if (res){
      this.reload();
    }
  });
  }

  public reload(): void {
    if (!this.filterObj || this.filterObj == null){
      // this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter );
    } else{
      this.applyFilter(this.filterObj);
    }

  }

  submitFilterForm(): void {
    console.log('filter data', this.q);
    this.q.page = this.pageIndex;
    this.q.perPage = this.pageSize;
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const pageObj = { 'page': 1, 'perPage': 25};
      let formObj = filter;
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else{
      return;
    }
  }

  selectTab(name: string): any{
    console.log('display content', name);
    this.selectedTab = name;
    if (this.selectedTab === 'Bets'){

    }
  }
  sortBy(event: any): void{
    
  }

  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    // this.countryService.getCountries(formObj).subscribe((data: any) => {
    //   console.log('deathmatch filtered data', data);
    //   if (data.body.items.length){
    //     this.loading = false;
    //     this.filterBtnLoader = false;
    //     this.total = data.body.total; // mock the total data here
    //     console.log('filter data length', this.total);
    //     this.listOfCountries = data.body.items;
    //   } else{
    //     this.loading = false;
    //     this.filterBtnLoader = false;
    //     this.msg.error('No data found through specified filter');
    //   }

    // }, (error) => {
    //   this.loading = false;
    //   this.filterBtnLoader = false;
    //   console.log(error);
    // });
  }

  addCountry(): void {
      this.modalHelper.open( AddCountriesComponent, { data: { title: 'New Country', titleI18n: 'page-name' }}, 'sm').subscribe((res: any) => {
        console.log('modal back response', res);
        if (res) {
          this.reload();
        }
      });
  }

  addDepositOption(): void {
    this.depositOptions.addDepositOptions();
  }
  addLocationProhibition(): void {
    this.locationProhibitionComponent.addLocationProhibition();
  }

  reset(): void {
    this.q = {};
    this.filterObj = null;
  }

  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }

  sortByTime(value: any): void{
    console.log(value);
  }

}

