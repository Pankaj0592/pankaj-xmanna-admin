import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountriesDrawerComponent } from './countries-drawer.component';

describe('CountriesDrawerComponent', () => {
  let component: CountriesDrawerComponent;
  let fixture: ComponentFixture<CountriesDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountriesDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountriesDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
