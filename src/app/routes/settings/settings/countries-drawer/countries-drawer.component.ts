import { Component, OnInit, Input } from '@angular/core';
import { NzDrawerRef } from "ng-zorro-antd/drawer";
import { NzMessageService } from "ng-zorro-antd/message";
import { _HttpClient, ModalHelper, DrawerHelper } from "@delon/theme";
import { NzModalService } from 'ng-zorro-antd/modal';
import { AddCountriesComponent } from '../add-countries/add-countries.component';
import { AddStatesComponent } from '../add-states/add-states.component';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { DeleteModalComponent } from 'src/app/routes/component/delete-modal/delete-modal.component';

interface StatesData {
  _id: string;
  name: string;
  short: string;
}



@Component({
  selector: 'app-countries-drawer',
  templateUrl: './countries-drawer.component.html',
  styleUrls: ['./countries-drawer.component.less']
})
export class CountriesDrawerComponent implements OnInit {


  @Input() record: any;
  statesData: StatesData[] = [ ];
  selectedRow: any;
  tabs = [
    'Bet Info',
  ];

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private http: _HttpClient,
    public msg: NzMessageService,
    private modalHelper: ModalHelper,
    private drawerHelper: DrawerHelper,
    private modal: NzModalService,
    private countryService: CountriesService
  ) {
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
    this.countryWithStates();
  }

  editCountry(): void {
    this.modalHelper.open( AddCountriesComponent, { data: { title: 'Edit Country', record: this.record, stateData: this.statesData }}, 'sm').subscribe((res: any) => {
      console.log('modal back response', res);
      if (res) {
        this.record = res.data;
        // this.reload();
      }
    });
}


deleteCountry(id: string): void {
  const title = 'Delete country?';
  const subtitle = 'This is a test feature. Deletion might affect backend data.';
  const stateData = null;
  const data = this.record;
  this.multiModal(subtitle, stateData, title, data);
}
addState(): void {
  this.modalHelper.open( AddStatesComponent, { data: { title: 'New State', record: this.record, stateData: this.statesData }}, 'sm').subscribe((res: any) => {
    console.log('modal back response', res);
    if (res) {
    this.statesData = res.data;
      // this.reload();
    }
  });  
}
public editState(data: any): void{
  console.log('data', data);
  this.selectedRow = data._id;
  this.modalHelper.open( AddStatesComponent,  { data: { title: 'Edit State', preStateData: data, record: this.record }}, 'sm').subscribe((res) => {
    console.log('back drawer response', res);
    this.msg.info(res);
    if (res){
      this.countryWithStates();
    }
  });
  }

public deleteState(value: any): void{
  const subtitle = 'This is a test feature. Deletion might affect backend data.';
  const title = 'Delete state?';
  const stateData = value;
  const data = this.record;
  // this.multiModal(subtitle, stateData, title, data);
}

multiModal(subtitle: string, stateData: any, title: string, data: any): void{
  const modal = this.modal.warning({
    nzTitle: title,
    nzClosable: true,
    nzAutofocus: null,
    nzCentered: true,
    nzComponentParams: {
      data: data,
      subtitle: subtitle,
      stateData: stateData,
    },
    nzContent: DeleteModalComponent,
  });
  modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
  modal.afterClose.subscribe((result: any) => { 
    if (result){
    console.log('[afterClose] The result is:', result);
    // return;
    if (result){
      this.countryService.deleteCountry(this.record._id).subscribe((res: any) => {
        if (res.responseStatus.success){
          this.drawerRef.close(res.body);
        }
      }, (error) => {
        console.log(error);
      });
    }
    }
  });
}

public countryWithStates(): void {
  this.countryService.fetchCountryWithStates(this.record._id).subscribe((res: any) => {
    if (res.responseStatus.success && res.body){
      this.statesData = res.body;
      console.log("states data", this.statesData);
    }
  }, (error) => {
    console.log('fetching in state error', error);
  });
}

close(): void {
  //   this.drawerRef.close(this.value);
}
}
