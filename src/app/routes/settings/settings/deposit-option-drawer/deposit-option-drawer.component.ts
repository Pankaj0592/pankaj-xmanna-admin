import { Component, OnInit, Input } from '@angular/core';
import { NzDrawerRef } from "ng-zorro-antd/drawer";
import { NzMessageService } from "ng-zorro-antd/message";
import { _HttpClient, ModalHelper, DrawerHelper } from "@delon/theme";
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserManagementService } from 'src/app/services/userService/user-management.service';
import { DepositOptionsService } from 'src/app/services/deposit-option/deposit-option.service';
import { AddDepositOptionComponent } from '../add-deposit-option/add-deposit-option.component';

@Component({
  selector: 'app-deposit-option-drawer',
  templateUrl: './deposit-option-drawer.component.html',
  styleUrls: ['./deposit-option-drawer.component.less']
})
export class DepositOptionDrawerComponent implements OnInit {


  @Input() record: any;
  @Input() depositOptionsArray: any;
  @Input() listOfCountries: any;
  tabs = [
    {
      name: 'Options Info',
      disabled: false
    },
    {
      name: 'Archive',
      disabled: false
    },
  ];

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private http: _HttpClient,
    public msg: NzMessageService,
    private modalHelper: ModalHelper,
    private drawerHelper: DrawerHelper,
    private modal: NzModalService,
    private depositOptionService: DepositOptionsService
  ) {
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
    console.log('drawer value', this.depositOptionsArray);
  }

  editOptions(depositOptions: any): void {
    console.log('data', depositOptions);
    const obj = { 'country': depositOptions.country, 'state': depositOptions.state };
    this.modalHelper.open(AddDepositOptionComponent, { data: { title: 'Edit Deposit Option', titleI18n: 'page-name', record: this.listOfCountries, depositOptionsArray: this.depositOptionsArray, depositOptionsData: depositOptions } }, 'md').subscribe((res: any) => {
    });

  }


  close(): void {
    //   this.drawerRef.close(this.value);
  }
}

