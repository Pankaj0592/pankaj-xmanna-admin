import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOptionDrawerComponent } from './deposit-option-drawer.component';

describe('DepositOptionDrawerComponent', () => {
  let component: DepositOptionDrawerComponent;
  let fixture: ComponentFixture<DepositOptionDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositOptionDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOptionDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
