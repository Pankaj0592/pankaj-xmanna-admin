import { Component, OnInit, Input } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AddLocationProhibitionComponent } from '../add-location-prohibition/add-location-prohibition.component';
import { DeleteModalComponent } from 'src/app/routes/component/delete-modal/delete-modal.component';
import { RestrictedLocationService } from 'src/app/services/restricted-location/restricted-location.service';

@Component({
  selector: 'app-location-prohibition-drawer',
  templateUrl: './location-prohibition-drawer.component.html',
  styleUrls: ['./location-prohibition-drawer.component.less']
})
export class LocationProhibitionDrawerComponent implements OnInit {


  @Input() record: any;
  @Input() listOfCountries: any;

  constructor(
    private drawerRef: NzDrawerRef<string>,
    public msg: NzMessageService,
    private modalHelper: ModalHelper,
    private modal: NzModalService,
    private prohibitionService: RestrictedLocationService
  ) {
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
  }

  editProhibitionLocation(prohibitionData: any): void {
    console.log('data', prohibitionData);
    this.modalHelper.open(AddLocationProhibitionComponent, { data: { title: 'Edit Prohibition', titleI18n: 'page-name', listOfCountries: this.listOfCountries, prohibitedData: this.record } }, 'sm').subscribe((res: any) => {
      console.log('modal back response', res);
      if (res) {
        this.record = res.data;
      }
    });
  }

  deleteProhibitionLocation(prohibitionData: any): void {
    const subtitle = 'Be carefull, it is recommended to perform this action at the time of maintenance.';
    const title = 'Delete exclusion?';
    const stateData = prohibitionData;
    const data = this.record;
    this.multiModal(subtitle, stateData, title, data);
  }
  multiModal(subtitle: string, stateData: any, title: string, data: any): void {
    const modal = this.modal.warning({
      nzTitle: title,
      nzClosable: true,
      nzAutofocus: null,
      nzCentered: true,
      nzComponentParams: {
        data: data,
        subtitle: subtitle,
        stateData: stateData,
      },
      nzContent: DeleteModalComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      if (result.data) {
        console.log('[afterClose] The result is:', result.data);
        this.prohibitionService.deleteRestrictedLocation(this.record._id).subscribe((res: any) => {
          console.log('deleted successfully');
          this.drawerRef.close(res);
        });
      }
    });
  }
  close(): void {
    //   this.drawerRef.close(this.value);
  }
}

