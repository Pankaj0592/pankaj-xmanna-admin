import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationProhibitionDrawerComponent } from './location-prohibition-drawer.component';

describe('LocationProhibitionDrawerComponent', () => {
  let component: LocationProhibitionDrawerComponent;
  let fixture: ComponentFixture<LocationProhibitionDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationProhibitionDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationProhibitionDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
