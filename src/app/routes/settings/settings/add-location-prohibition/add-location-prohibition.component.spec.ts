import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLocationProhibitionComponent } from './add-location-prohibition.component';

describe('AddLocationProhibitionComponent', () => {
  let component: AddLocationProhibitionComponent;
  let fixture: ComponentFixture<AddLocationProhibitionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddLocationProhibitionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLocationProhibitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
