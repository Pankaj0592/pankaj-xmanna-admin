import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { RestrictedLocationService } from 'src/app/services/restricted-location/restricted-location.service';

@Component({
  selector: 'app-add-location-prohibition',
  templateUrl: './add-location-prohibition.component.html',
  styleUrls: ['./add-location-prohibition.component.less']
})
export class AddLocationProhibitionComponent implements OnInit {

  @Input() data: any;
  prohibitionForm!: FormGroup;
  submitted = false;
  submitting = false;
  patchStatess!: FormArray;
  states: any;
  listOfCountries: any[] = [];

  restrictionGameType = [
    { index: 0, label: 'Cash Games', value: 'cashGames', type: 'default', checked: false },
    { index: 1, label: 'Card Games', value: 'cardGames', type: 'default', checked: false },
  ];
  constructor(private fb: FormBuilder, private modal: NzModalRef, private countryService: CountriesService,
    private msg: NzMessageService, private prohibitionService: RestrictedLocationService) { }

  ngOnInit(): void {
    console.log('posted data', this.data);
    this.prohibitionForm = this.fb.group({
      country: ['', [Validators.required]],
      state: [],
      gameRestriction: ['', Validators.required],
    });
    this.prohibitionForm.controls['state'].disable();
    if (this.data.prohibitedData) {
      this.formSetValue();
    }
  }



  selectedCountry(countryData: any): void {
    console.log(countryData);
    if (!countryData) { return; }
    // this.prohibitionForm.controls['country'].setValue(countryData.name);
    this.countryService.fetchCountryWithStates(countryData._id).subscribe((res: any) => {
      console.log('all states', res.body);
      if (res.responseStatus.success) {
        if (res.body && res.body.length) {
          this.states = res.body;
          this.prohibitionForm.controls['state'].enable();
        } else {
          this.prohibitionForm.controls['state'].disable();
          this.prohibitionForm.controls['state'].reset();
        }
      } else {
        this.prohibitionForm.controls['state'].disable();
        this.prohibitionForm.controls['state'].reset();
        // this.msg.
      }
    }, (error) => {
      console.log(error);
    });
  }


  /**
  *
  * @param isValid
  * @param formValue
  * to save addProhibitedForm
  */
  save(isValid: any, formValue: any) {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; };
    this.submitted = true;
    const formObj = {
      country: formValue.country.name,
      state: formValue.state,
      gameRestriction: formValue.gameRestriction,

    };
    console.log('formObj', formObj);
    if (!this.data.prohibitedData) {
      this.prohibitionService.addRestrictedLocation(formObj).subscribe(
        (res: any) => {
          console.log(res);
          if (res.responseStatus.success) {
            this.msg.success('Prohibition for location added successfully');
            console.log('data submit successfully', res);
            this.destroyModal(res.body);
          } else {

          }
        },
        error => {
          console.log('an error has occured while sending data', error);
        });
    } else{
      this.prohibitionService.updateRestrictedLocation(this.data.prohibitedData._id, formObj).subscribe(
        (res: any) => {
          console.log(res);
          if (res.responseStatus.success) {
            console.log('data submit successfully', res);
            this.destroyModal(res.body);
          } else {

          }
        },
        error => {
          console.log('an error has occured while sending data', error);
        });
    }

  }


  formSetValue(): void {
    if (this.data.prohibitedData) {
      this.listOfCountries = this.data.listOfCountries;
      this.listOfCountries.forEach((element) => {
        if (element.name === this.data.prohibitedData.country) {
          // this.selectedCountry = element.name;
          this.prohibitionForm.controls['country'].setValue(element);
          this.countryService.fetchCountryWithStates(element._id).subscribe((res: any) => {
            if (res.responseStatus.success) {
              if (res.body && res.body.length) {
                this.states = res.body;
                this.states.forEach((state: any) => {
                  if (state.name === this.data.prohibitedData.state) {
                    // this.selectedStateName = state.name;
                    this.prohibitionForm.controls['state'].setValue(this.data.prohibitedData.state);
                  }
                });
                this.prohibitionForm.controls['state'].enable();
              } else {
                this.prohibitionForm.controls['state'].disable();
              }
            }
          }, (error) => {
            console.log(error);
          });
        }
      });
      this.prohibitionForm.get('gameRestriction')?.setValue(this.data.prohibitedData.gameRestriction);
    }
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }

}
