import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-add-deposit-option',
  templateUrl: './add-deposit-option.component.html',
  styleUrls: ['./add-deposit-option.component.less']
})
export class AddDepositOptionComponent implements OnInit {

 @Input()data: any;
 depositOptionForm!: FormGroup;
 listOfData: any[] = [1, 2, 3, 4, 5]
 submitted = false;
 states: any[] = [];
 countriesData: any[] = [];
 selectedCountryName: any;
 selectedStateName: any;
 noState: any;
  constructor(private fb: FormBuilder, private countryService: CountriesService) {}


  ngOnInit(): void{
    console.log('being update data', this.data);
    this.depositOptionForm = this.fb.group({
      country: ['', Validators.required],
      state: ['', Validators.required],
      depositOptions: this.fb.array([])
    });
    this.depositOptionForm.controls['state'].disable();

    this.countriesData = this.data.record;
    if (this.data.depositOptionsData){
      this.countriesData.forEach((element) => {
        if (element.name === this.data.depositOptionsData.country) {
          this.selectedCountry = element.name;
          this.depositOptionForm.controls['country'].setValue(element);
          this.countryService.fetchCountryWithStates(element._id).subscribe((res: any) => {
            if (res.responseStatus.success){
              if (res.body && res.body.length){
                this.states = res.body;
                this.states.forEach((state: any) => {
                if (state.name === this.data.depositOptionsData.state){
                    this.selectedStateName = state.name;
                    this.depositOptionForm.controls['state'].setValue(state._id);
                }
              });
                this.depositOptionForm.controls['state'].enable();
              } else{
                // this.depositOptionForm.controls['state'].disable();
              }
            } 
          }, (error) => {
            console.log(error);
          });
        }
      });
    }

    if (this.data.depositOptionsArray) {
      for (let i = 0; i <= this.data.depositOptionsArray.length - 1; ++i){
        this.addDepositOption();
      }
      this.formSetValue();
    } else{
      for (let i = 0; i <= 4; ++i) {
        this.addDepositOption();
      }
    }
  }
 
  depositOptions(): FormArray {
    return this.depositOptionForm.get('depositOptions') as FormArray;
  }
 
  newDepositOption(): FormGroup {
    return this.fb.group({
      country: [this.selectedCountryName],
      state: [this.selectedStateName],
      amount: [5, [Validators.required, Validators.min(1)]],
      bonus: [2],
      startDate: [],
      days: [],
      promo: this.fb.array([])
    });
  }
 
  addDepositOption(): void {
    this.depositOptions().push(this.newDepositOption());
  }
 
  removeDepositOption(depositOptionIndex: number): void {
    this.depositOptions().removeAt(depositOptionIndex);
  }
 
  depositOptionPromo(depositOptionIndex: number): FormArray {
    return this.depositOptions().at(depositOptionIndex).get('promo') as FormArray;
  }
 
  newPromo(): FormGroup {
    return this.fb.group({
      amount: [10, [Validators.required, Validators.min(1)]],
      bonus: [0.5],
      startDate: [null, Validators.required],
      days: [7],
    });
  }
 
  addDepositOptionPromo(depositOptionIndex: number): void {
    this.depositOptionPromo(depositOptionIndex).push(this.newPromo());
  }
 
  removeDepositOptionPromo(depositOptionIndex: number, promoIndex: number): void {
    this.depositOptionPromo(depositOptionIndex).removeAt(promoIndex);
  }
 
  selectedCountry(countryData: any): void{
    console.log(countryData);
    if (!countryData) { return; }
    this.countryService.fetchCountryWithStates(countryData._id).subscribe((res: any) => {
      console.log('all states', res.body);
      if (res.responseStatus.success){
        if (res.body && res.body.length){
          this.states = res.body;
          this.depositOptionForm.controls['state'].enable();
        } else{
          this.depositOptionForm.controls['state'].disable();
          this.noState = 'No state found for this country.';
        }
      } else {
        this.depositOptionForm.controls['state'].disable();
        this.noState = 'No state found for this country.';
        // this.msg.
      }
    }, (error) => {
      console.log(error);
    });
    }

  /**
   *
   * @param isValid
   * @param formValue
   * to save addCountryForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; }
    const formObj = {
      country: formValue.country.name,
      state: formValue.state,
      depositOptions: formValue.depositOptions
    }
    console.log('formObj', formObj);
  }

  formSetValue(): void {
    var data = { depositOptions: this.data.depositOptionsArray }
    this.depositOptionForm.patchValue(data);
  }

}