import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDepositOptionComponent } from './add-deposit-option.component';

describe('AddDepositOptionComponent', () => {
  let component: AddDepositOptionComponent;
  let fixture: ComponentFixture<AddDepositOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDepositOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDepositOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
