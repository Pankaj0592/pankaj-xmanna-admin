import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-add-countries',
  templateUrl: './add-countries.component.html',
  styleUrls: ['./add-countries.component.less']
})
export class AddCountriesComponent implements OnInit {

  @Input() data: any;
  gamesForm!: FormGroup;
  submitted = false;
  submitting = false;
  patchStatess!: FormArray;
  constructor(private fb: FormBuilder, private modal: NzModalRef, private countryService: CountriesService, private msg: NzMessageService) { }

  ngOnInit(): void {
    console.log('posted data', this.data);
    this.gamesForm = this.fb.group({
      name: [null, [Validators.required]],
      iso2: [{ value: ''.toUpperCase() , disabled: false}, [Validators.required, Validators.minLength(2), Validators.maxLength(2), Validators.pattern(/^[A-Z].*$/)]],
      iso3: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(3), Validators.pattern(/^[A-Z].*$/)]],
      phoneCode: [null, [ Validators.required, Validators.pattern(/^(\+?\d{1,3}|\d{1,4})$/) ]],
      patchStatess: this.fb.array([]),
    });
    this.patchStatess = this.gamesForm.get('patchStatess') as FormArray;

    if (this.data.stateData) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.data.stateData.length; ++i) {
      this.addPatchStatess();
    }
    this.formSetValue();
    }
    if (this.data.record) {
      this.formSetValue();
    }
  }


  createPatchStatess(): FormGroup {
    return this.fb.group({
      name: [null, Validators.required],
      short: [null, Validators.required],
    });
  }

  patchStates(): FormGroup {
    return this.fb.group({
      name: [this.data.preStateData.name, Validators.required],
      short: [this.data.preStateData.short, Validators.required],
    });
  }

  addPatchStatess(): void {
    this.patchStatess = this.gamesForm.get('patchStatess') as FormArray;
    this.patchStatess.push(this.createPatchStatess());
  }

  onInput(event: any) {
    event.target.value = event.target.value.toLocaleUpperCase();
}

  /**
   *
   * @param isValid
   * @param formValue
   * to save addCountryForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; }

    this.submitted = true;
    this.submitting = true;
    const formObj = {
      name: formValue.name,
      iso2: formValue.iso2.toUpperCase(),
      iso3: formValue.iso3.toUpperCase(),
      phoneCode: formValue.phoneCode,
      states: this.data.stateData ? this.patchStatess.value : [],
    };
    console.log('form obj', formObj);
    if (!this.data.record) {
      this.countryService.addCountry(formObj).subscribe(
        (res: any) => {
          console.log(res);
          if (res.responseStatus.success) {
            this.submitting = false;
            this.msg.success('Added successfully');
            this.destroyModal(res.body);
          } else {

          }
        },
        error => {
          console.log('an error has occured while sending data', error);
          this.submitting = false;
        });
    } else {
      this.countryService.updateCountry(this.data.record._id, formObj).subscribe((res: any) => {
        if (res.responseStatus.success) {
          console.log('updated successfully', res);
          this.destroyModal(res.body);
          this.submitting = false;
        } else {

        }
      }, (error) => {
        console.log(error);
        this.submitting = false;
      });

    }

  }

  formSetValue(): void {
    if (this.data.stateData){
      var data = { patchStatess: this.data.stateData };
      this.gamesForm.patchValue(data);
      console.log('patch states value', this.patchStatess.value);
    }
    this.gamesForm.get('name')?.setValue(this.data.record.name);
    this.gamesForm.get('iso2')?.setValue(this.data.record.iso2);
    this.gamesForm.get('iso3')?.setValue(this.data.record.iso3);
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }

}
