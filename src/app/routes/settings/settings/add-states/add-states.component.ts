import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CountriesService } from 'src/app/services/countries/countries.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-add-states',
  templateUrl: './add-states.component.html',
  styleUrls: ['./add-states.component.less']
})
export class AddStatesComponent implements OnInit {

  @Input() data: any;
  gamesForm!: FormGroup;
  submitted = false;
  submitting = false;
  states!: FormArray;
  patchStatess!: FormArray;
  statesArray: any;

  constructor(private fb: FormBuilder, private modal: NzModalRef, private countryService: CountriesService, private msg: NzMessageService) { }

  ngOnInit(): void {
    console.log('posted data', this.data);
    console.log('preState data', this.data.preStateData);
    this.gamesForm = this.fb.group({
      states: this.fb.array([]),
      patchStatess: this.fb.array([]),
    });

    this.states = this.gamesForm.get('states') as FormArray;
    this.patchStatess = this.gamesForm.get('patchStatess') as FormArray;
    this.addStates();

    if (this.data.stateData) {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.data.stateData.length; ++i) {
      this.addPatchStatess();
    }
    this.formSetValue();
    }
  }


  createStates(): FormGroup {
    return this.fb.group({
      name: [null, Validators.required],
      short: [null, Validators.required],
    });
  }

  createPatchStatess(): FormGroup {
    return this.fb.group({
      name: [null, Validators.required],
      short: [null, Validators.required],
    });
  }

  patchStates(): FormGroup {
    return this.fb.group({
      name: [this.data.preStateData.name, Validators.required],
      short: [this.data.preStateData.short, Validators.required],
    });
  }

  addStates(): void {
    this.states = this.gamesForm.get('states') as FormArray;
    if (this.data.preStateData){
    this.states.push(this.patchStates());
    } else{
      this.states.push(this.createStates());
    }
  }

  addPatchStatess(): void {
    this.patchStatess = this.gamesForm.get('patchStatess') as FormArray;
    this.patchStatess.push(this.createPatchStatess());
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save addCountryForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; }

    this.submitted = true;
    this.submitting = true;
    if (this.data.stateData){
    this.statesArray = this.patchStatess.value.concat(this.states.value);
  }
    const formObj = {
      name: this.data.record.name,
      iso2: this.data.record.iso2,
      iso3: this.data.record.iso3,
      states: this.data.stateData ? this.statesArray : this.states.value[0],
    };

    console.log('form obj', formObj);

    if (!this.data.preStateData){
    this.countryService.updateCountry(this.data.record._id, formObj).subscribe(
      (res: any) => {
        console.log(res);
        if (res.responseStatus.success) {
          this.submitting = false;
          // this.msg.success('Added successfully');
          this.destroyModal(res.body.states);
        } else {
        } }, error => {
        console.log('an error has occured while sending data', error);
        this.submitting = false;
      });
    } else if (this.data.preStateData) {
      this.countryService.updateState(this.data.record._id, this.data.preStateData._id, formObj).subscribe((res: any) => {
        if (res.responseStatus.success){
          this.destroyModal(res.body);
        } else{

        }
      }, (error) => {
        console.log('error while state update', error);
      });
    }

  }


  formSetValue(): void {
    if (this.data.stateData){
      var data = { patchStatess: this.data.stateData };
      this.gamesForm.patchValue(data);
    }
    console.log('patch states value', this.patchStatess.value);
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }

}
