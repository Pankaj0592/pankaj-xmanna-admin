import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.less']
})
export class DeleteModalComponent implements OnInit {

  @Input() subtitle?: string;
  @Input() data?: any;
  @Input() statement?: any;
  @Input() stateData?: any;

  constructor(private modal: NzModalRef, private countryService: CountriesService) { }

  ngOnInit(): void {
    console.log('delete data', this.data);
  }

 public delete(): void{
  // this.countryService.deleteState(this.stateData._id, this.data._id).subscribe(
  //   (res: any) => {
  //   if (res.responseStatus.success) {
  //     this.modal.destroy(true);
  //   } else {

  //   }}, error => {
  //   console.log(error);
    
  //   });
  this.destroyModal(this.data);
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }
}
