import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBalanceModalComponent } from './user-balance-modal.component';

describe('UserBalanceModalComponent', () => {
  let component: UserBalanceModalComponent;
  let fixture: ComponentFixture<UserBalanceModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserBalanceModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBalanceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
