import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-user-balance-modal',
  templateUrl: './user-balance-modal.component.html',
  styleUrls: ['./user-balance-modal.component.less']
})
export class UserBalanceModalComponent implements OnInit {

  @Input() subtitle?: string;
  @Input() statement?: string;
  @Input() paid?: boolean;
  @Input() data?: any;
  submit = false;
  balanceForm!: FormGroup;
  constructor( private fb: FormBuilder, private modal: NzModalRef ) { }

  ngOnInit(): void {
     this.balanceForm = this.fb.group({
      balance: [0.00, Validators.required],
      comments: ['', Validators.required]
    });
  }


  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void{
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    this.submit = true;
    const formObj = {
      comments: formValue.comments
    };
    console.log('report value', formObj);
  }
  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }
}
