import { Component, OnInit, Input } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { RandomUserService } from 'src/app/services/mock-service/randomUser.service';
import { LoginService } from 'src/app/services/loginService/login.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {

  @Input() subtitle?: string;
  @Input() data?: any;
  @Input() status?: any;
  securityStatus?: string;
  confirmForm!: FormGroup;
  statusToggle: any = {};
  // tslint:disable-next-line: no-inferrable-types
  submit: boolean = false;
  passwordVisible = false;
  password?: string;
  username: any;
  passPhrase: any;
  loginError = false;

  constructor(private modal: NzModalRef, private fb: FormBuilder, private userService: RandomUserService, private loginService: LoginService) {}

  ngOnInit(): void{
    this.securityStatus = this.status;
    this.username = localStorage.getItem('username');

    switch (this.securityStatus){
        case (this.securityStatus = 'warning'):
        this.statusToggle.warning = true;
        break;
        case (this.securityStatus = 'locked'):
        this.statusToggle.lock = true;
        break;
      case (this.securityStatus = 'blacklisted'):
        this.statusToggle.blacklist = true;
        break;
      case (this.securityStatus = 'remove warning'):
        this.statusToggle.moveDefault = true;
        break;
      case (this.securityStatus = 'remove locked'):
        this.statusToggle.moveDefault = true;
        break;
      case (this.securityStatus = 'remove blacklisted'):
        this.statusToggle.moveDefault = true;
        break;
    }

    this.confirmForm = this.fb.group({
      comments: ['', [ Validators.required, Validators.minLength(10) ]],
      password: ['', [ this.statusToggle.blacklist ? ( Validators.required) : Validators.nullValidator]],
    });
  }

  confirmValidator = (control: FormControl): any => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.passPhrase) {
      return { confirm: true, error: true };
    }
  };

  validateConfirmPassword(): void {
    setTimeout(() => this.confirmForm.controls.confirmPassword.updateValueAndValidity());
  }


  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void{
    this.submit = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    if (this.statusToggle.blacklist){
      const formObj = {
        username: this.username,
        password: formValue.password
      };
      this.loginService.validateLogin(formObj).subscribe((res: any) => {
        if (res.responseStatus.success && res.body === true){
          this.loginError = false;
          this.changeStatus(formValue);
        }
      }, (error) => {
        this.loginError = true;
        this.confirmForm.controls['password'].setErrors({'error': true});
        return;
      });
    }
    if (!this.statusToggle.blacklist){
      this.changeStatus(formValue);

    }
 
  } 



  changeStatus(formValue: any){

    const formObj = {
      id: this.data._id,
      securityStatus: this.statusToggle.moveDefault ? 'fair' : this.securityStatus,
      comments: formValue.comments
    };
    console.log('report value', formObj);

    this.userService.updateSecurityStatus(formObj).subscribe((res: any) => {
      if (res.responseStatus.success){
        console.log('updated security status res', res);
        this.destroyModal(res.body);
      }
    }, (error) => {
      console.log('error in security status res', error);
      });
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }

}
