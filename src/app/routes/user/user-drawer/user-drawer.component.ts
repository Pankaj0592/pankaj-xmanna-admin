import { Component, Input, OnInit, TemplateRef, ViewContainerRef, Output, EventEmitter } from '@angular/core';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { I18NService } from '@core';
import { getTimeDistance } from '@delon/util/date-time';
import { _HttpClient, DrawerHelper } from '@delon/theme';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { STColumn } from '@delon/abc/st';
import { yuan } from '@shared';
import { deepCopy } from '@delon/util/other';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { ModalComponent } from '../../component/modal/modal.component';
import { UserBalanceModalComponent } from '../../component/user-balance-modal/user-balance-modal.component';
import { UserManagementService } from 'src/app/services/userService/user-management.service';


interface Record {
  _id: string;
  loginType: string;
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Component({
  selector: 'app-user-drawer',
  templateUrl: './user-drawer.component.html',
  styleUrls: ['./user-drawer.component.css'],
})
export class UserDrawerComponent implements OnInit {

  @Input() public record: any;
  @Output() public userId = new EventEmitter<any>();
  toggle = false;
  tabs = [
    {
      name: 'User Info',
      disabled: false
    },
    {
      name: 'Challenges',
      disabled: false
    },
    {
      name: 'Tournaments',
      disabled: true
    },
    {
      name: 'Balance',
      disabled: false
    },
    {
      name: 'Game Profile',
      disabled: false
    },
    {
      name: 'Admin Actions',
      disabled: false
    }
  ];

  data: any = {};
  loading = true;
  userToken: any;
  date_range: Date[] = [];

  rankingListData: Array<{ title: string; total: number }> = Array(7)
    .fill({})
    .map((_, i) => {
      return {
        title: this.i18n.fanyi('app.analysis.test', { no: i }),
        total: 323234,
      };
    });
  titleMap = {
    y1: this.i18n.fanyi('app.analysis.traffic'),
    y2: this.i18n.fanyi('app.analysis.payments'),
  };
  searchColumn: STColumn[] = [
    {
      title: { text: '排名', i18n: 'app.analysis.table.rank' },
      index: 'index',
    },
    {
      title: { text: '搜索关键词', i18n: 'app.analysis.table.search-keyword' },
      index: 'keyword',
      click: (item) => this.msg.success(item.keyword),
    },
    {
      type: 'number',
      title: { text: '用户数', i18n: 'app.analysis.table.users' },
      index: 'count',
      sort: {
        compare: (a, b) => a.count - b.count,
      },
    },
    {
      type: 'number',
      title: { text: '周涨幅', i18n: 'app.analysis.table.weekly-range' },
      index: 'range',
      render: 'range',
      sort: {
        compare: (a, b) => a.range - b.range,
      },
    },
  ];

  salesType = 'all';
  salesPieData: any;
  salesTotal = 0;

  saleTabs: Array<{ key: string; show?: boolean }> = [
    { key: 'sales', show: true },
    { key: 'visits' },
  ];

  offlineIdx = 0;

  panels = [
    {
      active: false,
      disabled: false,
      name: 'ETH',
    },
    {
      active: true,
      disabled: false,
      name: 'USDT',
    },
    {
      active: false,
      disabled: true,
      name: 'TRX',
    },
    {
      active: false,
      disabled: false,
      name: 'BTC',
    },
  ];

  gameProfilePanel = [
    {
      active: false,
      disabled: false,
      name: 'Football Star',
    },
    {
      active: true,
      disabled: false,
      name: 'Manna Wars',
    }
  ];

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private http: _HttpClient,
    public msg: NzMessageService,
    private i18n: I18NService,
    private cdr: ChangeDetectorRef,
    private modal: NzModalService,
    private userService: UserManagementService,
  ) { }

  close(): void {
    //   this.drawerRef.close(this.value);
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
    this.generateToken();
  }


  copyMessage(val: string): void{
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }


  public tabSelect(value: any): void {
    if (value === this.tabs[4]) {
      this.http.get('/chart').subscribe((res) => {
        res.offlineData.forEach((item: any, idx: number) => {
          item.show = idx === 0;
          item.chart = deepCopy(res.offlineChartData);
          console.log('chart data', item.chart);
        });
        this.data = res;
        console.log('pie chart data', this.data);
        this.loading = false;
        this.changeSaleType();
      });
    }
  }

  setDate(type: 'today' | 'week' | 'month' | 'year'): void {
    this.date_range = getTimeDistance(type);
    setTimeout(() => this.cdr.detectChanges());
  }

  changeSaleType(): void {
    this.salesPieData =
      this.salesType === 'all' ? this.data.salesTypeData : this.salesType === 'online' ? this.data.salesTypeDataOnline : this.data.salesTypeDataOffline;
    if (this.salesPieData) {
      console.log('sales pie data', this.salesPieData);
      this.salesTotal = this.salesPieData.reduce(
        (pre: number, now: { y: number }) => now.y + pre,
        0
      );
    }
    this.cdr.detectChanges();
  }

  handlePieValueFormat(value: string | number): string {
    return yuan(value);
  }
  salesChange(idx: number): void {
    if (this.saleTabs[idx].show !== true) {
      this.saleTabs[idx].show = true;
      this.cdr.detectChanges();
    }
  }
  offlineChange(idx: number): void {
    if (this.data.offlineData[idx].show !== true) {
      this.data.offlineData[idx].show = true;
      this.cdr.detectChanges();
    }
  }

  reload(): void {
    console.log('reload works');
  }

  generateToken(): void{
    const obj = { 'id': this.record._id, 'gameId': this.record.game};
    this.userService.getUserToken(obj).subscribe((res: any) => {
      if (res.responseStatus.success){
        this.userToken = res.body.token;
        console.log('token', this.userToken);
      }
    }, (error) => {
      console.log(error);
    });
  }

  statusChange(status: string): void {
    console.log('status value', status);

    switch (status) {
      case (status = 'warning'):
        const modal = this.modal.warning({
          nzTitle: 'Do you want to warn this user?',
          nzClosable: false,
          nzCentered: true,
          nzAutofocus: null,
          nzComponentParams: {
            subtitle: 'Leave a message to inform user about the reason of ' + status,
            data: this.record,
            status: status,
          },
          nzContent: ModalComponent,
        });
        modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        modal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result);
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;

      case (status = 'remove warning'):
        const rmWarningModal = this.modal.warning({
          nzTitle: 'Do you want to remove warning?',
          nzClosable: false,
          nzCentered: true,
          nzAutofocus: null,
          nzComponentParams: {
            subtitle: 'Leave a message to keep in the system the reason of warning removal.',
            data: this.record,
            status: status,
          },
          nzContent: ModalComponent,
        });
        rmWarningModal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        rmWarningModal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result)
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;

      case (status = 'locked'):
        const lockedModal = this.modal.error({
          nzTitle: 'Do you want to lock this user?',
          nzClosable: false,
          nzCentered: true,
          nzAutofocus: null,
          nzContent: ModalComponent,
          nzComponentParams: {
            subtitle: 'The user won’t be able to use xManna products for a lock period. Leave a message to keep in the system the reason of locking.',
            data: this.record,
            status: status,
          },
        });
        lockedModal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        lockedModal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result);
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;
      case (status = 'remove locked'):
        const rmLockedModal = this.modal.error({
          nzTitle: 'Do you want to unlock this user?',
          nzClosable: false,
          nzCentered: true,
          nzAutofocus: null,
          nzContent: ModalComponent,
          nzComponentParams: {
            subtitle: 'Leave a message to keep in the system the reason of locked removal.',
            data: this.record,
            status: status,
          },
        });
        rmLockedModal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        rmLockedModal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result);
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;
      case (status = 'blacklisted'):
        const blackListmodal = this.modal.error({
          nzTitle: 'Do you want to blocklist this user?',
          nzCentered: true,
          nzClosable: false,
          nzAutofocus: null,
          nzContent: ModalComponent,
          nzComponentParams: {
            subtitle: 'Blocklisted user will be restricted from using any xManna product. Leave a message to keep in the system the reason of blocking.',
            data: this.record,
            status: status,
          },
        });
        blackListmodal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        // Return a result when closed
        blackListmodal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result);
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;
      case (status = 'remove blacklisted'):
        const rmBlackListmodal = this.modal.error({
          nzTitle: 'Do you want to remove blacklist this user?',
          nzClosable: false,
          nzAutofocus: null,
          nzContent: ModalComponent,
          nzComponentParams: {
            subtitle: 'Leave a message to keep in the system the reason of blacklist removal.',
            data: this.record,
            status: status,
          },
        });
        rmBlackListmodal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
        // Return a result when closed
        rmBlackListmodal.afterClose.subscribe((result) => {
          if (result) {
            console.log('[afterClose] The result is:', result);
            this.record.securityStatus = result.data.securityStatus;
          }
        });
        break;
      // code
    }
  }


  balanceModal(value: any): void {
    switch (value) {
      case (value = 'depositPractice'):
        var subtitle = "How much do you want to add to user's balance?";
        var statement = 'Balance after deposit:';
        var title = 'Deposit Practice';
        var data = this.record._id;
        var paid = false;
        this.multiModal(subtitle, statement, title, data, paid);
        break;
      case (value = 'removePractice'):
        var subtitle = "How much do you want to remove from user's balance?";
        var statement = 'Balance after removal:';
        var title = 'Remove Practice';
        var data = this.record._id;
        var paid = false;
        this.multiModal(subtitle, statement, title, data, paid);
        break;
      case (value = 'depositFiat'):
        var subtitle = "How much do you want to remove from user's balance?";
        var statement = 'Balance after removal:';
        var title = 'Deposit fiat';
        var data = this.record._id;
        var paid = true;
        this.multiModal(subtitle, statement, title, data, paid);
        break;
      case (value = 'removeFiat'):
        var subtitle = "How much do you want to remove from user's balance?";
        var statement = 'Balance after removal:';
        var title = 'Remove fiat';
        var data = this.record._id;
        var paid = true;
        this.multiModal(subtitle, statement, title, data, paid);
        break;
    }
  }

  multiModal(subtitle: string, statement: string, title: string, data: any, paid: boolean): void {
    const modal = this.modal.warning({
      nzTitle: title,
      nzClosable: false,
      nzCentered: true,
      nzAutofocus: null,
      nzComponentParams: {
        data: this.record._id,
        subtitle: subtitle,
        statement: statement,
        paid: paid
      },
      nzContent: UserBalanceModalComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
      }
    });
  }

}
