import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { UserDrawerComponent } from './user-drawer/user-drawer.component';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    UserRoutingModule,
    NzToolTipModule
  ],
  declarations: [COMPONENTS, UserComponent, UserDrawerComponent],
  entryComponents: [ UserDrawerComponent ],
  exports: [UserDrawerComponent]
})
export class UserModule { }
