import { Component, OnInit } from '@angular/core';
import { DrawerHelper } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { UserDrawerComponent } from './user-drawer/user-drawer.component';
import { UserManagementService } from 'src/app/services/userService/user-management.service';


interface User {
  _id: string;
  loginType: string;
  email: string;
  username: string;
  securityStatus: string;
  lastActive: string;
  fName: string;
  lName: string;
}

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})

export class UserComponent implements OnInit {

  q: any = {
    username: '',
    primaryEmail: '',
    loginType: '',
    lastActive: '',
    securityStatus: '',
    status: '',
    kycVerified: null,
    phoneNumber: ''
  };
  // search filter through phoneNumber value : 684130909
  loading = false;
  filterBtnLoader = false;
  size: any = 'default';
  selectedRow: any;
  expandForm = false;

  listOfUser: User[] = [];
  total = 0;
  pageSize = 30;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedUser: any = null;
  pageReset = false;

  securityStatus = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Fair', value: 'fair', type: 'default', checked: false },
    { index: 2, text: 'Warning', value: 'warning', type: 'default', checked: false },
    { index: 3, text: 'Locked', value: 'locked', type: 'default', checked: false },
    { index: 4, text: 'Blacklisted', value: 'blacklisted', type: 'default', checked: false },
  ];

  status = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];

  kycValue = [
    // { label: 'None', value: null, checked: true },
    { label: 'Yes', value: true, checked: false },
    { label: 'No', value: false },
  ];

  sortByFilters = [
    { index: 0, label: 'Most Recent', value: '', type: 'default', checked: false },
    { index: 1, label: 'Username: A to Z', value: 'username', type: 'default', checked: false },
    { index: 2, label: 'Username: Z to A', value: '-username', type: 'default', checked: false },
    { index: 3, label: 'Lastname: A to Z', value: 'lName fName', type: 'default', checked: false },
    { index: 4, label: 'Lastname: Z to A', value: '-lName -fName', type: 'default', checked: false },
  ];

  constructor(private userService: UserManagementService, public msg: NzMessageService, private drawerHelper: DrawerHelper) { }

  ngOnInit(): void { }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.userService.getUsers(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      if (data.responseStatus.success) {
        console.log('user data', data);
        this.loading = false;
        this.total = data.body.totalNumberOfUsers;
        this.listOfUser = data.body.userData;
      } else {
        console.log('not success error', data);
      }
    }, (error) => {
      this.loading = false;
      console.log(error);
    });
  }

  // onQueryParamsChange(params: NzTableQueryParams): void {
  //   console.log(params);
  //   const { pageSize, pageIndex, sort, filter } = params;
  //   const currentSort = sort.find((item) => item.value !== null);
  //   let sortField = (currentSort && currentSort.key) || null;
  //   const sortOrder = (currentSort && currentSort.value) || null;
  //   if (sortField && sortOrder === 'descend' && (sortField !== 'lName fName' && sortOrder === 'descend')) {
  //     sortField = '-' + sortField;
  //   } else if (sortField === 'lName fName' && sortOrder === 'descend') {
  //     sortField = '-lName -fName';
  //   }
  //   console.log('sort field', sortField);
  //   // this.msg.info(JSON.stringify(sortField) + ',' + pageIndex + ',' + pageSize);
  //   // this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
  //   this.pageIndex = pageIndex;
  //   this.pageSize = pageSize;
  //   this.sortField = sortField ? sortField : this.sortField;
  //   this.sortOrder = sortOrder ? sortOrder : this.sortOrder;
  //   this.filter = filter;
  //   this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
  // }

  onQueryParamsChange(params: any): void {
    console.log(params);
    if (this.pageReset) {
    this.pageReset = false;
    return;
    }
    // if (!this.filterObj || this.filterObj == null) {
    const { pageSize, pageIndex, sort, filter } = params;
    if (!Array.isArray(sort)) {
      this.sortField = params;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      return;
    }
    const currentSort = sort.find((item, index) => item.value !== null);
    console.log('current sort', currentSort);
    if (currentSort === undefined) {
      this.selectedUser = '';
      this.sortField = null;
      this.sortOrder = null;
    }
    let sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    console.log('sort field and order', this.sortField, this.sortOrder);
    // this.selectedUser = this.sortByFilter[4]['value'];
    if ((sortField === 'username' && sortOrder === 'ascend') || (sortField === 'daily' && sortOrder === 'ascend')) {
      this.selectedUser = this.sortByFilters[1]['value'];
    } else if ((sortField === 'username' && sortOrder === 'descend') || (sortField === 'daily' && sortOrder === 'descend')) {
      this.selectedUser = this.sortByFilters[2]['value'];
      if (sortField === 'daily') {
        this.selectedUser = this.sortByFilters[3]['value'];
      }
      // sortField = sortField + '-desc';
    } else if (sortField === 'lName fName' && sortOrder === 'ascend') {
      this.selectedUser = this.sortByFilters[3]['value'];
      // sortField = '-' + sortField;
    } else if (sortField === 'lName fName' && sortOrder === 'descend') {
      this.selectedUser = this.sortByFilters[4]['value'];
      // sortField = '-' + sortField;
    }

    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField ? sortField : this.sortField;
    this.sortOrder = sortOrder ? sortOrder : this.sortOrder;
    this.filter = this.filterObj ? this.filterObj.filter : filter;
    if (!this.filterObj || this.filterObj == null) {
    this.loadDataFromServer(pageIndex, pageSize, this.sortField, this.sortOrder, this.filter);
  } else{
      this.getData(pageIndex, pageSize);
  }
  }

  sortBy(sortParams: any): void {
    this.onQueryParamsChange(sortParams);
    return;
  }

  public rowClick(data: any): void {
    console.log('data', data);
    this.selectedRow = data._id;
    this.drawerHelper
      .create('', UserDrawerComponent, { record: data })
      .subscribe((res) => {
        this.msg.info(res);
      });
    // this.drawerHelper.create('', UserDrawerComponent, { record: data },
    // { size: 540, footer: false, drawerOptions: {      
    //   nzMaskClosable: true,
    //   nzClosable: true,
    // } }, ).subscribe(res => console.log('成功'));
  }

  public reload(): void {
    if (!this.filterObj || this.filterObj == null) {
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    } else {
      // this.applyFilter(this.filterObj);
      this.getData(this.pageIndex, this.pageSize);
    }

  }

  getData(pageIndex: number, pageSize: number): void {
    console.log('filter data', this.q);
    const filter = this.q;
    for (const propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ''
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const pageObj = { 'page': pageIndex, 'resPerPage': pageSize, 'sortBy': this.sortField };
      const formObj = { filter };
      const formObj1 = { filter, ...pageObj, };
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj1);
    } else {
      return;
    }
  }

  applyFilter(formObj: any): void {
    this.userService.filterUser(formObj).subscribe((data: any) => {
      this.loading = false;
      this.filterBtnLoader = false;
      if (data.body.totalNumberOfUsers > 0) {
        console.log('user data', data.body);
        this.loading = false;
        this.filterBtnLoader = false;
        this.total = data.body.totalNumberOfUsers;
        console.log('filter data length', this.total);
        this.listOfUser = data.body.userData;
      } else {
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No user found through specified filter');
      }
    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log(error);
    });
  }

  log(value: any): void {
    console.log(value);
    this.q.kyc = value;
  }

  filterFormReset(): void {
    this.q = {};
    this.filterObj = null;
    this.pageReset = true;
    // this.pageIndex = 1;
    // this.pageSize = 30;
  }

  changeTableSize(sizeValue: any): void {
    console.log(sizeValue);
    this.size = sizeValue.toString();
  }

}
