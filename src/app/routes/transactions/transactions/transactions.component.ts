import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { SinglePlayerService } from 'src/app/services/single-player/single-player.service';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';
import { MultiplayerService } from 'src/app/services/multiplayer/multiplayer.service';
import { BetsService } from 'src/app/services/Bets/bets.service';
import { TransactionsService } from 'src/app/services/transactions/transactions.service';
import { WithdrawalsDrawerComponent } from '../withdrawals-drawer/withdrawals-drawer.component';


interface Transaction {
  _id: string;
  amount: string;
  currency: string;
  method: any;
  status: string;
  createdAt: string;
}

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.less']
})
export class TransactionsComponent implements OnInit {

  validateForm!: FormGroup;
  paginationToggle = true;
  playOptionsData: any;
  gamesData: any;
  q: any = {
    transactionId: '',
    userId: '',
    status: '',
    method: '',
    currency: '',
    page: 1,
    perPage: 30
  };
  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  selectedTab: any = 'Withdrawals';
  selectedIndex: number = 1;
  btnSize = '24 hours';

  status = [{ index: 0, text: 'None', value: null, type: 'default', checked: false },
  { index: 1, text: 'New', value: 'new', type: 'default', checked: false },
  { index: 2, text: 'Cancelled', value: 'canceled', type: 'default', checked: false },
  { index: 3, text: 'In progress', value: 'in_progress', type: 'default', checked: false },
  { index: 3, text: 'Pending', value: 'pending', type: 'default', checked: false },
];

  currency = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'USD', value: 'usd', type: 'default', checked: false },
  ];
  method = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Paypal', value: 'paypal', type: 'success', checked: false },
  ];
  sortByFilter = [
    { index: 0, label: 'Most Recent', value: '', type: 'default', checked: false },
    { index: 1, label: 'Oldest First', value: 'oldestFirst', type: 'default', checked: false },
  ];

  tabs = [ 'Deposits', 'Withdrawals', 'Pending Withdrawals', 'All Transations' ];

  expandForm = false;
  size: any = '"default"';
  listOfTransactions: Transaction[] = [ ];
  total = 0;

  pageSize = 30;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedUser = null;

  constructor(private transactionService: TransactionsService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private gameService: GamesManagementService, private playOptionService: PlayOptionsService) {}

  ngOnInit(): void {
    this.loading = false;
    this.fetchAllGames();
    this.fetchAllPlayOptions();
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.transactionService.getTransactions(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      // console.log('challenges data', data);
      this.total = data.body.total; // mock the total data here
      this.listOfTransactions = data.body.items;
      console.log('challenges data', this.listOfTransactions);

    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

  public fetchAllGames(): void{
    this.gameService.allGames().subscribe((res: any) => {
      if (res.responseStatus.success){
        console.log('games data', res);
        // this.gamesData = res.body.gameData;
        this.gamesData = [];
        res.body.gameData.forEach((element: any) => {
          if (element.playerType === 'multiple'){
            this.gamesData.push(element);
          }      
        });
        console.log('games data', this.gamesData);
      }
    }, (error) => {
      console.log('games data error', error);
    });
  }

  public fetchAllPlayOptions(): void{
    this.playOptionService.getAllPlayOptionsWithoutPage().subscribe((res: any) =>{
      if (res.responseStatus.success){
        console.log('playOptions data', res);
        this.playOptionsData = res.body.playOptions;
      }
    }, (error) => {
      console.log('playOption data error', error);
    });
  }


public rowClick(data: any): void{
  console.log('data', data);
  this.selectedRow = data._id;
  this.drawerHelper.create('', WithdrawalsDrawerComponent, { record: data , menuSelectedTab : this.selectedTab}).subscribe((res) => {
    console.log('back drawer response', res);
    this.msg.info(res);
  });
  }

  public reload(): void {
    if (!this.filterObj || this.filterObj == null){
      this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.paginationToggle = true;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter );
    } else{
      this.applyFilter(this.filterObj);
    }

  }

  getData(): void {
    console.log('filter data', this.q);
    this.q.page = this.pageIndex;
    this.q.perPage = this.pageSize;
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const pageObj = { 'page': 1, 'perPage': 25};
      let formObj = filter;
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else{
      return;
    }
  }

  selectTab(name: string): any{
    console.log('display content', name);
    this.selectedTab = name;
    if (this.selectedTab === 'Bets'){

    }
  }
  sortBy(event: any): void{
    
  }

  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    this.transactionService.filterTransaction(formObj).subscribe((data: any) => {
      console.log('deathmatch filtered data', data);
      if (data.body.items.length){
        this.loading = false;
        this.filterBtnLoader = false;
        this.total = data.body.total; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfTransactions = data.body.items;
      } else{
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log(error);
    });
  }
  
  reset(): void {
    this.q = {};
    this.filterObj = null;
  }
  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }
  sortByTime(value: any): void{
    console.log(value);
  }


}
