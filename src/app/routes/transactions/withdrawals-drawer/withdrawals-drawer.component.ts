import { Component, OnInit, Input } from '@angular/core';
import { NzDrawerRef } from "ng-zorro-antd/drawer";
import { NzMessageService } from "ng-zorro-antd/message";
import { _HttpClient, ModalHelper, DrawerHelper } from "@delon/theme";
import { SinglePlayerService } from '../../../services/single-player/single-player.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserManagementService } from 'src/app/services/userService/user-management.service';
import { SinglePlayerModalComponent } from '../../single-player/single-player-modal/single-player-modal.component';


@Component({
  selector: 'app-withdrawals-drawer',
  templateUrl: './withdrawals-drawer.component.html',
  styleUrls: ['./withdrawals-drawer.component.less']
})
export class WithdrawalsDrawerComponent implements OnInit {


  @Input() record: any;
  tabs = [
    {
      name: 'Transation Info',
      disabled: false
    },
    {
      name: 'User Stats',
      disabled: true
    },
    {
      name: 'Actions',
      disabled: false
    },
  ];

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private http: _HttpClient,
    public msg: NzMessageService,
    private modalHelper: ModalHelper,
    private drawerHelper: DrawerHelper,
    private modal: NzModalService,
    private challengeService: SinglePlayerService,
    private userService: UserManagementService
  ) {
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
  }


close(): void {
  //   this.drawerRef.close(this.value);
}
}

