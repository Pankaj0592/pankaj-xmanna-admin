import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WithdrawalsDrawerComponent } from './withdrawals-drawer.component';

describe('WithdrawalsDrawerComponent', () => {
  let component: WithdrawalsDrawerComponent;
  let fixture: ComponentFixture<WithdrawalsDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WithdrawalsDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WithdrawalsDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
