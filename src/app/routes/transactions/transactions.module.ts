import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions/transactions.component';
import { WithdrawalsDrawerComponent } from './withdrawals-drawer/withdrawals-drawer.component';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    TransactionsRoutingModule,
  ],
  declarations: [ COMPONENTS, TransactionsComponent, WithdrawalsDrawerComponent],
})
export class TransactionsModule { }
