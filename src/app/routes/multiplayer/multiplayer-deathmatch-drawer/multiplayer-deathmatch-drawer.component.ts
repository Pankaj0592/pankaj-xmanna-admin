import { Component, OnInit, Input } from '@angular/core';
import { NzMessageService } from "ng-zorro-antd/message";
import { _HttpClient } from "@delon/theme";
import { NzModalService } from 'ng-zorro-antd/modal';
import { SinglePlayerModalComponent } from '../../single-player/single-player-modal/single-player-modal.component';


@Component({
  selector: 'app-multiplayer-deathmatch-drawer',
  templateUrl: './multiplayer-deathmatch-drawer.component.html',
  styleUrls: ['./multiplayer-deathmatch-drawer.component.less']
})
export class MultiplayerDeathmatchDrawerComponent implements OnInit {


  @Input() record: any;
  @Input() menuSelectedTab: any;
  transactionDetails: any = {};
  cheaterDetail: any;
  data: any = {};
  loading = false;
  tabs = [
    'Room Info',
    'Bouts',
    'Bids & Transactions',
    'Log'
  ];

  constructor(public msg: NzMessageService, private modal: NzModalService) { }

  ngOnInit(): void {
    console.log('drawer value', this.record);
    console.log('tab value', this.menuSelectedTab);
  }

  cancelMatch(value: string): void {

    switch (value) {
      case (value = 'open'): case (value = 'reported'):
        var subtitle = 'Who’s going to get the entry fee refund?';
        var statement = 'Balance after deposit:';
        var refund = false;
        var title = 'Do you want to cancel this match?';
        var data = this.record;
        this.multiModal(subtitle, statement, title, data, refund);
        break;
      case (value = 'finished'): case (value = 'aborted'):
        var title = 'Do you want to refund entry fees?';
        var statement = 'Balance after removal:';
        var refund = true;
        var subtitle = 'dummy subtitle';
        // var subtitle = 'Only the loser ' + this.record.bouts.looser + 'with ' + lessScore + ' points will get the refund.';
        var data = this.record;
        this.multiModal(subtitle, statement, title, data, refund);
        break;
    }
  }

  multiModal(subtitle: string, statement: string, title: string, data: any, refund: boolean): void {
    const modal = this.modal.warning({
      nzTitle: title,
      nzClosable: false,
      nzAutofocus: null,
      nzComponentParams: {
        data: this.record,
        subtitle: subtitle,
        statement: statement,
        refund: refund
      },
      nzContent: SinglePlayerModalComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
      }
    });
  }
  cheaterDetails(playerId: any): void {
    // console.log(playerId);
    // const filter = { '_id': playerId};
    // const formObj = {filter};
    // this.userService.filterUser(formObj).subscribe((res:any) => {
    //   console.log(res);
    //   this.cheaterDetail = res.body[0];
    //   console.log('cheater detail=========', this.cheaterDetail);
    //   // this.drawerHelper.create('', UserDrawerComponent, { record: this.cheaterDetail }).subscribe((res: any) => {
    //   //   this.msg.info(res);
    //   // });
    //   this.drawerHelper.create('', UserDrawerComponent, { record: this.cheaterDetail },
    //   { size: 540, footer: false, drawerOptions: {      
    //     nzMaskClosable: true,
    //     nzClosable: true,
    //   } }, ).subscribe(res => console.log('成功'));
    // });
  }

  close(): void {
    //   this.drawerRef.close(this.value);
  }
}
