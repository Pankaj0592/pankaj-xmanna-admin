import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplayerDeathmatchDrawerComponent } from './multiplayer-deathmatch-drawer.component';

describe('MultiplayerDeathmatchDrawerComponent', () => {
  let component: MultiplayerDeathmatchDrawerComponent;
  let fixture: ComponentFixture<MultiplayerDeathmatchDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplayerDeathmatchDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplayerDeathmatchDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
