import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { MultiplayerBetsDrawerComponent } from './multiplayer-bets-drawer/multiplayer-bets-drawer.component';
import { MultiplayerBetsComponent } from './multiplayer-bets/multiplayer-bets.component';
import { MultiplayerDeathmatchDrawerComponent } from './multiplayer-deathmatch-drawer/multiplayer-deathmatch-drawer.component';
import { MultiplayerRoutingModule } from './multiplayer-routing.module';
import { MultiplayerComponent } from './multiplayer/multiplayer.component';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
  SharedModule,
    MultiplayerRoutingModule, 
  ],
  declarations: [COMPONENTS, MultiplayerComponent, MultiplayerDeathmatchDrawerComponent, MultiplayerBetsComponent, MultiplayerBetsDrawerComponent],
})
export class MultiplayerModule { }
