import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplayerBetsDrawerComponent } from './multiplayer-bets-drawer.component';

describe('MultiplayerBetsDrawerComponent', () => {
  let component: MultiplayerBetsDrawerComponent;
  let fixture: ComponentFixture<MultiplayerBetsDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplayerBetsDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplayerBetsDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
