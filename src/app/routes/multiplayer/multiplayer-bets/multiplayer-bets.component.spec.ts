import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiplayerBetsComponent } from './multiplayer-bets.component';

describe('MultiplayerBetsComponent', () => {
  let component: MultiplayerBetsComponent;
  let fixture: ComponentFixture<MultiplayerBetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MultiplayerBetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiplayerBetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
