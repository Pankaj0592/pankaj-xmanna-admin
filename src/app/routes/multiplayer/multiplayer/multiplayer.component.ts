import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';
import { MultiplayerService } from 'src/app/services/multiplayer/multiplayer.service';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';
import { MultiplayerDeathmatchDrawerComponent } from '../multiplayer-deathmatch-drawer/multiplayer-deathmatch-drawer.component';



interface Deathmatch {
  _id: string;
  name: string;
  playOption: string;
  playOptions: any;
  logo: string;
  entryFee: string;
  bets: string;
  status: string;
  createdBy: string;
  updatedAt: string;
  createdAt: string;
}

@Component({
  selector: 'app-multiplayer',
  templateUrl: './multiplayer.component.html',
  styleUrls: ['./multiplayer.component.less']
})
export class MultiplayerComponent implements OnInit {

  q: any = {
    _id: '',
    name: '',
    genre: '',
    status: '',
    type: '',
    playOption: '',
    game: '',
    page: null,
    perPage: null
  };

  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  playOptionsData: any;
  gamesData: any;
  selectedTab: any = 'Deathmatch';
  selectedIndex: number = 1;
  sortByPeriod = '24 hours';

  status = [{ index: 0, text: 'None', value: null, type: 'default', checked: false },
  { index: 1, text: 'Active', value: 'created', type: 'default', checked: false },
  { index: 2, text: 'Cancelled', value: 'cancelled', type: 'default', checked: false },
  { index: 3, text: 'Finished', value: 'finished', type: 'default', checked: false },
  { index: 4, text: 'Reported', value: 'reported', type: 'default', checked: false },
  { index: 4, text: 'Suspicious', value: 'suspicious', type: 'default', checked: false },
  { index: 4, text: 'None', value: 'none', type: 'default', checked: false },
  ];

  type = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'active', value: false, type: 'default', checked: false },
    { index: 2, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  genre = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  sortByFilter = [
    { index: 0, label: 'Last Updated', value: '', type: 'default', checked: false },
    { index: 1, label: 'Challenge ID: 0 to 9', value: 'username', type: 'default', checked: false },
    { index: 2, label: 'Challenge ID: 9 to 0', value: '-username', type: 'default', checked: false },
  ];

  tabs = ['To Review', 'Deathmatch', 'Team Vs Team', 'Bets'];

  expandForm = false;
  size: any = '"default"';
  listOfDeathmatch: Deathmatch[] = [];
  total = 0;
  pageIndex = 1;
  pageSize = 30;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedUser = null;
  applyFilterFn = false;
  constructor(private multiplayerService: MultiplayerService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private gameService: GamesManagementService, private playOptionService: PlayOptionsService) { }

  ngOnInit(): void {
    this.loading = false;
    this.fetchAllGames();
    this.fetchAllPlayOptions();
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: any
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.multiplayerService.challengeAllList(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      console.log('listOfDeathmatch', data);
      this.total = data.length;
      // this.total = data.body.totalNumberOfChallenges;
      this.listOfDeathmatch = data;
    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }

  public fetchAllGames(): void {
    this.gameService.getAllGamesAsList().subscribe((res: any) => {
      if (res.responseStatus.success) {
        console.log('games data', res);
        this.gamesData = [];
        res.body.forEach((element: any) => {
          if (element.playerType === 'multiple') {
            this.gamesData.push(element);
          }
        });
        console.log('games data', this.gamesData);
      }
    }, (error) => {
      console.log('games data error', error);
    });
  }

  public fetchAllPlayOptions(): void {
    const obj = {'gameType': 'multiple'};
    this.playOptionService.getAllPlayOptionsAsList(obj).subscribe((res: any) => {
      if (res.responseStatus.success) {
        console.log('playOptions data', res);
        this.playOptionsData = [];
        this.playOptionsData = res.body;
        // res.body.forEach((element: any) => {
        //   if (element.type === 'dm') {
        //     this.playOptionsData.push(element);
        //   }
        // });
      }
    }, (error) => {
      console.log('playOption data error', error);
    });
  }


  public rowClick(data: any): void {
    console.log('data', data);
    this.selectedRow = data._id;
    if (this.selectedTab === 'Brackets') {
      this.drawerHelper.create('', MultiplayerDeathmatchDrawerComponent, { record: data, menuSelectedTab: this.selectedTab }).subscribe((res) => {
        console.log('back drawer response', res);
        this.msg.info(res);
      });
    } else{
      this.drawerHelper.create('', MultiplayerDeathmatchDrawerComponent, { record: data, menuSelectedTab: this.selectedTab }).subscribe((res) => {
        console.log('back drawer response', res);
        this.msg.info(res);
      });
    }
  }

  public reload(): void {
    if (this.q && this.filterObj !== null) {
      this.submitFilterForm(); return;
    } else {
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    }
    // if (!this.filterObj || this.filterObj == null) {
    //   this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
    //   this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    // } else {
    //   this.applyFilter(this.filterObj);
    // }

  }

  submitFilterForm(): void {
    console.log('filter data', this.q);

    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    if (this.q.page === this.pageIndex && this.q.perPage === this.pageSize){
      delete this.q.page;
      delete this.q.perPage;
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      this.q.page = this.pageIndex;
      this.q.perPage = this.pageSize;
      let formObj = filter;
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else {
    this.filterObj = null;
      return;
    }
  }

  selectTab(name: string): any {
    console.log('display content', name);
    this.selectedTab = name;
    if (this.selectedTab === 'Bets') {

    }
  }
  sortBy(event: any): void {

  }

  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    this.multiplayerService.filterMultiplayer(formObj).subscribe((data: any) => {
      console.log('deathmatch filtered data', data);
      if (data.length) {
        this.loading = false;
        this.filterBtnLoader = false;
        this.total = data.length; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfDeathmatch = data;
        this.applyFilterFn = true;
      } else {
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log(error);
    });
  }

  reset(): void {
    this.q = {};
    this.filterObj = null;
  }

  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }

  sortByTime(value: any): void {
    console.log(value);
  }

}
