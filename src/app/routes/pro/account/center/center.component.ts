import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { _HttpClient } from '@delon/theme';
import { Subscription, zip } from 'rxjs';
import { filter } from 'rxjs/operators';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzMenuModeType } from 'ng-zorro-antd/menu';
import { Observable, Observer } from 'rxjs';
import { NzUploadFile } from 'ng-zorro-antd/upload';

interface ProAccountSettingsUser {
  email: string;
  name: string;
  profile: string;
  country: string;
  address: string;
  phone: string;
  avatar: string;
  geographic: {
    province: {
      key: string;
    };
    city: {
      key: string;
    };
  };
}

interface ProAccountSettingsCity {
  name: string;
  id: string;
}


@Component({
  selector: 'app-account-center',
  templateUrl: './center.component.html',
  styleUrls: ['./center.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class ProAccountCenterComponent implements OnInit, OnDestroy {
  userData: any;
  userAdmin: any;
  avatarUrl?: string;
  loading= false;
  menu: any = {name: 'Profile Information'};
  constructor(private router: Router, private http: _HttpClient, private cdr: ChangeDetectorRef,
              private msg: NzMessageService) {
    // Retrieve the object from storage
       this.userData = localStorage.getItem('userData');
              }
              avatar = '';
              userLoading = true;
              users!: ProAccountSettingsUser;
            
              // #region geo
            
              provinces: ProAccountSettingsCity[] = [];
              cities: ProAccountSettingsCity[] = [];


              mode: NzMenuModeType = 'inline';
              title!: string;
              menus: Array<{ key: string; title: string; selected?: boolean }> = [
                {
                  key: 'Profile Information',
                  title: 'Profile Information',
                  selected: true
                },
                {
                  key: 'security Settings',
                  title: 'Security Settings',
                }
              ];

              
  private router$!: Subscription;
  @ViewChild('tagInput', { static: false }) private tagInput!: ElementRef<HTMLInputElement>;
  user: any;
  notice: any;
  tabs = [ 'Profile', 'Account Settings' ];
  selectedTab: any = 'Profile';

  pos = 0;
  taging = false;
  tagValue = '';
  openMap: { [name: string]: boolean } = {
    sub1: true,
    sub2: false,
    sub3: false
  };
  private setActive(): void {
    const key = this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
    // const idx = this.tabs.findIndex((w) => w.key === key);
    // if (idx !== -1) {
    //   this.pos = idx;
    // }
  }

  ngOnInit(): void {
    this.userAdmin =  JSON.parse(this.userData);
    console.log("user admin data",this.userAdmin);
    zip(this.http.get('/user/current'), this.http.get('/geo/province')).subscribe(
      ([user, province]: [ProAccountSettingsUser, ProAccountSettingsCity[]]) => {
        this.userLoading = false;
        this.user = user;
        this.provinces = province;
        this.choProvince(user.geographic.province.key, false);
        this.cdr.detectChanges();
      },
    );
    zip(this.http.get('/user/current'), this.http.get('/api/notice')).subscribe(([user, notice]) => {
      this.user = user;
      this.notice = notice;
      this.cdr.detectChanges();
    });
    this.router$ = this.router.events.pipe(filter((e) => e instanceof ActivationEnd)).subscribe(() => this.setActive());
    this.setActive();
  }


  choProvince(pid: string, cleanCity: boolean = true): void {
    this.http.get(`/geo/${pid}`).subscribe((res) => {
      this.cities = res;
      if (cleanCity) {
        this.user.geographic.city.key = '';
      }
      this.cdr.detectChanges();
    });
  }

  to(item: { key: string }): void {
    this.router.navigateByUrl(`/pro/account/center/${item.key}`);
  }
  tagShowIpt(): void {
    this.taging = true;
    this.cdr.detectChanges();
    this.tagInput.nativeElement.focus();
  }

  tagBlur(): void {
    const { user, cdr, tagValue } = this;
    if (tagValue && user.tags.filter((tag: { label: string }) => tag.label === tagValue).length === 0) {
      user.tags.push({ label: tagValue });
    }
    this.tagValue = '';
    this.taging = false;
    cdr.detectChanges();
  }

  tagEnter(e: KeyboardEvent): void {
    // tslint:disable-next-line: deprecation
    if (e.keyCode === 13) {
      this.tagBlur();
    }
  }
  selectTab(name: any): any{
    console.log('display content', name);
    this.selectedTab = name;
  }
  ngOnDestroy(): void {
    this.router$.unsubscribe();
  }


  openHandler(value: string): void {
    console.log(value);
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }
 menuClicked(value: any): void{
console.log(value.title);
 }


 save(): boolean {
  this.msg.success(JSON.stringify(this.user));
  return false;
}


menuChange(menuName: any): void{
console.log(menuName);
this.menu.name = menuName.title;
}
beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) =>
new Observable((observer: Observer<boolean>) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    this.msg.error('You can only upload JPG file!');
    observer.complete();
    return;
  }
  const isLt2M = file.size! / 1024 / 1024 < 2;
  if (!isLt2M) {
    this.msg.error('Image must smaller than 2MB!');
    observer.complete();
    return;
  }
  observer.next(isJpgOrPng && isLt2M);
  observer.complete();
});

private getBase64(img: File, callback: (img: string) => void): void {
const reader = new FileReader();
reader.addEventListener('load', () => callback(reader.result!.toString()));
reader.readAsDataURL(img);
}

handleChange(info: { file: NzUploadFile }): void {
switch (info.file.status) {
  case 'uploading':
    this.loading = true;
    break;
  case 'done':
    // Get this url from response in real world.
    this.getBase64(info.file!.originFileObj!, (img: string) => {
      this.loading = false;
      this.avatarUrl = img;
    });
    break;
  case 'error':
    this.msg.error('Network error');
    this.loading = false;
    break;
}
}
}
