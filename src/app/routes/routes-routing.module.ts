import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SimpleGuard } from '@delon/auth';
import { environment } from '@env/environment';
// layout
import { LayoutBasicComponent } from '../layout/basic/basic.component';
import { LayoutBlankComponent } from '../layout/blank/blank.component';
import { AuthGuard } from '../services/guard/auth.guard';

const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '', 
  // canActivate: [AuthGuard],
  pathMatch: 'full', loadChildren: () => import('./passport/passport.module').then((m) => m.PassportModule) },

  {
    path: '',
    component: LayoutBasicComponent,
    canActivate: [AuthGuard],
    // canActivateChild: [AuthGuard],
    // canActivate: [SimpleGuard],
    // canActivateChild: [SimpleGuard],
    data: {},
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then((m) => m.DashboardModule) },
      { path: 'widgets', loadChildren: () => import('./widgets/widgets.module').then((m) => m.WidgetsModule), },
      { path: 'style', loadChildren: () => import('./style/style.module').then((m) => m.StyleModule) },
      { path: 'delon', loadChildren: () => import('./delon/delon.module').then((m) => m.DelonModule) },
      { path: 'extras', loadChildren: () => import('./extras/extras.module').then((m) => m.ExtrasModule) },
      { path: 'pro', loadChildren: () => import('./pro/pro.module').then((m) => m.ProModule) },
      { path: 'user', loadChildren: () => import('./user/user.module').then((m) => m.UserModule) },
      { path: 'games-management', loadChildren: () => import('./games-management/games-management.module').then((m) => m.GamesManagementModule) },
      { path: 'challenges', loadChildren: () => import('./single-player/single-player.module').then((m) => m.SinglePlayerModule) },
      { path: 'transactions', loadChildren: () => import('./transactions/transactions.module').then((m) => m.TransactionsModule) },
      { path: 'multiplayer', loadChildren: () => import('./multiplayer/multiplayer.module').then((m) => m.MultiplayerModule) },
      { path: 'settings', loadChildren: () => import('./settings/settings.module').then((m) => m.SettingsModule) },
      { path: 'tournament-management', loadChildren: () => import('./tournament-management/tournament-management.module').then((m) => m.TournamentManagementModule) },
      { path: 'customer-support', loadChildren: () => import('./customer-support/customer-support.module').then((m) => m.CustomerSupportModule) },
      { path: 'la-leaderboard', loadChildren: () => import('./la-leaderboard/la-leaderboard.module').then((m) => m.LaLeaderboardModule) },
    ],
  },
  // Blak Layout 空白布局
  {
    path: 'data-v',
    component: LayoutBlankComponent,
    children: [{ path: '', loadChildren: () => import('./data-v/data-v.module').then((m) => m.DataVModule) }],
  },
  // passport
  // { path: '', loadChildren: () => import('./passport/passport.module').then((m) => m.PassportModule) },
  { path: 'exception', loadChildren: () => import('./exception/exception.module').then((m) => m.ExceptionModule) },
  { path: '**', redirectTo: 'exception/404' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
      // NOTICE: If you use `reuse-tab` component and turn on keepingScroll you can set to `disabled`
      // Pls refer to https://ng-alain.com/components/reuse-tab
      scrollPositionRestoration: 'top',
    }),
  ],
  exports: [RouterModule],
})
export class RouteRoutingModule {}
