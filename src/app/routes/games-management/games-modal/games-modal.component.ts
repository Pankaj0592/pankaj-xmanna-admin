import { Component, Input, OnInit, } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { Observable, Observer } from 'rxjs';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';

@Component({
  selector: 'app-games-modal',
  templateUrl: './games-modal.component.html',
  styleUrls: ['./games-modal.component.less'],
})
export class GamesModalComponent implements OnInit {

  @Input() data: any;
  gamesForm!: FormGroup;
  submitting = false;
  gameData: any;
  loading = false;
  avatarUrl?: string;

  genre: Array<{ value: string; label: string }> = [
    { value: 'Action', label: 'Action' },
    { value: 'Arcade', label: 'Arcade' },
    { value: 'Card Game', label: 'Card Game' },
    { value: 'Puzzle', label: 'Puzzle' },
    { value: 'Shooter', label: 'Shooter' },

    // { value: 'skill', label: 'Skill' },
    // { value: 'luck', label: 'luck' },
  ];
  developer: Array<{ value: string; label: string }> = [
    { value: 'dev1', label: 'Developer 1' },
    { value: 'dev2', label: 'Developer 2' },
  ];
  structure: Array<{ value: string; label: string }> = [
    { value: 'single', label: 'Single' },
    { value: 'multiple', label: 'Multiple' },
    { value: 'pvp', label: 'PvP' },
    { value: 'other', label: 'Other' },
  ];
  legalType: Array<{ value: string; label: string }> = [
    { value: 'luck', label: 'Luck-game' },
    { value: 'skill', label: 'Skill-game' },
  ];
  ageRestriction: Array<{ value: any; label: string }> = [
    { value: 'none', label: 'No restriction' },
    { value: '14+', label: '14+' },
    { value: '18+', label: '18+' },
  ];
  owner: Array<{ value: string; label: string }> = [
    { value: 'owner 1', label: 'Owner 1' },
    { value: 'owner2', label: 'Owner 2' },
  ];

  constructor(private fb: FormBuilder, private gameService: GamesManagementService, private msg: NzMessageService, private modal: NzModalRef) {
  }

  ngOnInit(): void {
    this.gameData = this.data.record;
    console.log('modal data', this.gameData);
    this.gamesForm = this.fb.group({
      name: [null, [Validators.required]],
      genre: [null, [Validators.required]],
      logo: [],
      playerType: [null, [Validators.required]],
      legalType: [null, [Validators.required]],
      restriction: [null, [Validators.required]],
      description: [null, [Validators.required]],
      developer: [null, [Validators.required]],
      isVerified: [false, [Validators.required]],
      isDeleted: [false, [Validators.required]],
      owner: [null],
      appStoreLink: [],
      playStoreLink: [],
      apkLink: [],
    });
    if (this.gameData) {
      this.formSetValue();
    }
  }

  /** =================== For Image upload (START) ================= */
  beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]) => {
    return new Observable((observer: Observer<boolean>) => {
      const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
      if (!isJpgOrPng) {
        this.msg.error('You can only upload JPG/PNG file!');
        observer.complete();
        return;
      }
      const isLt2M = file.size! / 1024 / 1024 < 1;
      if (!isLt2M) {
        this.msg.error('Image must smaller than 1MB!');
        observer.complete();
        return;
      }
      observer.next(isJpgOrPng && isLt2M);
      observer.complete();
    });
  }
  private getBase64(img: File, callback: (img: string) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result!.toString()));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: NzUploadFile }): void {
    console.log('image file status ====', info);
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        break;
      case 'done':
        console.log('status', info.file.status);
        console.log('file value', info.file!.originFileObj!);
        let file = info.file!.originFileObj!;
        this.gamesForm.patchValue({ logo: file });
        this.gamesForm.controls.logo.updateValueAndValidity();
        console.log('logo', this.gamesForm.controls['logo'].value);
        // Get this url from response in real world.
        this.getBase64(info.file!.originFileObj!, (img: string) => {
          this.loading = false;
          this.avatarUrl = img;
        });
        break;
      case 'error':
        this.msg.error('Image must smaller than 1MB!');
        this.loading = false;
        break;
    }
  }
  /** =================== For Image upload (END) ================= */


  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void {
    console.log('function works');
    this.submitting = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    if (!this.gameData) {
      // const formData: any = new FormData();
      // console.log(this.gamesForm.controls.logo);
      // formData.append('logo', this.gamesForm.controls.logo.value);
      // formData.append('name', this.gamesForm.controls.name.value);
      // formData.append('genre', this.gamesForm.controls.genre.value);
      // formData.append('status', 'active');
      // formData.append('playerType', this.gamesForm.controls.playerType.value);
      // formData.append('isVerified', false);
      // formData.append('isDeleted', false);
      // formData.append('developer', this.gamesForm.controls.developer.value);
      // formData.append('appStoreLink', this.gamesForm.controls.appStoreLink.value);
      // formData.append('playStoreLink', this.gamesForm.controls.playStoreLink.value);
      // formData.append('apiAuthKey', '');
      // formData.append('description', this.gamesForm.controls.description.value);
      // formData.append('instanceCount', 0);
      // console.log('formdata', formData);
      // // Display the key/value pairs
      // for (const pair of formData.entries()) {
      //   console.log(pair[0] + ', ' + pair[1]);
      // }
      const formData: any = new FormData();
      console.log(this.gamesForm.controls.logo);
      formData.append('logo', this.gamesForm.controls.logo.value);
      formData.append('name', formValue.name);
      formData.append('genre', formValue.genre);
      formData.append('status', 'active');
      formData.append('playerType', formValue.playerType);
      formData.append('isVerified', false);
      formData.append('isDeleted', false);
      formData.append('developer', formValue.developer);
      formData.append('appStoreLink', formValue.appStoreLink);
      formData.append('playStoreLink', formValue.playStoreLink);
      formData.append('apiAuthKey', '');
      formData.append('description', formValue.description);
      formData.append('instanceCount', 0);
      console.log('formdata', formData);
      // Display the key/value pairs
      for (const pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
      }
// return;
      this.gameService.addGames(formData).subscribe((res: any) => {
        if (res.responseStatus.success) {
          this.submitting = false;
          console.log('data submit successfully', res);
          // this.resetForm();
          this.destroyModal(true);
        } else {
          this.submitting = false;
        }
      }, (error) => {
        this.submitting = false;
        console.log('error', error);
      });

    } else {
      const formObj = {
        name: formValue.name,
        genre: formValue.genre,
        status: 'active',
        // instanceCount: formValue.instanceCount,
        playerType: formValue.playerType,
        isVerified: JSON.parse(formValue.isVerified),
        isDeleted: JSON.parse(formValue.isDeleted),
        appStoreLink: formValue.appStoreLink,
        playStoreLink: formValue.playStoreLink,
        developer: formValue.developer,
        description: formValue.description,
      };
      console.log(formObj);
      this.gameService.editGames(this.gameData._id, formObj).subscribe((res: any) => {
        if (res.responseStatus.success) {
          console.log('updated successfully', res);
          this.submitting = false;
          this.gameData = res.body;
          this.destroyModal(this.gameData);
          // this.formSetValue();
        } else {
          this.submitting = false;
        }
      }, (error) => {
        this.submitting = false;
        console.log(error);
      });
    }
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }


  formSetValue(): void {
    this.avatarUrl = this.gameData.logo;
    this.gamesForm.controls['name'].setValue(this.gameData.name);
    this.gamesForm.controls['genre'].setValue(this.gameData.genre);
    this.gamesForm.controls['description'].setValue(this.gameData.description);
    // this.gamesForm.controls['status'].setValue(this.gameData.status);
    this.gamesForm.controls['playerType'].setValue(this.gameData.playerType);
    this.gamesForm.controls['playStoreLink'].setValue(this.gameData.playStoreLink);
    this.gamesForm.controls['appStoreLink'].setValue(this.gameData.appStoreLink);
    // this.gamesForm.controls['apkLink'].setValue(this.gameData.playStoreLink);
  }
}
