import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';

@Component({
  selector: 'app-play-options-modal',
  templateUrl: './play-options-modal.component.html',
  styleUrls: ['./play-options-modal.component.less'],
})
export class PlayOptionsModalComponent implements OnInit {

  @Input() data: any;
  form!: FormGroup;
  playOptionsForm!: FormGroup;
  submitting = false;
  submitted = false;
  gameData: any;
  loading = false;
  avatarUrl?: string;
  rewardQuantity!: FormArray;
  dataSource: any;
  round: any;
  USDCount: any = 0;
  trophiesTotal: any = 0;
  rewardCount = 0;
  experienceTotal: any = 0;
  ribbonsTotal: any = 0;
  netPool = 0;
  prizeTotalFlag = false;
  rake = 20;
  winnerAlgo: any = {
    2: [100, 0],
    3: [70, 30, 0],
    4: [70, 30, 0, 0],
    6: [70, 30],
    8: [10, 25, 65],
    16: [10, 15, 20, 55],
    32: [5, 10, 15, 20, 50]
  };

  bracketPlayers: any[] = [{ 'label': 4, 'value': 4 }, { 'label': 8, 'value': 8 }, { 'label': 16, 'value': 16 }, { 'label': 32, 'value': 32 },];
  deathmatchPlayers: any[] = [{ 'label': 3, 'value': 3 }, { 'label': 4, 'value': 4 }];

  constructor(private fb: FormBuilder, private msg: NzMessageService, private modal: NzModalRef, private playOptionService: PlayOptionsService) { }


  ngOnInit(): void {
    this.gameData = this.data.record;
    console.log('modal data', this.gameData);

    this.playOptionsForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      playOptionType: [this.gameData.playerType === 'multiple' ? 'tvt' : 'challenge', Validators.required],
      playMode: ['m'],
      playersCount: [2, [Validators.required]],
      isExperience: [true],
      isTrophies: [true],
      isUSD: [true],
      xCoins: [true],
      ribbons: [true],
      positionIndex: [0, [Validators.required]],
      prizeTotal: [0, [Validators.required]],
      fee: [6, [Validators.required]],
      regOnly: [false],
      timeLimit: [true],
      fillLimit: [24],
      roundCount: [2],
      isSingleWinner: [false],
      description: [null],
      rewardQuantity: this.fb.array([]),
      USDTotal: [0],
      trophiesTotal: [0],
      experienceTotal: [0],
      ribbonsTotal: [0],
    });

    this.rewardQuantity = this.playOptionsForm.get('rewardQuantity') as FormArray;
    this.valueChangeSubscription();
    if (this.gameData) {
      this.formSetvalue();
    }
    this.createDataSource();
  }


  /** rewardQuantity array create and set functions */
  createRewardQty(reward: any): FormGroup {
    return this.fb.group({
      round: [reward.round , {disabled: true}],
      USD: [{ value : reward.USD ? (reward.USD % 1 ? reward.USD.toFixed(2) : Math.floor(reward.USD)) : 0, disabled: false}],
      trophies: [reward.trophies ? Math.floor(reward.trophies) : 0],
      xCoins: [reward.xCoins ? (reward.xCoins % 1 ? reward.xCoins.toFixed(2) : Math.floor(reward.xCoins)) : 0],
      ribbons: [reward.ribbons ? Math.floor(reward.ribbons) : 0],
      experience: [ { value : reward.experience ? Math.floor(reward.experience) : 0, disabled: !this.playOptionsForm.controls['isExperience'].value }]
    });
  }
  private setRewardQtyForm(): void {
    this.rewardQuantity = this.playOptionsForm.get('rewardQuantity') as FormArray;
    this.dataSource.forEach((reward: any) => {
      console.log(reward);
      this.rewardQuantity.push(this.createRewardQty(reward));
    });
    console.log('Reward qty', this.rewardQuantity);
  }

  clearFormArray = (formArray: FormArray) => {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
  }
  /** END */


  playerCountChanged(): void {
    console.log('player count changed called');
    let playerCount = this.playOptionsForm.controls['playersCount'].value;
    let playOptionType = this.playOptionsForm.controls['playOptionType'].value;
    console.log(playerCount);
    if (playOptionType === 'dm') {
      if (this.playOptionsForm.controls.playersCount.value == 3) {
        this.playOptionsForm.controls['roundCount'].setValue(3);
      } else if (this.playOptionsForm.controls.playersCount.value == 4) {
        this.playOptionsForm.controls['roundCount'].setValue(4);
      } else if (this.playOptionsForm.controls.playersCount.value == 8) {
        this.playOptionsForm.controls['roundCount'].setValue(8);
      } else if (this.playOptionsForm.controls.playersCount.value == 16) {
        this.playOptionsForm.controls['roundCount'].setValue(16);
      } else if (this.playOptionsForm.controls.playersCount.value == 32) {
        this.playOptionsForm.controls['roundCount'].setValue(32);
      }
    } else {
      if (this.playOptionsForm.controls.playersCount.value == 4) {
        this.playOptionsForm.controls['roundCount'].setValue(2);
      } else if (this.playOptionsForm.controls.playersCount.value == 8) {
        this.playOptionsForm.controls['roundCount'].setValue(3);
      } else if (this.playOptionsForm.controls.playersCount.value == 16) {
        this.playOptionsForm.controls['roundCount'].setValue(4);
      } else if (this.playOptionsForm.controls.playersCount.value == 32) {
        this.playOptionsForm.controls['roundCount'].setValue(5);
      }
    }
    this.createDataSource();
  }

  entryFeeChange(fee: number): void {
    this.createDataSource();
  }

  playOptionTypeChange(type: string): void {
    console.log('playOption type', type);
  }

  valueChangeSubscription(): void {
    this.playOptionsForm.controls['fillLimit'].disable();
    // this.playOptionsForm.controls['roundCount'].disable();
    if (this.playOptionsForm.controls['playOptionType'].value === 'tvt') {
      this.playOptionsForm.controls.playersCount.setValue(6);
    }
    this.playOptionsForm.controls['playOptionType'].valueChanges.subscribe((playOptionType: string) => {
      switch (playOptionType) {
        case (playOptionType = 'brackets'):
          this.playOptionsForm.controls.playersCount.setValue(8);
          break;
        case (playOptionType = 'tvt'):
          this.playOptionsForm.controls.playersCount.setValue(6);
          break;
        case (playOptionType = 'challenge'):
          this.playOptionsForm.controls.playersCount.setValue(2);
          break;
        case (playOptionType = 'dm'):
          this.playOptionsForm.controls.playersCount.setValue(4);
          break;
        default:
          alert('out of range');
          break;
      }
    });
    this.playOptionsForm.controls['timeLimit'].valueChanges.subscribe((res: boolean) => {
      console.log(res);
      if (res) {
        this.playOptionsForm.controls['fillLimit'].disable();
        // this.playOptionsForm.controls['roundCount'].disable();
      } else {
        this.playOptionsForm.controls['fillLimit'].enable();
        this.playOptionsForm.controls['roundCount'].enable();
      }
    });
    this.playOptionsForm.controls['isExperience'].valueChanges.subscribe((res: boolean) => {
        this.createDataSource();
    });
    this.playOptionsForm.controls['isTrophies'].valueChanges.subscribe((res: boolean) => {
      this.createDataSource();
    });
    this.playOptionsForm.controls['ribbons'].valueChanges.subscribe((res: boolean) => {
      this.createDataSource();
    });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save playOptions edit form
   */
  save(isValid: any, formValue: any): any {
    this.submitted = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }

    if (this.playOptionsForm.controls['playMode'].value === 'm') {
      this.rewardCount = 0;
      this.rewardCount = this.playOptionsForm.controls['isUSD'].value ? this.rewardCount + 1 : this.rewardCount;
      this.rewardCount = this.playOptionsForm.controls['isTrophies'].value ? this.rewardCount + 1 : this.rewardCount;
      this.rewardCount = this.playOptionsForm.controls['isExperience'].value ? this.rewardCount + 1 : this.rewardCount;
    } else {
      this.rewardCount = 0;
      this.rewardCount = this.playOptionsForm.controls['xCoins'].value ? this.rewardCount + 1 : this.rewardCount;
      this.rewardCount = this.playOptionsForm.controls['ribbons'].value ? this.rewardCount + 1 : this.rewardCount;
      this.rewardCount = this.playOptionsForm.controls['isExperience'].value ? this.rewardCount + 1 : this.rewardCount;
    }

    this.rewardQuantity.value.forEach((controls: any) => {
      console.log(controls);
      if (!this.playOptionsForm.controls['isTrophies'].value) {
        controls.trophies = 0;
      } 
      if (!this.playOptionsForm.controls['isExperience'].value) {
        controls.experience = 0;
      } 
      if (!this.playOptionsForm.controls['ribbons'].value) {
        controls.ribbons = 0;
      }
    });
    console.log('reward qty', this.rewardQuantity.value);

    const formObj = {
      game: this.gameData._id,
      name: formValue.name,
      status: 'active',
      isBracket: false,
      genre: 'skill',
      positionIndex: formValue.positionIndex,
      // playerType: formValue.playOptionType,
      type: formValue.playOptionType,
      playerCount: JSON.parse(formValue.playersCount),
      roundCount: formValue.roundCount,
      fee: formValue.fee,
      rake: 20,
      rewardCount: this.rewardCount,
      playMode: formValue.playMode,
      isSingleWinner: true,
      // isSingleWinner: JSON.parse(formValue.isSingleWinner),
      winnerAlgorithm: null,
      gamePlay: null,
      description: formValue.description,
      // isPrizes: formValue.isPrizes,
      rewardQuantity: this.rewardQuantity.value,
      // regOnly: formValue.regOnly,
      // fillLimit: formValue.fillLimit,
      rewardTypes: {
        isUSD: formValue.playMode === 'm' ? formValue.isUSD : false,
        isTrophies: formValue.playMode === 'm' ? formValue.isTrophies : false,
        isExperience: formValue.isExperience,
        xCoins: formValue.playMode === 'p' ? formValue.xCoins : false,
        ribbons: formValue.playMode === 'p' ? formValue.ribbons : false,
      },
    };
    console.log('formvalue', formObj);
    this.submitted = false;
return;
    this.playOptionService.createPlayOptions(formObj).subscribe((res: any) => {
      console.log('res of playOption create', res);
      if (res.responseStatus.success){
        this.destroyModal(res.body);
      } else{
        console.log('something went wrong while creating', res);
      }
    }, (error) => {
      console.log('error', error);
    });

  }
  formSetvalue(): void {
    // this.form.controls['name'].setValue(this.gameData.name);
    // this.form.controls['genre'].setValue(this.gameData.genre);
    // this.form.controls['description'].setValue(this.gameData.description);
  }

prizeTotal(totalPool: number): any{
  console.log('total Pool', totalPool);
  const playerCount = this.playOptionsForm.controls['playersCount'].value;
  const fee = this.playOptionsForm.controls['fee'].value;
  if (totalPool <= playerCount * fee){
    this.prizeTotalFlag = true;
    console.log('rake total pool', totalPool);
    this.createDataSource();
  } else{
    alert('total pool ===');
    // this.playOptionsForm.controls['prizeTotal'].setValue(totalPool);
    // this.playOptionsForm.controls['prizeTotal'].setErrors({'error': true});
  }
}

total(event: any): any {
if(event.target.value > this.netPool){
event.target.value = this.netPool;
}
}

numberFixed(event: any): any {
let value = event.target.value;
if(value % 1){
  value = Math.floor(value);
  event.target.value = value;
  this.calculateTotalPrize();
}
}

  createDataSource(): void {
    /**
     * This function should auto trigger on basis of players Count, fee, rake, isBracket, reward Types
     */
    let playOptionType = this.playOptionsForm.controls['playOptionType'].value;
    console.log('bracket', playOptionType);

    let playerCount = this.playOptionsForm.controls['playersCount'].value;
    console.log('playerCount', playerCount);


    // let roundCount = 2;
    let roundCount = this.playOptionsForm.controls['roundCount'].value;
    if (!roundCount || roundCount < 2) {
      return;
    }
    if (playerCount == 6) {
      roundCount = 2;
    }
    console.log('roundCount', roundCount);
    this.round = roundCount;

    let mode = this.playOptionsForm.controls['playMode'].value;
    let fee = this.playOptionsForm.controls['fee'].value;
    console.log('mode and fee', mode, fee);


    // let rake = this.playOptionsForm.controls['rake'].value;
    let rake = 20;
    if (isNaN(rake) || rake < 0) {
      return;
    }

    let isUSD = this.playOptionsForm.controls['isUSD'].value;
    let isTrophies = this.playOptionsForm.controls['isTrophies'].value;

    let isExp = this.playOptionsForm.controls['isExperience'].value;

    let isXCoins = this.playOptionsForm.controls['xCoins'].value;
    let isRibbons = this.playOptionsForm.controls['ribbons'].value;

    if (mode === 'm' && !isUSD) {
      return;
    }

    if (mode === 'p' && !isXCoins) {
      return;
    }
    let setNetPool = true;
    if (this.prizeTotalFlag){
      let prizePool = this.playOptionsForm.controls['prizeTotal'].value;
      console.log('prizePool', prizePool);
      this.prizeTotalFlag = false;
      const netPool = fee * playerCount;
      const poolPercentage =  netPool - prizePool; 
      console.log('poolPercentage', poolPercentage);
      rake = (poolPercentage / prizePool) * 100;
      console.log('rake%', rake);
      this.rake = rake;
      this.prizeTotalFlag = false;
      setNetPool = false;
      // return;
    } 
    if (isNaN(rake) || rake < 0) {
      return;
    }
    let totalPool = fee * playerCount; // this equals to 100 + rake %
    // rake = rake ? rake : 25;
    let totalPercent = 100 + rake;
    let singleX = (totalPool) / totalPercent;
    let rakePool = rake * singleX;
    let netPool = 100 * singleX;
    this.netPool = netPool;
    console.log('netPool', netPool);

    if (setNetPool){
      this.playOptionsForm.controls['prizeTotal'].setValue(netPool);
      // this.playOptionsForm.controls.prizeTotal.setValue(netPool);
      console.log('prizeTotal flag', this.prizeTotalFlag);
    }
    // this.playOptionsForm.controls.prizeTotal.setValue(netPool);

// return;
    this.clearFormArray(this.rewardQuantity);
    this.dataSource = [];

    let rewardArray: any = this.winnerAlgo[playerCount];
    for (let i = 0; i < (playOptionType === 'brackets' ? roundCount + 1 : roundCount); ++i) {
      let obj: any = {};
      if (mode === 'm') {
        obj['round'] = i + 1;
        obj['USD'] = (rewardArray[i] / 100) * netPool;
        if (isTrophies) {
          obj['trophies'] = Math.floor(obj['USD']);
          // obj['trophies'] = obj['USD'] * 10;
        }
        if (isExp && playOptionType === 'challenge') {
          let round = obj['round'] == 1 ? 2 : 1;
          obj['experience'] = round * 10;
          // obj['experience'] = obj['USD'];
        }
        if (isExp && playOptionType === 'dm') {
          let round = this.round == 4 ? 4 : 3;
          obj['experience'] = (round - i) * 10;
          // obj['experience'] = obj['USD'];
        }

      } else if (mode === 'p') {
        obj['round'] = i + 1;
        obj['xCoins'] = (rewardArray[i] / 100) * netPool;
        if (isRibbons) {
          obj['ribbons'] = Math.floor(obj['xCoins']);
          // obj['ribbons'] = obj['xCoins'] * 10;
        }
        if (isExp && playOptionType === 'challenge') {
          let round = obj['round'] == 1 ? 2 : 1;
          obj['experience'] = round;
        }
        if (isExp && playOptionType === 'dm') {
          let round = this.round == 4 ? 4 : 3;
          obj['experience'] = (round - i);
          // obj['experience'] = obj['USD'];
        }

      }
      this.dataSource.push(obj);
    }
    console.log('DataSource', this.dataSource);
    this.setRewardQtyForm();
    this.calculateTotalPrize();


  }

    /*========= COUNT TOTAL OF PRIZE AS PER REWARD TYPES (OPEN)========= */
  calculateTotalPrize(): void {
    if (this.dataSource) {
      // this.dataSource = this.rewardQuantity.value;
      this.USDCount = 0; this.trophiesTotal = 0; this.experienceTotal = 0; this.ribbonsTotal = 0;
      this.rewardQuantity.value.forEach((element: any) => {
        if (element.USD || element.trophies && !isNaN(element.experience)) {
          this.USDCount = this.USDCount + element.USD;
          this.trophiesTotal = this.trophiesTotal + element.trophies;
          // this.experienceTotal = this.experienceTotal + element.experience;
          // this.ribbonsTotal = this.ribbonsTotal + element.ribbons;
        }
        // this.USDCount = 0; this.experienceTotal = 0; this.ribbonsTotal = 0;
        if (element.xCoins || element.ribbons && !isNaN(element.experience)) {
          this.USDCount = this.USDCount + element.xCoins;
          // this.experienceTotal = this.experienceTotal + element.experience;
          this.ribbonsTotal = this.ribbonsTotal + element.ribbons;
        }
        if (element.experience){
          this.experienceTotal = this.experienceTotal + element.experience;
        }
      });
      console.log('usd trophies experience total DataSource', this.USDCount);
      console.log('usd trophies experience total DataSource', this.trophiesTotal);
      console.log('usd trophies experience total DataSource', this.experienceTotal);
      console.log('usd trophies experience total DataSource', this.ribbonsTotal);
      // return;
      console.log('netPool', this.netPool);
      const netPool = this.netPool;
      if (netPool < this.USDCount || netPool < this.trophiesTotal || netPool < this.ribbonsTotal){
       console.log('if block');
        this.createDataSource();
      } else{
       console.log('else block');

      this.playOptionsForm.controls['USDTotal'].setValue(this.USDCount);
      this.playOptionsForm.controls['trophiesTotal'].setValue(Math.floor(this.trophiesTotal));
      this.playOptionsForm.controls['experienceTotal'].setValue(Math.floor(this.experienceTotal));
      this.playOptionsForm.controls['ribbonsTotal'].setValue(Math.floor(this.ribbonsTotal));
      }
    }
  }
    /*========= COUNT TOTAL OF PRIZE AS PER REWARD TYPES (CLOSE)========= */

  tableReset(): void {
    this.loading = true;
    this.createDataSource();
    setTimeout(() => {
      this.loading = false;
    }, 400);
    // this.clearFormArray(this.rewardQuantity);
    // this.dataSource = [];
    // this.setRewardQtyForm();
  }

  closeModal(): void {
    this.modal.destroy();
  }

  destroyModal(obj: any): void {
    this.modal.destroy(obj);
  }
}
