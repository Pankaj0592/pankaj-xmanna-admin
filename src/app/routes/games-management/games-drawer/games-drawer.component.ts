import { Component, Input, OnInit } from '@angular/core';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { I18NService } from '@core';
import { STColumn } from '@delon/abc/st';
import { yuan } from '@shared';
import { getTimeDistance } from '@delon/util/date-time';
import { deepCopy } from '@delon/util/other';
import { NzMessageService } from 'ng-zorro-antd/message';
import { _HttpClient, ModalHelper } from '@delon/theme';
import { GamesModalComponent } from '../games-modal/games-modal.component';
import { PlayOptionsModalComponent } from '../play-options-modal/play-options-modal.component';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-games-drawer',
  templateUrl: './games-drawer.component.html',
  styleUrls: ['./games-drawer.component.less'],
})
export class GamesDrawerComponent implements OnInit {

  @Input() record: any;
  playOptionsData: any;
  tabs = [
    'Games Info',
    'Play Options',
    'Financial Stats',
    'Admin Actions',
  ];

  archive = false;
  status = 'success';
  data: any = {};
  loading = true;
  date_range: Date[] = [];
  nzOffsetBottom = 400;

  rankingListData: Array<{ title: string; total: number }> = Array(7)
    .fill({})
    .map((_, i) => {
      return {
        title: this.i18n.fanyi('app.analysis.test', { no: i }),
        total: 323234,
      };
    });

  titleMap = {
    y1: this.i18n.fanyi('app.analysis.traffic'),
    y2: this.i18n.fanyi('app.analysis.payments'),
  };
  searchColumn: STColumn[] = [
    {
      title: { text: '排名', i18n: 'app.analysis.table.rank' },
      index: 'index',
    },
    {
      title: { text: '搜索关键词', i18n: 'app.analysis.table.search-keyword' },
      index: 'keyword',
      click: (item) => this.msg.success(item.keyword),
    },
    {
      type: 'number',
      title: { text: '用户数', i18n: 'app.analysis.table.users' },
      index: 'count',
      sort: {
        compare: (a, b) => a.count - b.count,
      },
    },
    {
      type: 'number',
      title: { text: '周涨幅', i18n: 'app.analysis.table.weekly-range' },
      index: 'range',
      render: 'range',
      sort: {
        compare: (a, b) => a.range - b.range,
      },
    },
  ];

  salesType = 'all';
  salesPieData: any;
  salesTotal = 0;

  saleTabs: Array<{ key: string; show?: boolean }> = [
    { key: 'sales', show: true },
    { key: 'visits' },
  ];

  offlineIdx = 0;

  panels = [
    {
      active: false,
      disabled: false,
      name: 'ETH',
    },
    {
      active: true,
      disabled: false,
      name: 'USDT',
    },
    {
      active: false,
      disabled: true,
      name: 'TRX',
    },
    {
      active: false,
      disabled: false,
      name: 'BTC',
    },
  ];

  playOptionsSinglePanel = [
    {
      active: true,
      disabled: false,
      name: 'Brackets',
    },
    {
      active: false,
      disabled: false,
      name: 'Player vs Player',
    },
    {
      active: false,
      disabled: false,
      name: 'Practice',
    },
  ];

  playOptionsMultiplePanel = [
    {
      active: true,
      disabled: false,
      name: 'Paid Deathmatch',
    },
    {
      active: true,
      disabled: false,
      name: 'Practice Deathmatch',
    },
    {
      active: false,
      disabled: false,
      name: 'Paid Team vs Team',
    },
    {
      active: false,
      disabled: false,
      name: 'Practice Team vs Team',
    }
  ];

  constructor(
    private drawerRef: NzDrawerRef<string>,
    private http: _HttpClient,
    public msg: NzMessageService,
    private i18n: I18NService,
    private cdr: ChangeDetectorRef,
    private modalHelper: ModalHelper,
    private playOptions: PlayOptionsService,
    private modalService: NzModalService,
  ) {
  }

  ngOnInit(): void {
    console.log('drawer value', this.record);
    this.getPlayOptions();
  }


  public getPlayOptions(): void {
    const filter = { game: this.record._id };
    const formObj = { filter };
    this.playOptions.getAllPlayOptions(formObj).subscribe((res: any) => {
      console.log('playOptions data', res.body);
      if (res.responseStatus.success && res.body.totalnumberOfPlayOptions) {
        this.playOptionsData = res.body.playOptions;
        this.playOptionsData.total = res.body.totalnumberOfPlayOptions;
      }
    }, (error) => {
      console.log(error);
    });
  }


  public tabSelect(value: any): void {
    this.archive = false;
    if (value === this.tabs[4]) {
      this.http.get('/chart').subscribe((res) => {
        res.offlineData.forEach((item: any, idx: number) => {
          item.show = idx === 0;
          item.chart = deepCopy(res.offlineChartData);
          console.log('chart data', item.chart);
        });
        this.data = res;
        this.loading = false;
        this.changeSaleType();
      });
    }
  }
  setDate(type: 'today' | 'week' | 'month' | 'year'): void {
    this.date_range = getTimeDistance(type);
    setTimeout(() => this.cdr.detectChanges());
  }

  changeSaleType(): void {
    this.salesPieData =
      this.salesType === 'all'
        ? this.data.salesTypeData
        : this.salesType === 'online'
          ? this.data.salesTypeDataOnline
          : this.data.salesTypeDataOffline;
    if (this.salesPieData) {
      this.salesTotal = this.salesPieData.reduce(
        (pre: number, now: { y: number }) => now.y + pre,
        0
      );
    }
    this.cdr.detectChanges();
  }

  handlePieValueFormat(value: string | number): string {
    return yuan(value);
  }
  salesChange(idx: number): void {
    if (this.saleTabs[idx].show !== true) {
      this.saleTabs[idx].show = true;
      this.cdr.detectChanges();
    }
  }
  offlineChange(idx: number): void {
    if (this.data.offlineData[idx].show !== true) {
      this.data.offlineData[idx].show = true;
      this.cdr.detectChanges();
    }
  }

  public editGameInfo(): void {
    this.modalHelper.open(GamesModalComponent, { data: { title: 'Edit Game Info', titleI18n: 'page-name', record: this.record } }, 'md').subscribe((res: any) => {
      console.log('res', res);
      if (res) {
        this.record = res.data;
        console.log('record', this.record);
      }
    });
  }

  public addPlayOption(): void {
    this.modalHelper.open(PlayOptionsModalComponent, { data: { title: 'New Play Option', titleI18n: 'page-name', record: this.record } }, 'md').subscribe((res: any) => {
      // this.msg.info(res);
      if (res){
        this.playOptionsData.push(res);
      }
    });
  }

  playOptionDisable(playOption: any, sno: number){
console.log('playOption data to disable', playOption, sno);
this.playOptionsData.splice(sno,1)

  }

  public editPlayOption(): void {
    this.modalHelper.open(PlayOptionsModalComponent, { data: { title: 'Edit Play Option', titleI18n: 'page-name', record: this.record } }, 'md').subscribe((res: any) => {
      // this.msg.info(res);
    });
  }
  onChange(status: boolean): void {
    console.log(status);
  }
  public maintenance(): void {
    const modal = this.modalService.warning({
      nzTitle: 'Maintenance for ' + this.record.name,
      nzClosable: false,
      nzCentered: true,
      nzAutofocus: null,
      nzComponentParams: {
        subtitle: 'Set a timer for maintenance start and use your password to confirm.',
        data: this.record,
        status: false,
      },
      nzContent: NzModalCustomFooterComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
        this.record.securityStatus = result.data.securityStatus;
      }
    });
  }

  public disable(): void {
    const modal = this.modalService.warning({
      nzTitle: 'Disable ' + this.record.name,
      nzClosable: false,
      nzCentered: true,
      nzAutofocus: null,
      nzComponentParams: {
        subtitle: 'What should we do with the open challenges and brackets?',
        data: this.record,
        status: true,
      },
      nzContent: NzModalCustomFooterComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
        this.record.securityStatus = result.data.securityStatus;
      }
    });
  }

  cancelDisablement(): void {
    const modal = this.modalService.warning({
      nzTitle: 'Cancel disablement?',
      nzClosable: false,
      nzCentered: true,
      nzAutofocus: null,
      nzComponentParams: {
        subtitle: 'Disablement for ' + this.record.name + ' is planned to start in 19:59:54. Sure you want to cancel it?',
        data: this.record,
        status: true,
      },
      nzContent: NzModalCustomFooter1Component,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
        this.record.securityStatus = result.data.securityStatus;
      }
    });
  }


  cancelMaintenance(): void {
    const modal = this.modalService.warning({
      nzTitle: 'Cancel maintenance?',
      nzClosable: false,
      nzCentered: true,
      nzAutofocus: null,
      nzComponentParams: {
        subtitle: 'Maintenance for ' + this.record.name + ' is planned to start in 19:59:54. Sure you want to cancel it?',
        data: this.record,
        status: false,
      },
      nzContent: NzModalCustomFooter1Component,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
        this.record.securityStatus = result.data.securityStatus;
      }
    });
  }

  public unarchive(): void {
    console.log('unarchive works');
  }

  openArchive(): void {
    console.log('archive clicked');
    this.archive = true;
  }

  onBack(): void {
    console.log('onBack');
    this.archive = false;
  }

  close(): void {
    this.drawerRef.close(this.record);
  }

}





@Component({
  selector: 'nz-modal-custom-footer1-component',
  template: `
  <div>
  <h4>{{ subtitle }}</h4>
    <div class="modal_footer text-right mt20">
      <button nz-button class="ml-sm" type="reset" (click)="destroyModal()">Cancel</button>
      <button  nz-button nzType="primary"><span *ngIf="!status">Confirm Cancellation</span><span *ngIf="status">Confirm Cancellation</span></button>
    </div>
    <!-- <button nz-button [nzType]="'success'" (click)="destroyModal()">success</button> -->
  <!-- </p> -->

</div>
  `,
  styles: [`
    nz-input-number {
      width: 146px!important;
    }
    [nz-radio] {
      display: block;
      height: 32px;
      line-height: 32px;
    }
  `],
})
export class NzModalCustomFooter1Component implements OnInit {

  @Input() subtitle: any;
  @Input() data: any;
  @Input() status: any;
  confirmForm!: FormGroup;
  passwordVisible = false;

  constructor(private modal: NzModalRef, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.confirmForm = this.fb.group({
      timer: ['', Validators.required],
      freeze: [''],
      password: ['', Validators.required],
      timerUnit: ['m',],
    });
  }

  bothPlayerSelected(bothPlayerValue: any): void {

  }

/**
 *
 * @param isValid
 * @param formValue
 * to save confirmForm
 */

  save(isValid: any, formValue: any): void {
    console.log('function works');
    // this.submitting = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
  }

  destroyModal(): void {
    this.modal.destroy();
  }
}





@Component({
  selector: 'nz-modal-custom-footer-component',
  template: `
  <div>
  <h4>{{ subtitle }}</h4>
  <form [formGroup]="confirmForm" (ngSubmit)="save(confirmForm.valid, confirmForm.value)">
  <div *ngIf="status">
  <nz-radio-group formControlName="freeze"  (ngModelChange)="bothPlayerSelected($event)">
    <label nz-radio [nzValue]="'true'">Freeze games</label>
    <label nz-radio [nzValue]="'false'">Cancel and refund rakes</label>
  </nz-radio-group>
</div>
  <div class="row d-flex flex-row"  *ngIf="!status"><h4>Start In</h4> &nbsp; <span nz-typography nzType="secondary">(leave as 0 to start instantly)</span></div>
  <div class="row d-flex flex-row"  *ngIf="status"><h4>Disable in</h4> &nbsp; <span nz-typography nzType="secondary">(leave as 0 to disable instantly)</span></div>
    <nz-form-item>
    <nz-form-control [nzSm]="12" [nzXs]="20"  nzValidatingTip="Validating..." [nzErrorTip]="timerErrorTpl">
      <nz-input-number formControlName="timer" [nzMin]="0" [nzStep]="1" style="width:220px;"></nz-input-number>
      <ng-template #timerErrorTpl let-control>
        <ng-container *ngIf="control.hasError('required')">
          Please input fee!
        </ng-container>
        <ng-container *ngIf="control.hasError('duplicated')">
          The fee is redundant!
        </ng-container>
      </ng-template>
    </nz-form-control>

    <nz-form-control [nzSm]="12" [nzXs]="26">
      <nz-radio-group formControlName="timerUnit">
        <label nz-radio-button nzValue="m">Minutes</label>
        <label nz-radio-button nzValue="h">Hours</label>
      </nz-radio-group>
    </nz-form-control>
  </nz-form-item>


      <nz-form-label class="mt-0" [nzNoColon]='true' nzTooltipTitle="Enter your password">
        <span>Your password</span>
      </nz-form-label>

      <nz-form-item>
        <nz-form-control  nzValidatingTip="Validating..." [nzErrorTip]="cpasswordErrorTpl">
            <nz-input-group [nzSuffix]="suffixTemplate">
          <input [type]="passwordVisible ? 'text' : 'password'" nz-input  formControlName="password" placeholder="Input Password" />
          </nz-input-group>
          <ng-template #suffixTemplate>
            <i nz-icon [nzType]="passwordVisible ? 'eye-invisible' : 'eye'" (click)="passwordVisible = !passwordVisible"></i>
          </ng-template>
          <ng-template #cpasswordErrorTpl let-control>
            <ng-container *ngIf="control.hasError('required')">
              Please enter password
            </ng-container>
            <ng-container *ngIf="control.hasError('error')">
              Invalid password!
            </ng-container>
          </ng-template>
        </nz-form-control>
      </nz-form-item>

    <div class="modal_footer text-right mt20">
      <button nz-button class="ml-sm" type="reset" (click)="destroyModal()">Cancel</button>
      <button  nz-button nzType="primary" 
      type="submit" [disabled]="!confirmForm.valid"><span *ngIf="!status">Confirm Maintenence</span><span *ngIf="status">Disable Game</span></button>
    </div>
    <!-- <button nz-button [nzType]="'success'" (click)="destroyModal()">success</button> -->
  <!-- </p> -->
</form>
</div>
  `,
  styles: [`
    nz-input-number {
      width: 146px!important;
    }
    [nz-radio] {
      display: block;
      height: 32px;
      line-height: 32px;
    }
  `],
})
export class NzModalCustomFooterComponent implements OnInit {

  @Input() subtitle: any;
  @Input() data: any;
  @Input() status: any;
  confirmForm!: FormGroup;
  passwordVisible = false;

  constructor(private modal: NzModalRef, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.confirmForm = this.fb.group({
      timer: ['', Validators.required],
      freeze: [''],
      password: ['', Validators.required],
      timerUnit: ['m',],
    });
  }

  bothPlayerSelected(bothPlayerValue: any): void {

  }


/**
 *
 * @param isValid
 * @param formValue
 * to save confirmForm
 */

save(isValid: any, formValue: any): void {
  console.log('function works');
  // this.submitting = true;
  console.log(`${isValid}, ${formValue}`);
  if (!isValid) {
    return;
  }
}

  destroyModal(): void {
    this.modal.destroy();
  }
}