import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesDrawerComponent } from './games-drawer.component';

describe('GamesDrawerComponent', () => {
  let component: GamesDrawerComponent;
  let fixture: ComponentFixture<GamesDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
