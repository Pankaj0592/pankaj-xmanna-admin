import { Component, OnInit, TemplateRef } from '@angular/core';
import { DrawerHelper } from '@delon/theme';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';
import { GamesDrawerComponent } from './games-drawer/games-drawer.component';
import { GamesModalComponent } from './games-modal/games-modal.component';
import {NzDrawerRef, NzDrawerService } from 'ng-zorro-antd/drawer';

interface Game {
  _id: string;
  name: string;
  logo: string;
  genre: string;
  status: string;
  playerType: string;
  daily: number;
  dailyPrev: number;
  total: number;
  players24: any;
  players24Prev: any;
}

@Component({
  selector: 'app-games-management',
  templateUrl: './games-management.component.html',
  styleUrls: ['./games-management.component.css']
})
export class GamesManagementComponent implements OnInit {

  paginationToggle = true;
  validateForm!: FormGroup;
  archive = false;
  q: any = {
    _id: '',
    name: '',
    genre: '',
    status: '',
    playerType: '',
  };
  data: any[] = [];
  loading = false;
  filterBtnLoader = false;
  status = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Active', value: 'active', type: 'default', checked: false },
    { index: 2, text: 'Inactive', value: 'inactive', type: 'success', checked: false },
  ];
  playerType = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Single', value: 'single', type: 'default', checked: false },
    { index: 2, text: 'Multiple', value: 'multiple', type: 'success', checked: false },
    { index: 3, text: 'PvP', value: 'PvP', type: 'default', checked: false },
    { index: 4, text: 'Other', value: 'active', type: 'default', checked: false },
  ];
  genre = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Skill', value: 'skill', type: 'default', checked: false },
    { index: 2, text: 'Luck', value: 'luck', type: 'success', checked: false },
    { index: 3, text: 'Fighting', value: 'fighting', type: 'default', checked: false },
    { index: 4, text: 'Other', value: 'other', type: 'default', checked: false },
    { index: 5, text: 'Quiz', value: 'quiz', type: 'success', checked: false },
  ];
  developer = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Test-Developer1', value: null, type: 'default', checked: false },
    { index: 2, text: 'Test-Developer2', value: null, type: 'success', checked: false },
  ];
  sortByFilter = [
    { index: 0, label: '24h Matches: High to Low', value: 'total-asc', type: 'default', checked: false },
    { index: 1, label: '24h Matches: Low to High', value: 'total-desc', type: 'default', checked: false },
    { index: 2, label: '24h players: High to Low', value: 'daily-asc', type: 'default', checked: false },
    { index: 3, label: '24h players: Low to High', value: 'daily-desc', type: 'default', checked: false },
    { index: 4, label: 'Game Name: A to Z', value: 'name', type: 'default', checked: false },
    { index: 5, label: 'Game Name: Z to A', value: '-name', type: 'default', checked: false },
  ];
  description = '';
  totalCallNo = 0;
  expandForm = false;
  size: any = 'default';
  total = 0;
  listOfGames: Game[] = [];
  pageSize = 25;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedUser: any = null;
  selectedRow: any;
  pageReset = false;

  constructor(private gameService: GamesManagementService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private drawerService: NzDrawerService ) {}

  ngOnInit(): void {
    // this.selectedUser = this.sortByFilter[3]['value'];
  }


  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: any
    // filter: Array<{ key: string; value: string[] }>
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.gameService.getGames(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      this.filterBtnLoader = false;
      this.paginationToggle = true;
      console.log('game data', data);
      this.total = data.body.totalNumberOfGames; // mock the total data here
      this.listOfGames = data.body.gameData;
    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
    });
  }

  onQueryParamsChange(params: any): void {
    console.log(params);
    if (this.pageReset){
      this.pageReset = false;
      return;
    }
    const { pageSize, pageIndex, sort, filter } = params;
    if (!Array.isArray(sort)){
    this.sortField = params;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    return;
    }
    const currentSort = sort.find((item, index) => item.value !== null);
    console.log('current sort', currentSort);
    if (currentSort === undefined){
      this.selectedUser = '';
      this.sortField = null;
      this.sortOrder = null;
    }
    let sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
  
    // this.selectedUser = this.sortByFilter[4]['value'];
    if ((sortField === 'total' && sortOrder === 'ascend') || (sortField === 'daily' && sortOrder === 'ascend')) {
      this.selectedUser = this.sortByFilter[0]['value'];
      if (sortField === 'daily'){
      this.selectedUser = this.sortByFilter[2]['value'];
      }
      sortField = sortField + '-asc';
    } else if ((sortField === 'total' && sortOrder === 'descend') || (sortField === 'daily' && sortOrder === 'descend')){
      this.selectedUser = this.sortByFilter[1]['value'];
      if (sortField === 'daily'){
        this.selectedUser = this.sortByFilter[3]['value'];
        }
      sortField = sortField + '-desc';
    } else if (sortField === 'name' && sortOrder === 'descend'){
      this.selectedUser = this.sortByFilter[5]['value'];
      sortField = '-' + sortField;
    } else if (sortField === 'name' && sortOrder === 'ascend'){
      this.selectedUser = this.sortByFilter[4]['value'];
      sortField =  sortField;
    }
  
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField ? sortField : this.sortField;
    this.sortOrder = sortOrder ? sortOrder : this.sortOrder;
    this.filter = this.filterObj ? this.filterObj.filter : filter;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    // this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, filter);
  }
  
  sortBy(sortParams: any): void{
    // if (this.filterObj){
    //   this.reset();
    // }
    this.onQueryParamsChange(sortParams);
    return;
    console.log('sort by', sortParams);
    switch (sortParams){
      case (sortParams = 'total-desc'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
      case (sortParams = 'total-asc'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
      case (sortParams = 'daily-desc'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
      case (sortParams = 'daily-asc'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
      case (sortParams = 'name'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
      case (sortParams = '-name'):
      this.sortField = sortParams;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      break;
    }
  }

  public reload(): void {
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter );
  }
  
  getData(): void {
    console.log('filter data', this.q);
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      let formObj = { filter };
      formObj ? this.filterObj = formObj : null;
      // this.loading = true;
      this.filterBtnLoader = true;
      this.filter = formObj.filter;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      // this.applyFilter(formObj);
    } else{
      return;
    }
  }
  
  applyFilter(formObj: any): void {
    this.gameService.filterGame(formObj).subscribe((data: any) => {
      this.loading = false;
      this.filterBtnLoader = false;
      if (data.body.gameData.length){
        this.paginationToggle = false;
        console.log('games filtered data', data);
        this.total = data.body.gameData.length; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfGames = data.body.gameData;
      } else{
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log('error', error);
    });
  }

  public rowClick(data: any): void{
    console.log('data', data);
    this.selectedRow = data._id;
    this.drawerHelper.create('', GamesDrawerComponent, { record: data }).subscribe((res: any) => {
      console.log('back drawer response', res);
      this.msg.info(res);
    });
    }
    
    public addGame(): void{
      this.modalHelper.open( GamesModalComponent, { data: { title: 'New Game', titleI18n: 'page-name' }}, 'md').subscribe((res: any) => {
        console.log('modal back response', res);
        if (res) {
          this.reload();
        }
      });
    }

  reset(): void {
    this.q = {};
    this.filterObj.filter = [];
    this.filter = [];
    this.pageReset = true;
    // this.pageIndex = 1;
    // this.pageSize = 30;
  }

  tableSize(size: string): void{
    this.size = size;
  }

}
