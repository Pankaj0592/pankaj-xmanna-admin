import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { SharedModule } from '@shared';
import { GamesDrawerComponent, NzModalCustomFooterComponent, NzModalCustomFooter1Component } from './games-drawer/games-drawer.component';
import { GamesManagementRoutingModule } from './games-management-routing.module';
import { GamesManagementComponent } from './games-management.component';
import { GamesModalComponent } from './games-modal/games-modal.component';
import { PlayOptionsModalComponent } from './play-options-modal/play-options-modal.component';
import { NzAffixModule } from 'ng-zorro-antd/affix';
const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    GamesManagementRoutingModule,
    NzAffixModule
  ],
  declarations: [
    COMPONENTS,
    GamesManagementComponent,
    GamesDrawerComponent,
    GamesModalComponent,
    PlayOptionsModalComponent,
    NzModalCustomFooterComponent,
    NzModalCustomFooter1Component
  ],
})
export class GamesManagementModule {}
