import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { TournamentManagementRoutingModule } from './tournament-management-routing.module';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    TournamentManagementRoutingModule
  ],
  declarations: COMPONENTS,
})
export class TournamentManagementModule { }
