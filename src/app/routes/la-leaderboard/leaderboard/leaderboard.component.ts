import { Component, OnInit } from '@angular/core';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { LeaderboardService } from 'src/app/services/leaderboard/leaderboard.service';
import { AddLeaderboardComponent } from '../add-leaderboard/add-leaderboard.component';
import { LeaderboardDrawerComponent } from '../leaderboard-drawer/leaderboard-drawer.component';

interface Leaderboard {
  _id: string;
  name: string;
  startDate: string;
  endDate: string;
  prizes: any;
  proPrizePool: number;
  practicePrizePool: number;
  days: number;
  proPlayers: number;
  proPlayersCount: number;
  practicePlayers: number;
  practicePlayersCount: number;
  prizesUSD: number;
  prizesxCoins: number;
  status: string;
}

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.less']
})
export class LeaderboardComponent implements OnInit {


  q: any = {
    startDate: '',
    proPrizePool: '',
    practicePrizePool: '',
    status: '',
    playerType: '',
    prizes: '',
  };

  status = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'Ongoing', value: 'ongoing', type: 'ongoing', checked: false },
    { index: 2, text: 'Completed', value: 'completed', type: 'completed', checked: false },
    { index: 1, text: 'Finished', value: 'finished', type: 'finished', checked: false },
    { index: 1, text: 'Upcoming', value: 'upcoming', type: 'upcoming', checked: false },
  ];
  sortByFilter = [
    { index: 0, label: 'Start Date', value: 'name', type: 'default', checked: false },
    { index: 1, label: 'Pro Players: 0 to 9', value: 'total-asc', type: 'default', checked: false },
    { index: 2, label: 'Pro Players: 9 to 0', value: 'total-desc', type: 'default', checked: false },
    { index: 3, label: 'Pro Players: 0 to 9', value: 'daily-asc', type: 'default', checked: false },
    { index: 4, label: 'Pro Players: 9 to 0', value: 'daily-desc', type: 'default', checked: false },
  ];

  listOfLeaderBoards: Leaderboard[] = [];

  loading = false;
  archive = false;
  filterBtnLoader = false;
  expandForm = false;
  size: any = 'default';
  total = 0;
  startDate: any;
  endDate: any;
  pageSize = 25;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedLeaderboard: any = null;
  selectedRow: any;

  constructor(private leaderboardService: LeaderboardService, private msg: NzMessageService, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper) { }

  ngOnInit(): void {
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    filter: any
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    this.filter = filter;
    this.leaderboardService.getLeaderBoard(pageIndex, pageSize, sortField, sortOrder, filter).subscribe((data: any) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log('leaderboard data', data);
      this.total = data.body.totalNumberOfLeaderBoards;
      this.listOfLeaderBoards = data.body.leaderBoardData;

      /*============  FOR ADDING ONE KEY IN ENVERY OBJECT NAMED AS DAYS [START] ================= **/
      this.listOfLeaderBoards = this.listOfLeaderBoards.map(function (el) {
        var o = Object.assign({}, el);
        o.days = 0;
        return o;
      });

      /*============  FOR ADDING ONE KEY IN ENVERY OBJECT NAMED AS DAYS [END] ================= **/

      this.listOfLeaderBoards.forEach((element: any) => {
        if (element.startDate && element.endDate) {
          this.startDate = new Date(element.startDate);
          this.endDate = new Date(element.endDate);
          const diffTime = Math.abs(this.startDate - this.endDate);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          element.days = diffDays;
        }
      });
    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
    });
  }

  onQueryParamsChange(params: any): void {
    console.log(params);
    const { pageSize, pageIndex, sort, filter } = params;
    if (!Array.isArray(sort)) {
      this.sortField = params;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      return;
    }
    const currentSort = sort.find((item, index) => item.value !== null);
    console.log('current sort', currentSort);
    if (currentSort === undefined) {
      this.selectedLeaderboard = '';
      this.sortField = null;
      this.sortOrder = null;
    }
    let sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;

    if ((sortField === 'total' && sortOrder === 'ascend') || (sortField === 'daily' && sortOrder === 'ascend')) {
      this.selectedLeaderboard = this.sortByFilter[0]['value'];
      if (sortField === 'daily') {
        this.selectedLeaderboard = this.sortByFilter[2]['value'];
      }
      sortField = sortField + '-asc';
    } else if ((sortField === 'total' && sortOrder === 'descend') || (sortField === 'daily' && sortOrder === 'descend')) {
      this.selectedLeaderboard = this.sortByFilter[1]['value'];
      if (sortField === 'daily') {
        this.selectedLeaderboard = this.sortByFilter[3]['value'];
      }
      sortField = sortField + '-desc';
    } else if (sortField === 'name' && sortOrder === 'descend') {
      this.selectedLeaderboard = this.sortByFilter[5]['value'];
      sortField = '-' + sortField;
    } else if (sortField === 'name' && sortOrder === 'ascend') {
      this.selectedLeaderboard = this.sortByFilter[4]['value'];
      sortField = '-' + sortField;
    }

    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField ? sortField : this.sortField;
    this.sortOrder = sortOrder ? sortOrder : this.sortOrder;
    this.filter = this.filterObj ? this.filterObj.filter : filter;
    this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
  }

  sortBy(sortParams: any): void {
    this.onQueryParamsChange(sortParams);
    return;
    console.log('sort by', sortParams);
    switch (sortParams) {
      case (sortParams = 'total-desc'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
      case (sortParams = 'total-asc'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
      case (sortParams = 'daily-desc'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
      case (sortParams = 'daily-asc'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
      case (sortParams = 'name'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
      case (sortParams = '-name'):
        this.sortField = sortParams;
        this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
        break;
    }
  }

  public reload(): void {
    if (this.q) {
      this.submitFilter(); return;
    } else {
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
    }
  }

  submitFilter(): void {
    console.log('filter data', this.q);
    const filter = this.q;
    if (this.q.proPrizePool) {
      this.q.prizes = { '$elemMatch': { 'type': 'pro', 'amount': this.q.proPrizePool } };
    }
    if (this.q.practicePrizePool) {
      this.q.prizes = { '$elemMatch': { 'type': 'practice', 'amount': this.q.practicePrizePool } };
    }
    delete this.q.proPrizePool;
    delete this.q.practicePrizePool;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ''
      ) {
        delete filter[propName];
      }
    }
    if (this.q.startDate) {
      this.q.startDate = this.convertDate(this.q.startDate);
    }

    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const formObj = { filter };
      // tslint:disable-next-line: no-unused-expression
      formObj ? this.filterObj = formObj : null;
      // this.loading = true;
      this.filterBtnLoader = true;
      this.filter = formObj.filter;
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      if (this.q.prizes) {
        if (this.q.prizes.$elemMatch.type === 'pro') {
          this.q.proPrizePool = this.q.prizes.$elemMatch.amount;
        } else {
          this.q.practicePrizePool = this.q.prizes.$elemMatch.amount;
        }
      }
    } else {
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.filter);
      return;
    }
  }


  convertDate(str: any): any {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-');
  }

  applyFilter(formObj: any): void {
    this.leaderboardService.filterLeaderboard(formObj).subscribe((data: any) => {
      this.loading = false;
      this.filterBtnLoader = false;
      if (data.body.leaderBoardData.length > 0) {
        console.log('games filtered data', data);
        this.total = data.body.gameData.length;
        console.log('filter data length', this.total);
        this.listOfLeaderBoards = data.body.gameData;
      } else {
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log('error', error);
    });
  }

  /** ======== TRIGGERED WHEN TABLE ROW CLICKED. THEN OPEN INFO [START] ====== */
  public rowClick(data: any): void {
    console.log('data', data);
    this.selectedRow = data._id;
    this.drawerHelper.create('', LeaderboardDrawerComponent, { record: data }).subscribe((res: any) => {
      console.log('back drawer response', res);
      if (res) {
        this.reload();
      }
    });
  }
  /** ======== TRIGGERED WHEN TABLE ROW CLICKED. THEN OPEN INFO [END] ====== */

  /** ======== TRIGGERED WHEN WANT TO ADD LEADERBOARD [START] ====== */
  public addGame(): void {
    this.modalHelper.open(AddLeaderboardComponent, { data: { title: 'New Leaderboard', titleI18n: 'page-name' } }, 'sm').subscribe((res: any) => {
      console.log('modal back response', res);
      if (res) {
        this.reload();
      }
    });
  }
  /** ======== TRIGGERED WHEN WANT TO ADD LEADERBOARD [END] ====== */

  reset(): void {
    this.q = {};
    this.filterObj.filter = [];
    this.filter = [];
  }

  tableSize(size: string): void {
    this.size = size;
  }

}
