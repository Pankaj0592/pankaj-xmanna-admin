import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { differenceInCalendarDays, setHours } from 'date-fns';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { LeaderboardService } from 'src/app/services/leaderboard/leaderboard.service';

@Component({
  selector: 'app-add-leaderboard',
  templateUrl: './add-leaderboard.component.html',
  styleUrls: ['./add-leaderboard.component.less']
})
export class AddLeaderboardComponent implements OnInit {

  @Input() data: any;
  leaderBoardForm!: FormGroup;
  submitted = false;
  submitting = false;
  patchStatess!: FormArray;
  today = new Date();

  constructor(private fb: FormBuilder, private modal: NzModalRef, private leaderboardService: LeaderboardService, private msg: NzMessageService) { }

  ngOnInit(): void {
    console.log('posted data', this.data);
    this.leaderBoardForm = this.fb.group({
      startDate: [null, [Validators.required]],
      endDate: [, [Validators.required]],
      proPrizePool: [null, [Validators.required]],
      practicePrizePool: [null, [Validators.required]],
    });

    if (this.data.leaderboardData) {
      this.formSetValue();
    }
  }

  disabledDate = (current: Date): boolean =>
    // Can not select days before today and today
    differenceInCalendarDays(current, this.today) <= 0

  // disabledEndDate = (current: Date): boolean =>
  //   // Can not select days before today and next two days
  //   differenceInCalendarDays(current, this.today) <= 2


  disabledEndDate = (current: Date): boolean =>
    // Can not select days before today and next two days
    differenceInCalendarDays(current, this.today) <= 2

  convertDate(str: any): any {
    const date = new Date(str),
      mnth = ('0' + (date.getMonth() + 1)).slice(-2),
      day = ('0' + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join('-');
  }


  /**
   *
   * @param isValid
   * @param formValue
   * to save addCountryForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; }
    this.submitted = true;
    this.submitting = true;
    const prizesArray = [{ type: 'pro', amount: formValue.proPrizePool },
    { type: 'practice', amount: formValue.practicePrizePool },
    ];
    const startdate = this.convertDate(formValue.startDate);
    const enddate = this.convertDate(formValue.endDate);

    const formObj = {
      startDate: formValue.startDate,
      endDate: formValue.endDate,
      holdPrizeDistribution: false,
      // description: 'default description',
      name: 'Leaderboard' + startdate,
      prizes: prizesArray
    };
    console.log('form obj', formObj);

    // return;
    if (!this.data.leaderboardData) {
      this.leaderboardService.addLeaderboard(formObj).subscribe(
        (res: any) => {
          console.log(res);
          if (res.responseStatus.success) {
            this.submitting = false;
            this.msg.success('Added successfully');
            this.destroyModal(res.body);
          } else {

          }
        },
        error => {
          console.log('an error has occured while sending data', error);
          this.submitting = false;
        });
    } else {
      this.leaderboardService.editLeaderboard(this.data.leaderboardData._id, formObj).subscribe((res: any) => {
        if (res.responseStatus.success) {
          console.log('updated successfully', res);
          this.destroyModal(res.body);
          this.submitting = false;
        } else {

        }
      }, (error) => {
        console.log(error);
        this.submitting = false;
      });

    }

  }

  formSetValue(): void {
    this.leaderBoardForm.get('startDate')?.setValue(this.data.leaderboardData.startDate);
    this.leaderBoardForm.get('endDate')?.setValue(this.data.leaderboardData.endDate);
    this.leaderBoardForm.get('proPrizePool')?.setValue(this.data.leaderboardData.prizes[0].amount);
    this.leaderBoardForm.get('practicePrizePool')?.setValue(this.data.leaderboardData.prizes[1].amount);
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }

}
