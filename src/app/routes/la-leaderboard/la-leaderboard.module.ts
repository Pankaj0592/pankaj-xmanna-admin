import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { AddLeaderboardComponent } from './add-leaderboard/add-leaderboard.component';
import { AddPrizesComponent } from './add-prizes/add-prizes.component';
import { LaLeaderboardRoutingModule } from './la-leaderboard-routing.module';
import { LeaderboardDrawerComponent } from './leaderboard-drawer/leaderboard-drawer.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';


const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    LaLeaderboardRoutingModule,
  ],
  declarations: [COMPONENTS, LeaderboardComponent, AddLeaderboardComponent, LeaderboardDrawerComponent, AddPrizesComponent],
})
export class LaLeaderboardModule { }
