import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CountriesService } from 'src/app/services/countries/countries.service';

@Component({
  selector: 'app-add-prizes',
  templateUrl: './add-prizes.component.html',
  styleUrls: ['./add-prizes.component.less']
})
export class AddPrizesComponent implements OnInit {

  @Input() data: any;
  addPrizesForm!: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private modal: NzModalRef, private countryService: CountriesService, private msg: NzMessageService) { }

  ngOnInit(): void {
    console.log('add prizes data', this.data);
    this.addPrizesForm = this.fb.group({
      proPrizePool: [null, [Validators.required]],
      practicePrizePool: [null, [Validators.required]],
    });
  }



  /**
   *
   * @param isValid
   * @param formValue
   * to save addCountryForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) { return; }

    this.submitted = true;
    const formObj = {
      proPrizePool: formValue.proPrizePool,
      practicePrizePool: formValue.practicePrizePool,
    };
    console.log('form obj', formObj);
    return;
    this.countryService.addCountry(formObj).subscribe(
      (res: any) => {
        console.log(res);
        if (res.responseStatus.success) {
          this.submitted = false;
          this.msg.success('Added successfully');
          this.destroyModal(res.body);
        } else {

        }
      },
      error => {
        console.log('an error has occured while sending data', error);
        this.submitted = false;
      });
  }


  destroyModal(obj: any): void {
    this.modal.destroy({ data: obj });
  }

  closeModal(): any {
    this.modal.destroy();
  }

}
