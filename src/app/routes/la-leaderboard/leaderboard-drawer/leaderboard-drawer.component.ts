import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {  ModalHelper } from '@delon/theme';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { LeaderboardService } from 'src/app/services/leaderboard/leaderboard.service';
import { DeleteModalComponent } from '../../component/delete-modal/delete-modal.component';
import { AddLeaderboardComponent } from '../add-leaderboard/add-leaderboard.component';
import { AddPrizesComponent } from '../add-prizes/add-prizes.component';



@Component({
  selector: 'app-leaderboard-drawer',
  templateUrl: './leaderboard-drawer.component.html',
  styleUrls: ['./leaderboard-drawer.component.less']
})
export class LeaderboardDrawerComponent implements OnInit, OnDestroy {

  dateTimer: any;
  @Input() record: any;
  @Input() depositOptionsArray: any;
  @Input() listOfCountries: any;
  tabs = [
    {
      name: 'Leaderboard Info',
      disabled: false
    },
    {
      name: 'Pro League',
      disabled: false
    },
    {
      name: 'Practice League',
      disabled: false
    },
  ];

  leaderBoardPrizePoolData = [{ player: 'noddypickleball', trophies: '10', winUSD: '5', payment: true }]

  constructor(
    private drawerRef: NzDrawerRef<string>,
    public msg: NzMessageService,
    private modalHelper: ModalHelper,
    private modal: NzModalService,
    private leaderboardService: LeaderboardService
  ) {
  }

  ngOnInit(): void {
    // console.log('drawer value', this.record);
    if (this.record.status === 'ongoing'){
    this.record.daysLeft = this.timeDiffCalc(this.record.endDate);
    this.timeCalculation();
  }
    console.log('this.record', this.record);
  }

  public timeCalculation(): void {
    setInterval(() => {
      this.record.daysLeft = this.timeDiffCalc(this.record.endDate);
    }, 1000);

  }

  public timeDiffCalc(date: any): any {
    if (!date) { return; }
    let dateNow: any = new Date();
    let dateFuture: any = new Date(date);
    if (dateNow >= dateFuture){
      return '00' + ':' + '00' + ':' + '00' + ':' + '00';
    }
    let diffInMilliSeconds = Math.abs(dateFuture - dateNow) / 1000;

    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;
    // console.log('calculated days', days);

    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;
    // console.log('calculated hours', hours);

    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;
    // console.log('minutes', minutes);

    // calculate seconds
    const seconds = Math.floor(diffInMilliSeconds) % 60;
    diffInMilliSeconds -= seconds * 60;
    // console.log('seconds', seconds);

    let difference = '';

    if (days >= 0) {
      difference += (days === 1) ? `${days} ` : `${days}`;
    } else if (days <= 0 && hours > 0) {
      difference += (hours === 0 || hours === 1) ? `${hours}` : `${hours}`;
    } else if (days <= 0 && hours <= 0 && minutes >= 0) {
      if (minutes === 0 || minutes === 1) {
        difference += '60'
      } else {
        difference += `${minutes}`;
      }
    }

    return (/^\d$/.test(difference.toString()) ? '0' + difference : difference) + ':' + (/^\d$/.test(hours.toString()) ? '0' + hours : hours) + ':' + (/^\d$/.test(minutes.toString()) ? '0' + minutes : minutes) + ':' + (/^\d$/.test(seconds.toString()) ? '0' + seconds : seconds);
  }


  addPrizes(leaderboardInfo: any): void {
    console.log('leaderboardInfo data', leaderboardInfo);
    this.modalHelper.open(AddPrizesComponent, { data: { title: 'Add Prizes', leaderBoardData: leaderboardInfo } }, 'sm').subscribe((res: any) => {
      console.log('modal back response', res);
      if (res) {
        this.reload();
      }
    });
  }

  editLeaderboard(leaderboardInfo: any): void {
    console.log('leaderboardInfo data', leaderboardInfo);
    this.modalHelper.open(AddLeaderboardComponent, { data: { title: 'Edit Leaderboard', leaderboardData: leaderboardInfo } }, 'sm').subscribe((res: any) => {
      console.log('modal back response', res);
      if (res) {
        this.record = res.data;
      }
    });
  }

  deleteLeaderboard(leaderboardData: any): void {
    const subtitle = '';
    const title = 'Delete leaderboard?';
    const stateData = leaderboardData;
    const data = this.record;
    this.multiModal(subtitle, stateData, title, data);
  }
  /** ====== mulimodal FUNCTION CAN OPEN MULTIPLE TYPE MODALS [START] ======= */
  multiModal(subtitle: string, stateData: any, title: string, data: any): void {
    const modal = this.modal.warning({
      nzTitle: title,
      nzClosable: true,
      nzAutofocus: null,
      nzCentered: true,
      nzComponentParams: {
        data: data,
        subtitle: subtitle,
        stateData: stateData,
      },
      nzContent: DeleteModalComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      if (result.data) {
        console.log('[afterClose] The result is:', result.data);
        this.leaderboardService.deleteLeaderboard(this.record._id).subscribe((res: any) => {
          console.log('deleted successfully');
          this.drawerRef.close(res.body);
        });
      }
    });
  }
  /** ====== mulimodal FUNCTION CAN OPEN MULTIPLE TYPE MODALS [END] ======= */

  ngOnDestroy(): void {
    console.log('destroy calls');
  }

  reload(): void {

  }

  close(): void {
    //   this.drawerRef.close(this.value);
  }
}

