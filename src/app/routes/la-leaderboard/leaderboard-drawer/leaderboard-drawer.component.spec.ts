import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaderboardDrawerComponent } from './leaderboard-drawer.component';

describe('LeaderboardDrawerComponent', () => {
  let component: LeaderboardDrawerComponent;
  let fixture: ComponentFixture<LeaderboardDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeaderboardDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderboardDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
