import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { RouteRoutingModule } from './routes-routing.module';
import { ModalComponent } from './component/modal/modal.component';
import { UserBalanceModalComponent } from './component/user-balance-modal/user-balance-modal.component';
import { DeleteModalComponent } from './component/delete-modal/delete-modal.component';


const COMPONENTS: Type<null>[] = [];

@NgModule({
  imports: [SharedModule, RouteRoutingModule],
  declarations: [...COMPONENTS, ModalComponent, UserBalanceModalComponent, DeleteModalComponent],
})
export class RoutesModule {}
