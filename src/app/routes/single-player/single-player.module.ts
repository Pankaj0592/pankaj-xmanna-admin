import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { BracketsModalComponent } from './brackets-modal/brackets-modal.component';
import { BracketsdrawerComponent } from './brackets-modal/bracketsdrawer/bracketsdrawer.component';
import { SinglePlayerDrawerComponent } from './single-player-drawer/single-player-drawer.component';
import { SinglePlayerModalComponent } from './single-player-modal/single-player-modal.component';
import { SinglePlayerRoutingModule } from './single-player-routing.module';
import { SinglePlayerComponent } from './single-player/single-player.component';

import { UserModule } from '../user/user.module';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    SinglePlayerRoutingModule,
    UserModule,
  ],
  declarations: [COMPONENTS, SinglePlayerComponent, SinglePlayerDrawerComponent, BracketsModalComponent, BracketsdrawerComponent, SinglePlayerModalComponent],
  exports: [SinglePlayerModalComponent]
})
export class SinglePlayerModule { }
