import { Component, Input, OnInit } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { I18NService } from '@core';
import { STColumn } from '@delon/abc/st';
import { DrawerHelper, ModalHelper, _HttpClient } from '@delon/theme';
import { getTimeDistance } from '@delon/util/date-time';
import { deepCopy } from '@delon/util/other';
import { yuan } from '@shared';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserManagementService } from 'src/app/services/userService/user-management.service';
import { SinglePlayerService } from '../../../services/single-player/single-player.service';
import { SinglePlayerModalComponent } from '../single-player-modal/single-player-modal.component';
import { UserDrawerComponent } from '../../user/user-drawer/user-drawer.component';



@Component({
  selector: 'app-single-player-drawer',
  templateUrl: './single-player-drawer.component.html',
  styleUrls: ['./single-player-drawer.component.less'],
})
export class SinglePlayerDrawerComponent implements OnInit {

  @Input() record: any;
  @Input() menuSelectedTab: any;
  tabs = ['Challenge Info', 'Transactions', 'Reports'];

  transactionDetails: any = {};
  cheaterDetail: any;
  archive = false;
  status = 'success';
  data: any = {};
  loading = true;
  date_range: Date[] = [];
  prizePool: any;

  rankingListData: Array<{ title: string; total: number }> = Array(7)
    .fill({}) .map((_, i) => {
      return {
        title: this.i18n.fanyi('app.analysis.test', { no: i }),
        total: 323234,
      };
    });
    
  titleMap = {
    y1: this.i18n.fanyi('app.analysis.traffic'),
    y2: this.i18n.fanyi('app.analysis.payments'),
  };

  searchColumn: STColumn[] = [
    {
      title: { text: '排名', i18n: 'app.analysis.table.rank' },
      index: 'index',
    },
    {
      title: { text: '搜索关键词', i18n: 'app.analysis.table.search-keyword' },
      index: 'keyword',
      click: (item) => this.msg.success(item.keyword),
    },
    {
      type: 'number',
      title: { text: '用户数', i18n: 'app.analysis.table.users' },
      index: 'count',
      sort: {
        compare: (a, b) => a.count - b.count,
      },
    },
    {
      type: 'number',
      title: { text: '周涨幅', i18n: 'app.analysis.table.weekly-range' },
      index: 'range',
      render: 'range',
      sort: {
        compare: (a, b) => a.range - b.range,
      },
    },
  ];

  salesType = 'all';
  salesPieData: any;
  salesTotal = 0;

  saleTabs: Array<{ key: string; show?: boolean }> = [
    { key: 'sales', show: true },
    { key: 'visits' },
  ];

  offlineIdx = 0;

  panels = [
    {
      active: false,
      disabled: false,
      name: 'ETH',
    },
    {
      active: true,
      disabled: false,
      name: 'USDT',
    },
    {
      active: false,
      disabled: true,
      name: 'TRX',
    },
    {
      active: false,
      disabled: false,
      name: 'BTC',
    },
  ];

  playOptionsSinglePanel = [
    {
      active: true,
      disabled: false,
      name: 'Brackets',
    },
    {
      active: false,
      disabled: false,
      name: 'Player vs Player',
    },
    {
      active: false,
      disabled: false,
      name: 'Practice',
    },
  ];

  playOptionsMultiplePanel = [
    {
      active: true,
      disabled: false,
      name: 'Paid Deathmatch',
    },
    {
      active: false,
      disabled: false,
      name: 'Paid Team vs Team',
    },
    {
      active: false,
      disabled: false,
      name: 'Practice Team vs Team',
    }
  ];

  constructor(
    private http: _HttpClient,
    public msg: NzMessageService,
    private i18n: I18NService,
    private cdr: ChangeDetectorRef,
    private drawerHelper: DrawerHelper,
    private modal: NzModalService,
    private challengeService: SinglePlayerService,
    private userService: UserManagementService
  ) {
  }


  ngOnInit(): void {
    console.log('drawer value', this.record);
    console.log('tab value', this.menuSelectedTab);
    const totalPool = this.record.playOption.fee * 2; // this equals to 100 + rake %
    const totalPercent = 100 + this.record.playOption.rake;
    const singleX = (totalPool) / totalPercent;
    const rakePool = this.record.playOption.rake * singleX;
    this.prizePool = 100 * singleX;
    console.log('netPool', this.prizePool);

    if (this.record.status === 'open') {
    this.record.daysLeft = this.timeDiffCalc(this.record.createdAt);
    this.timeCalculation();
    console.log(this.record.daysLeft);
  }
    this.challengeService.transactionDetails(this.record._id).subscribe((res: any) => {
      if (res.responseStatus.success) {
        // console.log('transactions details', res);
        this.transactionDetails.data = res.body;
        console.log('transactions details', res.body);
        this.transactionDetails.tab = true;
      }
    }, (error) => {
      console.log(error);
    });
  }


  public timeCalculation(): void {
    setInterval(() => {
      this.record.daysLeft = this.timeDiffCalc(this.record.createdAt);
    }, 1000);

  }

  public timeDiffCalc(date: string): any {
    if (!date) { return; }
    let dateNow: any = new Date();
    let dt: any = new Date(date);
    dt.setHours( dt.getHours() + 24 );

    if (dateNow >= dt){
      return '00' + ':' + '00' + ':' + '00' + ':' + '00';
    }

    let diffInMilliSeconds = Math.abs(dt - dateNow) / 1000;

    // calculate days
    const days = Math.floor(diffInMilliSeconds / 86400);
    diffInMilliSeconds -= days * 86400;
    // console.log('calculated days', days);

    // calculate hours
    const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
    diffInMilliSeconds -= hours * 3600;
    // console.log('calculated hours', hours);

    // calculate minutes
    const minutes = Math.floor(diffInMilliSeconds / 60) % 60;
    diffInMilliSeconds -= minutes * 60;
    // console.log('minutes', minutes);

    // calculate seconds
    const seconds = Math.floor(diffInMilliSeconds) % 60;
    diffInMilliSeconds -= seconds * 60;
    // console.log('seconds', seconds);

    let difference = '';

    if (days >= 0) {
      difference += (days === 1) ? `${days} ` : `${days}`;
    } else if (days <= 0 && hours > 0) {
      difference += (hours === 0 || hours === 1) ? `${hours}` : `${hours}`;
    } else if (days <= 0 && hours <= 0 && minutes >= 0) {
      if (minutes === 0 || minutes === 1) {
        difference += '60';
      } else {
        difference += `${minutes}`;
      }
    }
    return (/^\d$/.test(difference.toString()) ? '0' + difference : difference) + ':' + (/^\d$/.test(hours.toString()) ? '0' + hours : hours) + ':' + (/^\d$/.test(minutes.toString()) ? '0' + minutes : minutes) + ':' + (/^\d$/.test(seconds.toString()) ? '0' + seconds : seconds);
    // return difference + ':' + hours + ':' + minutes + ':' + seconds;
  }


  public tabSelect(value: any): void {
    this.archive = false;
    if (value === this.tabs[4]) {
      this.http.get('/chart').subscribe((res) => {
        res.offlineData.forEach((item: any, idx: number) => {
          item.show = idx === 0;
          item.chart = deepCopy(res.offlineChartData);
          console.log('chart data', item.chart);
        });
        this.data = res;
        this.loading = false;
        this.changeSaleType();
      });
    }
    if (value === this.tabs[1]) {

    }
  }
  setDate(type: 'today' | 'week' | 'month' | 'year'): void {
    this.date_range = getTimeDistance(type);
    setTimeout(() => this.cdr.detectChanges());
  }

  changeSaleType(): void {
    this.salesPieData =
      this.salesType === 'all'
        ? this.data.salesTypeData
        : this.salesType === 'online'
          ? this.data.salesTypeDataOnline
          : this.data.salesTypeDataOffline;
    if (this.salesPieData) {
      this.salesTotal = this.salesPieData.reduce(
        (pre: number, now: { y: number }) => now.y + pre,
        0
      );
    }
    this.cdr.detectChanges();
  }

  handlePieValueFormat(value: string | number): string {
    return yuan(value);
  }
  salesChange(idx: number): void {
    if (this.saleTabs[idx].show !== true) {
      this.saleTabs[idx].show = true;
      this.cdr.detectChanges();
    }
  }
  offlineChange(idx: number): void {
    if (this.data.offlineData[idx].show !== true) {
      this.data.offlineData[idx].show = true;
      this.cdr.detectChanges();
    }
  }


  openArchive(): void {
    console.log('archive clicked');
    this.archive = true;
  }

  onBack(): void {
    console.log('onBack');
    this.archive = false;
  }
  cancelMatch(value: string): void {
    if (this.record.bouts) {
      if (this.record.bouts.playerOne.score > this.record.bouts.playerTwo.score) {
        var lessScore = this.record.bouts.playerTwo.score;
      } else {
        var lessScore = this.record.bouts.playerOne.score;
      }
    }

    switch (value) {
      case (value = 'open'): case (value = 'reported'):
        var subtitle = 'Who’s going to get the entry fee refund?';
        var statement = 'Balance after deposit:';
        var refund = false;
        var title = 'Do you want to cancel this match?';
        var data = this.record;
        this.multiModal(subtitle, statement, title, data, refund);
        break;
      case (value = 'finished'): case (value = 'aborted'):
        var title = 'Do you want to refund entry fees?';
        var statement = 'Balance after removal:';
        var refund = true;
        var subtitle = 'Only the loser ' + this.record.bouts.looser + 'with ' + lessScore + ' points will get the refund.';
        var data = this.record;
        this.multiModal(subtitle, statement, title, data, refund);
        break;
    }
  }

  multiModal(subtitle: string, statement: string, title: string, data: any, refund: boolean): void {
    const modal = this.modal.warning({
      nzTitle: title,
      nzClosable: false,
      nzAutofocus: null,
      nzComponentParams: {
        data: this.record,
        subtitle: subtitle,
        statement: statement,
        refund: refund
      },
      nzContent: SinglePlayerModalComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      if (result) {
        console.log('[afterClose] The result is:', result);
      }
    });
  }
  cheaterDetails(playerId: any): void {
    console.log(playerId);
    const filter = { '_id': playerId };
    const formObj = { filter };
    this.userService.filterUser(formObj).subscribe((res: any) => {
      console.log(res);
      this.cheaterDetail = res.body[0];
      console.log('cheater detail=========', this.cheaterDetail);
      // this.drawerHelper.create('', UserDrawerComponent, { record: this.cheaterDetail }).subscribe((res: any) => {
      //   this.msg.info(res);
      // });
      this.drawerHelper.create('', UserDrawerComponent, { record: this.cheaterDetail },
        {
          size: 540, footer: false, drawerOptions: {
            nzMaskClosable: true,
            nzClosable: true,
          }
        }).subscribe(res => console.log('成功'));
    });
  }

  close(): void {
    //   this.drawerRef.close(this.value);
  }
}
