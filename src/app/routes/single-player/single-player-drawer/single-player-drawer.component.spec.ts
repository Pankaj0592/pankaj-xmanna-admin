import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePlayerDrawerComponent } from './single-player-drawer.component';

describe('SinglePlayerDrawerComponent', () => {
  let component: SinglePlayerDrawerComponent;
  let fixture: ComponentFixture<SinglePlayerDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglePlayerDrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePlayerDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
