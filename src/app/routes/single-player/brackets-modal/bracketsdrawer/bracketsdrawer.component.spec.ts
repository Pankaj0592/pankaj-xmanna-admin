import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BracketsdrawerComponent } from './bracketsdrawer.component';

describe('BracketsdrawerComponent', () => {
  let component: BracketsdrawerComponent;
  let fixture: ComponentFixture<BracketsdrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BracketsdrawerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BracketsdrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
