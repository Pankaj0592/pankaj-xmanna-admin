import { Component, Input, OnInit } from "@angular/core";
import { NzDrawerRef } from "ng-zorro-antd/drawer";
import { NzIconModule } from "ng-zorro-antd/icon";
import { ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { I18NService } from "@core";
import { STColumn } from "@delon/abc/st";
import { yuan } from "@shared";
import { getTimeDistance } from "@delon/util/date-time";
import { deepCopy } from "@delon/util/other";
import { NzMessageService } from "ng-zorro-antd/message";
import { _HttpClient, ModalHelper, DrawerHelper } from "@delon/theme";
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { SinglePlayerDrawerComponent } from '../single-player-drawer/single-player-drawer.component';
import { BracketsdrawerComponent } from './bracketsdrawer/bracketsdrawer.component';


@Component({
  selector: "app-brackets-modal",
  templateUrl: "./brackets-modal.component.html",
  styleUrls: ["./brackets-modal.component.less"],
})
export class BracketsModalComponent implements OnInit {
  @Input() record: any;
  @Input() menuSelectedTab: any;
  tabs = [
    'Brackets Info',
    'Brackets Table',
  ];

  

  archive = false;
  status = 'success';
  data: any = {};
  loading = true;
 

  constructor( public msg: NzMessageService, private drawerHelper: DrawerHelper, ) {}

  ngOnInit(): void {
    console.log('drawer value', this.record);
    console.log('tab value', this.menuSelectedTab);
  }

  tabSelect(selectedTab: string): void{
    if (selectedTab === 'Brackets Table'){
      this.drawerHelper.create('', BracketsdrawerComponent, { record: this.record }).subscribe((res) => {
        console.log('back drawer response', res);
        this.msg.info(res);
      });
    }
  }

  close(): void {
    //   this.drawerRef.close(this.value);
  }
}
