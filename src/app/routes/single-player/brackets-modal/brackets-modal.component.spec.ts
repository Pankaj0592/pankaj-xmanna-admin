import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BracketsModalComponent } from './brackets-modal.component';

describe('BracketsModalComponent', () => {
  let component: BracketsModalComponent;
  let fixture: ComponentFixture<BracketsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BracketsModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BracketsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
