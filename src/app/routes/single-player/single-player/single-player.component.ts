import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DrawerHelper } from '@delon/theme';
import { ModalHelper } from '@delon/theme'; 
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { SinglePlayerService } from 'src/app/services/single-player/single-player.service';
import { SinglePlayerDrawerComponent } from '../single-player-drawer/single-player-drawer.component';
import { BracketsModalComponent } from '../brackets-modal/brackets-modal.component';
import { GamesManagementService } from 'src/app/services/games-management/games-management.service';
import { PlayOptionsService } from 'src/app/services/playOptions/play-options.service';
import { NzButtonSize } from 'ng-zorro-antd/button';


interface Challenge {
  _id: string;
  username: string;
  logo: string;
  game: string;
  fee: number;
  playOptionType: string;
  status: string;
  createdAt: string;
}

@Component({
  selector: 'app-single-player',
  templateUrl: './single-player.component.html',
  styleUrls: ['./single-player.component.less']
})
export class SinglePlayerComponent implements OnInit {


  q: any = {
    _id: '',
    name: '',
    genre: '',
    status: '',
    type: '',
    playOption: '',
    game: '',
  };
  
  duration = '24h';
  selectedRow: any;
  loading = false;
  filterBtnLoader = false;
  selectedIndex: number = 1;
  selectedTab: any = 'PvP';
  playOptionsData: any;
  gamesData: any;
  expandForm = false;
  size: any = '"default"';

  status = [{ index: 0, text: 'None', value: null, type: 'default', checked: false },
  { index: 1, text: 'Open', value: 'open', type: 'default', checked: false },
  { index: 2, text: 'Close', value: 'close', type: 'default', checked: false },
  { index: 3, text: 'Tie', value: 'tie', type: 'default', checked: false },
  { index: 4, text: 'Decline', value: 'decline', type: 'default', checked: false },
  { index: 5, text: 'Finished', value: 'finished', type: 'default', checked: false },
  { index: 6, text: 'Rematch', value: 'rematch', type: 'default', checked: false },
  { index: 7, text: 'Cancelled', value: 'cancelled', type: 'default', checked: false },
  { index: 8, text: 'Reported', value: 'reported', type: 'default', checked: false },
  { index: 9, text: 'Aborted', value: 'aborted', type: 'default', checked: false }];

  type = [
    { index: 0, text: 'None', value: null, type: 'default', checked: false },
    { index: 1, text: 'active', value: false, type: 'default', checked: false },
    { index: 2, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  genre = [
    { index: 0, text: 'active', value: false, type: 'default', checked: false },
    { index: 1, text: 'inactive', value: false, type: 'success', checked: false },
  ];
  sortByFilters = [
    { index: 0, label: 'Most Recent', value: '', type: 'default', checked: false },
    { index: 1, label: 'Challenge ID: 0 to 9', value: 'username', type: 'default', checked: false },
    { index: 2, label: 'Challenge ID: 9 to 0', value: '-username', type: 'default', checked: false },
  ];

  tabs = [ 'To Review', 'PvP', 'Brackets' ];

  total = 0;
  listOfChallenges: any;
  pageSize = 30;
  pageIndex = 1;
  sortField: any;
  sortOrder: any;
  filter: any;
  filterObj: any;
  selectedSort: any = null;
  pageReset = false;

  constructor(private challengeService: SinglePlayerService, public msg: NzMessageService, private fb: FormBuilder, private drawerHelper: DrawerHelper, private modalHelper: ModalHelper, private gameService: GamesManagementService, private playOptionService: PlayOptionsService) {}

  ngOnInit(): void {
    this.loading = false;
    this.fetchAllGames();
    this.fetchAllPlayOptions();
  }

  loadDataFromServer(
    pageIndex: number,
    pageSize: number,
    sortField: string | null,
    sortOrder: string | null,
    duration: string | '24h',
    // filter: any,
  ): void {
    this.loading = true;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.sortField = sortField;
    this.sortOrder = sortOrder;
    // this.filter = filter;
    this.duration = this.duration;
    this.challengeService.challengeAllList(pageIndex, pageSize, sortField, sortOrder, duration).subscribe((data: any) => {
      this.loading = false;
      console.log('challenges data', data);
      this.total = data.body.totalNumberOfChallenges; // mock the total data here
      this.listOfChallenges = data.body.challenges;
    }, (error) => {
      this.loading = false;
      console.log('error', error);
    });
  }

  onQueryParamsChange(params: NzTableQueryParams): void {
    console.log(params);
    if (this.pageReset) {
      this.pageReset = false;
      return;
      }
    const { pageSize, pageIndex, sort, filter } = params;
    const currentSort = sort.find(item => item.value !== null);
    const sortField = (currentSort && currentSort.key) || null;
    const sortOrder = (currentSort && currentSort.value) || null;
    if (!this.filterObj || this.filterObj == null){
      // this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, this.duration );
    } else{
      this.submitFilterForm(pageIndex, pageSize);
    }
    // this.loadDataFromServer(pageIndex, pageSize, sortField, sortOrder, this.duration);
  }



  public fetchAllGames(): void{
    this.gameService.getAllGamesAsList().subscribe((res: any) => {
      if (res.responseStatus.success){
        console.log('games data', res);
        // this.gamesData = res.body.gameData;
        this.gamesData = [];
        res.body.forEach((element: any) => {
          if (element.playerType === 'single'){
            this.gamesData.push(element);
          }
        });
        console.log('games data', this.gamesData);
      }
    }, (error) => {
      console.log('games data error', error);
    });
  }

  public fetchAllPlayOptions(): void{
    const obj = {'gameType': 'single'};
    this.playOptionService.getAllPlayOptionsAsList(obj).subscribe((res: any) =>{
      if (res.responseStatus.success){
        console.log('playOptions data', res);
        this.playOptionsData = res.body;
      }
    }, (error) => {
      console.log('playOption data error', error);
    });
  }


public rowClick(data: any): void{
  console.log('data', data);
  this.selectedRow = data._id;
  if (this.selectedTab === 'Brackets'){
    this.openBracketsDrawer(data);
  } else{
    this.drawerHelper.create('', SinglePlayerDrawerComponent, { record: data , menuSelectedTab : this.selectedTab}).subscribe((res) => {
      console.log('back drawer response', res);
      this.msg.info(res);
    });
  }
  }
  
  openBracketsDrawer(data: any): void{ 
    this.drawerHelper.create('', BracketsModalComponent, { record: data, menuSelectedTab : this.selectedTab },
    { size: 1060, footer: false, drawerOptions: {      
      nzMaskClosable: true,
      nzClosable: true,
    } }, ).subscribe(res => console.log('成功'));
  }

  public reload(): void {
    if (!this.filterObj || this.filterObj == null){
      // this.msg.info(JSON.stringify(this.sortField) + ',' + this.pageIndex + ',' + this.pageSize);
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, this.duration );
    } else{
      this.submitFilterForm(this.pageIndex, this.pageSize);
    }

  }

  submitFilterForm(pageIndex: number, pageSize: number): void {
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    console.log('filter data', this.q);
    const filter = this.q;
    for (let propName in filter) {
      if (
        filter[propName] === null ||
        filter[propName] === undefined ||
        filter[propName] === ""
      ) {
        delete filter[propName];
      }
    }
    console.log('filter data', filter);
    if (Object.entries(filter).length) {
      const timeDuration = {'duration': this.duration };
      const pageObj = {'page': pageIndex, 'resPerPage': pageSize};
      let formObj = { filter, ...timeDuration, ...pageObj };
      formObj ? this.filterObj = formObj : null;
      this.loading = true;
      this.filterBtnLoader = true;
      this.applyFilter(formObj);
    } else{
      return;
    }
  }

  applyFilter(formObj: any): void {
    this.loading = true;
    this.filterBtnLoader = false;
    this.challengeService.filterChallenge(formObj).subscribe((data: any) => {
      console.log('challenge filtered data', data);
      if (data.body.challenges.length){
        this.loading = false;
        this.filterBtnLoader = false;
        this.total = data.body.totalNumberOfChallenges; // mock the total data here
        console.log('filter data length', this.total);
        this.listOfChallenges = data.body.challenges;
      } else{
        this.loading = false;
        this.filterBtnLoader = false;
        this.msg.error('No data found through specified filter');
      }

    }, (error) => {
      this.loading = false;
      this.filterBtnLoader = false;
      console.log(error);
    });
  }


  selectTab(name: any): any{
    console.log('display content', name);
    this.selectedTab = name;
  }
  sortBy(event: any): void{
    
  }
  
  reset(): void {
    this.q = {};
    this.filterObj = null;
    this.pageReset = true;
    // this.pageIndex = 1;
    // this.pageSize = 30;
  }

  tableSize(value: any): void {
    console.log(value);
    this.size = value.toString();
  }


  /* tslint:disable-next-line:no-any */
  log(args: any[]): void {
    console.log(args);
  }

  sortByDuration(duration: any): void{
    console.log(duration);
    this.pageIndex = 1;
    this.pageSize = 30;
    if (!this.filterObj || this.filterObj == null){
      this.loadDataFromServer(this.pageIndex, this.pageSize, this.sortField, this.sortOrder, duration );
    } else{
      this.submitFilterForm(this.pageIndex, this.pageSize);
    }
  }
}
