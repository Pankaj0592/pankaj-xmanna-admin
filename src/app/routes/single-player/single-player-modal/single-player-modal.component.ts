import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-single-player-modal',
  templateUrl: './single-player-modal.component.html',
  styleUrls: ['./single-player-modal.component.less']
})
export class SinglePlayerModalComponent implements OnInit {

  @Input() subtitle?: string;
  @Input() statement?: string;
  @Input() data?: any;
  @Input() refund?: boolean;
  selectedPlayers:any;
  submit = false;
  balanceForm!: FormGroup;
  radioValue = 'A';
  constructor( private fb: FormBuilder, private modal: NzModalRef ) { }

  ngOnInit(): void {
     this.balanceForm = this.fb.group({
      player: [null, Validators.required],
      // comments: ['', Validators.required]
    });
  }

  bothPlayerSelected(bothPlayerValue: any): void{
    console.log('both player value', bothPlayerValue);
    if (Array.isArray(bothPlayerValue)){
      this.selectedPlayers = [];
      bothPlayerValue.forEach((player) => {
        console.log(player.playerId);
        this.selectedPlayers.push(player.playerId);
      });
    } else{
      this.selectedPlayers = null;
      this.selectedPlayers = bothPlayerValue;
    }
  }
  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void{
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    this.submit = true;
    const formObj = {
      player: this.selectedPlayers
    };
    console.log('report value', formObj);
  }
  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }
}
