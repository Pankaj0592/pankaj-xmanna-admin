import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglePlayerModalComponent } from './single-player-modal.component';

describe('SinglePlayerModalComponent', () => {
  let component: SinglePlayerModalComponent;
  let fixture: ComponentFixture<SinglePlayerModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SinglePlayerModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglePlayerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
