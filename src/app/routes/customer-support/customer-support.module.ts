import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { CustomerSupportRoutingModule } from './customer-support-routing.module';

const COMPONENTS: Type<void>[] = [];

@NgModule({
  imports: [
    SharedModule,
    CustomerSupportRoutingModule
  ],
  declarations: COMPONENTS,
})
export class CustomerSupportModule { }
