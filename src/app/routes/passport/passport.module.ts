import { NgModule } from '@angular/core';
import { SharedModule } from '@shared';
import { NzStepsModule } from 'ng-zorro-antd/steps';
import { CallbackComponent } from './callback.component';
import { UserLockComponent } from './lock/lock.component';
import { UserLoginComponent } from './login/login.component';
import { PassportRoutingModule } from './passport-routing.module';
import { UserRegisterResultComponent } from './register-result/register-result.component';
import { UserRegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './component/forgot-password/forgot-password.component';
// import { ValidateSecretComponent } from './component/validate-secret/validate-secret.component';
// import { ResetPasswordComponent } from './component/reset-password/reset-password.component';


const COMPONENTS = [UserLoginComponent, UserRegisterResultComponent, UserRegisterComponent, UserLockComponent, CallbackComponent];

@NgModule({
  imports: [SharedModule, PassportRoutingModule, NzStepsModule],
declarations: [...COMPONENTS, ForgotPasswordComponent,
  //  ValidateSecretComponent, ResetPasswordComponent
  ],
})
export class PassportModule {}
