import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { LoginService } from 'src/app/services/loginService/login.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less']
})
export class ResetPasswordComponent implements OnInit {

  @Input() data: any;
  confirmForm!: FormGroup;

  // tslint:disable-next-line: no-inferrable-types
  submit: boolean = false;
  passwordVisible = false;
  password?: string;

  constructor(private modal: NzModalRef, private fb: FormBuilder, private loginService: LoginService, private notification: NzNotificationService, private msg: NzMessageService) {}

  ngOnInit(): void{
    console.log('reset password', this.data);

    this.confirmForm = this.fb.group({
      password: ['', Validators.required],
      confirmPassword: ['', [this.confirmValidator ]],
    });
  }

  confirmValidator = (control: FormControl): any => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.confirmForm.controls.password.value) {
      return { confirm: true, error: true };
    }
  };

  validateConfirmPassword(): void {
    setTimeout(() => this.confirmForm.controls.confirmPassword.updateValueAndValidity());
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void{
    this.submit = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    const formObj = {
      username: this.data.username,
      password: formValue.password
    };
    console.log('reset password value', formObj);
    this.loginService.resetPassword(formObj).subscribe((res: any) => {
      console.log('reset password res', res);
      if (res.responseStatus){
        this.msg.success('Reset Successfully');
        this.destroyModal(formObj);
      }
    }, (error) => {
      this.msg.error(error.error.responseStatus.errorMessage);
    });
    // if (formObj) {
    //   this.destroyModal(formObj);
    // }
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }

}
