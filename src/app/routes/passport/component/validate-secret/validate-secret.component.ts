import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { LoginService } from 'src/app/services/loginService/login.service';

@Component({
  selector: 'app-validate-secret',
  templateUrl: './validate-secret.component.html',
  styleUrls: ['./validate-secret.component.less']
})
export class ValidateSecretComponent implements OnInit {

  @Input() data: any;
  resetForm!: FormGroup;
  // tslint:disable-next-line: no-inferrable-types
  submit: boolean = false;
  passwordVisible = false;
  password?: string;

  constructor(private modal: NzModalRef, private fb: FormBuilder, private loginService: LoginService, private notification: NzNotificationService, private msg: NzMessageService) {}

  ngOnInit(): void{
    console.log('data', this.data);
    this.resetForm = this.fb.group({
      secretKey: ['', Validators.required],
    });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save resetForm
   */

  save(isValid: any, formValue: any): void{
    this.submit = true;
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    const formObj = {
      username: this.data.username,
      secretKey: formValue.secretKey
    };
    console.log('validate secret formValue', formObj);
    this.loginService.validateSecretKey(formObj).subscribe((res: any) => {
      if (res.responseStatus.success){
      this.msg.success('Validate success');
      this.destroyModal(formObj);
      }
    }, (error) => {
      this.msg.error('Validate error', error.error.responseStatus.errorMessage);
    });
    // if (formObj) {
    //   this.destroyModal(formObj);
    // }
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }

}
