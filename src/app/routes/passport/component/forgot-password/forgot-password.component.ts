import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { LoginService } from 'src/app/services/loginService/login.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.less']
})
export class ForgotPasswordComponent implements OnInit {

  @Input() subtitle?: string;
  @Input() data?: any;
  submit = false;
  forgotPasswordForm!: FormGroup;
  
  constructor( private fb: FormBuilder, private modal: NzModalRef, private loginService: LoginService,
    
                private msg: NzMessageService, private notification: NzNotificationService) { }

  ngOnInit(): void {
    this.forgotPasswordForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: [''],
    });
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  save(isValid: any, formValue: any): void{
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    this.submit = true;
    const formObj = {
      email: formValue.email,
      resend: true,
    };
    console.log('forgot password form value', formObj);
    this.loginService.forgotPassword(formObj).subscribe((res: any) => {
      if (res.responseStatus.success){
        this.notification.success('Email sent',res.body.message);
        this.destroyModal(res.body);
      }
    }, (error) =>{
      console.log('error', error);
    });
  }

  destroyModal(obj: any): void {
    this.modal.destroy({ data : obj});
  }

  closeModal(): any{
    this.modal.destroy();
  }
}
