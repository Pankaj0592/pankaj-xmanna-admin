import { Component, Inject, OnDestroy, Optional, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { StartupService } from '@core';
import { ReuseTabService } from '@delon/abc/reuse-tab';
import { DA_SERVICE_TOKEN, ITokenService, SocialOpenType, SocialService } from '@delon/auth';
import { SettingsService, _HttpClient, ModalHelper } from '@delon/theme';
import { environment } from '@env/environment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzTabChangeEvent } from 'ng-zorro-antd/tabs';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
// import { ValidateSecretComponent } from '../component/validate-secret/validate-secret.component';
// import { ResetPasswordComponent } from '../component/reset-password/reset-password.component';
import { LoginService } from '../../../services/loginService/login.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ForgotPasswordComponent } from '../component/forgot-password/forgot-password.component';




@Component({
  selector: 'passport-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  providers: [SocialService],
})
export class UserLoginComponent implements OnDestroy, OnInit {

  loading = false;
  secretSubmit = false;
  submitConfirm = false;
  loginTab = true;
  secretKeyTabStatus = 'process';
  resetPassTabStatus = 'wait';
  returnUrl: any;

  constructor(
    fb: FormBuilder,
    // private modalHelper: ModalHelper,
    private loginService: LoginService,
    private modal: NzModalService,
    // private viewContainerRef: ViewContainerRef,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private socialService: SocialService,
    @Optional()
    @Inject(ReuseTabService)
    private reuseTabService: ReuseTabService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private startupSrv: StartupService,
    public http: _HttpClient,
    public msg: NzMessageService,
    public notification: NzNotificationService,
  ) {
    if (localStorage.getItem('auth_app_token')) {
      this.router.navigateByUrl('/dashboard');
    }
    this.form = fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required,]],
      mobile: [null, [Validators.required, Validators.pattern(/^1\d{10}$/)]],
      captcha: [null, [Validators.required]],
      remember: [true],
    });
    this.confirmForm = fb.group({
      password: ['', Validators.required],
      confirmPassword: ['', this.confirmValidator],
    });
    this.resetForm = fb.group({
      secretKey: ['', Validators.required],
    });
  }

  selectedIndex = 0;
  index = 0;
  current = 0;
  onIndexChange(event: number): void {
    this.index = event;
  }
  ngOnInit(): void {
    if (localStorage.getItem('auth_app_token')) {
      this.router.navigateByUrl('/dashboard');
    }
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/' || '';
    // console.log('returnurl', this.returnUrl);
  }
  // #region fields

  get userName(): AbstractControl {
    return this.form.controls.userName;
  }
  get password(): AbstractControl {
    return this.form.controls.password;
  }
  get mobile(): AbstractControl {
    return this.form.controls.mobile;
  }
  get captcha(): AbstractControl {
    return this.form.controls.captcha;
  }

  form: FormGroup;
  confirmForm: FormGroup;
  resetForm: FormGroup;
  error = '';
  type = 0;

  // #region get captcha

  count = 0;
  interval$: any;

  // #endregion

  switch({ index }: NzTabChangeEvent): void {
    console.log(index);
    this.type = index!;
  }

  getCaptcha(): void {
    if (this.mobile.invalid) {
      this.mobile.markAsDirty({ onlySelf: true });
      this.mobile.updateValueAndValidity({ onlySelf: true });
      return;
    }
    this.count = 59;
    this.interval$ = setInterval(() => {
      this.count -= 1;
      if (this.count <= 0) {
        clearInterval(this.interval$);
      }
    }, 1000);
  }

  // #endregion

  submit(): void {
    this.error = '';
    if (this.type === 0) {
      this.userName.markAsDirty();
      this.userName.updateValueAndValidity();
      this.password.markAsDirty();
      this.password.updateValueAndValidity();
      if (this.userName.invalid || this.password.invalid) {
        return;
      }
    } else {
      this.mobile.markAsDirty();
      this.mobile.updateValueAndValidity();
      this.captcha.markAsDirty();
      this.captcha.updateValueAndValidity();
      if (this.mobile.invalid || this.captcha.invalid) {
        return;
      }
    }

    // login submit

    const loginObj = {
      username: this.userName.value,
      password: this.password.value
    };
    console.log('login form obj', loginObj);
    localStorage.setItem('username', loginObj.username);
    this.loading = true;
    this.loginService.login(loginObj).subscribe((res: any) => {
      console.log('first time login res', res);
      if (res.responseStatus.success) {
        if (res.body.first) {
          this.notification.success('Email sent', res.body.message);
          this.loading = false;
          this.loginTab = false;
          // this.switchToValidationProcess();
        } else {
          localStorage.setItem('auth_app_token', res.body.token);
          const userData = JSON.stringify(res.body.admin);
          localStorage.setItem('userData', userData);
          this.router.navigateByUrl('/dashboard');
        }
      } else {

      }
    }, (error) => {
      this.loading = false;
      console.log('login error', error);
    });

    return;
    // In the default configuration, all HTTP requests are mandatory [Verification](https://ng-alain.com/auth/getting-started) User Token
    // Generally, login requests do not require verification, so you can add: `/login?_allow_anonymous=true` to the request URL, which means that user Token verification is not triggered
    this.http
      .post('/login/account?_allow_anonymous=true', {
        type: this.type,
        userName: this.userName.value,
        password: this.password.value,
      })
      .subscribe((res) => {
        if (res.msg !== 'ok') {
          this.error = res.msg;
          return;
        }
        // 清空路由复用信息
        this.reuseTabService.clear();
        // 设置用户Token信息
        // TODO: Mock expired value
        res.user.expired = +new Date() + 1000 * 60 * 5;
        this.tokenService.set(res.user);
        // 重新获取 StartupService 内容，我们始终认为应用信息一般都会受当前用户授权范围而影响
        this.startupSrv.load().then(() => {
          let url = this.tokenService.referrer!.url || '/';
          if (url.includes('/passport')) {
            url = '/';
          }
          this.router.navigateByUrl(url);
        });
      });
  }

  /** validate secret key form submission */

  /**
   *
   * @param isValid
   * @param formValue
   * to save resetForm
   */

  save(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    this.secretSubmit = true;
    const formObj = {
      username: this.userName.value,
      secretKey: formValue.secretKey
    };
    console.log('validate secret formValue', formObj);
    this.loginService.validateSecretKey(formObj).subscribe((res: any) => {
      if (res.responseStatus.success) {
        this.notification.success('Email sent', 'We’ve sent you an email with instructions to reset your password.');
        this.secretKeyTabStatus = 'finish';
        this.resetPassTabStatus = 'process';
        this.index = 1;
      }
    }, (error) => {
      console.log('validate error', error);
    });

  }



  confirmValidator = (control: FormControl): any => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.confirmForm.controls.password.value) {
      return { confirm: true, error: true };
    }
  };

  validateConfirmPassword(): void {
    setTimeout(() => this.confirmForm.controls.confirmPassword.updateValueAndValidity());
  }

  /**
   *
   * @param isValid
   * @param formValue
   * to save confirmForm
   */

  saveConfirm(isValid: any, formValue: any): void {
    console.log(`${isValid}, ${formValue}`);
    if (!isValid) {
      return;
    }
    this.submitConfirm = true;
    const formObj = {
      username: this.userName.value,
      password: formValue.password
    };
    console.log('reset password value', formObj);
    this.loginService.resetPassword(formObj).subscribe((res: any) => {
      console.log('reset password res', res);
      if (res.responseStatus) {
        this.notification.success('Reset password', 'Password reseted successfully.');
        this.secretKeyTabStatus = 'process';
        this.resetPassTabStatus = 'wait';
        this.loginTab = true;
        this.form.reset();
      }
    }, (error) => {
      console.log('error', error);
    });
  }
  backToLogin(): void {
    this.loginTab = true;
    this.index = 0;
    this.confirmForm.reset();
    this.resetForm.reset();
    this.secretKeyTabStatus = 'process';
    this.resetPassTabStatus = 'wait';
  }

  forgotPassword(): void {
    if (!this.userName.value) {
      this.msg.error('Please enter username first');
      return;
    }
    const modal = this.modal.warning({
      nzTitle: 'Forgot your password?',
      nzClosable: false,
      nzAutofocus: null,
      nzComponentParams: {
        data: this.userName.value,
        subtitle: 'Enter your email address and we’ll send you instrustions to reset password.'
      },
      nzContent: ForgotPasswordComponent,
    });
    modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    modal.afterClose.subscribe((result: any) => {
      console.log('[afterClose] The result is:', result.data);
      if (result.data) {
        console.log('[afterClose] The result is:', result);
        this.form.controls.password.reset();
        this.loginTab = false;
      }
    });
  }

  switchToValidationProcess(): void {

    // this.modalHelper.static( ValidateSecretComponent, { data: { title: 'Enter your secret key here!', username: this.userName.value }}, 'md').subscribe((res: any) => {
    //   console.log('valid secret modal back response', res);
    //   if (res) {
    //     /** open modal for reset password along with username */
    //     this.modalHelper.static( ResetPasswordComponent, { data: { title: 'Update your password here!', username: res.data.username }}, 'md').subscribe((result: any) => {
    //       console.log('valid secret modal back response', res);
    //       if (res){
    //         this.form.reset();
    //         this.router.navigateByUrl('');
    //       } else{
    //       this.msg.warning('Please complete login process');
    //       }
    //     });
    //   } else{
    //     this.msg.warning('Please complete login process');
    //   }
    // });



    // validate secret window open
    // const modal = this.modal.warning({
    //   // nzTitle: 'Modal Title',
    //   nzTitle: 'Enter your secret key here!',
    //   nzClosable: false,
    //   nzAutofocus: null,
    //   nzComponentParams: {
    //     subtitle: 'Please enter a valid secret-key.'
    //   },
    //   nzContent: ValidateSecretComponent,
    // });

    // modal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    // // Return a result when closed
    // modal.afterClose.subscribe((result) => {
    //   console.log('[afterClose] The result is:', result);
    //   if (result) {

    //     // open modal for update password
    //     const updatePasswordModal = this.modal.warning({
    //       nzTitle: 'Update your password here!',
    //       nzClosable: false,
    //       nzAutofocus: null,
    //       nzComponentParams: {
    //         subtitle: 'Please enter same password for both inputs!',
    //       },
    //       nzContent: ResetPasswordComponent,
    //     });
    //     updatePasswordModal.afterOpen.subscribe(() => console.log('[afterOpen] emitted!'));
    //     updatePasswordModal.afterClose.subscribe((res: any) => {
    //     console.log('[afterClose] The result is:', res);
    //     this.router.navigateByUrl('/');
    //   });

    //   }
    // });
  }

  // #region social

  open(type: string, openType: SocialOpenType = 'href'): void {
    let url = ``;
    let callback = ``;
    // tslint:disable-next-line: prefer-conditional-expression
    if (environment.production) {
      callback = 'https://ng-alain.github.io/ng-alain/#/passport/callback/' + type;
    } else {
      callback = 'http://localhost:4200/#/passport/callback/' + type;
    }
    switch (type) {
      case 'auth0':
        url = `//cipchk.auth0.com/login?client=8gcNydIDzGBYxzqV0Vm1CX_RXH-wsWo5&redirect_uri=${decodeURIComponent(callback)}`;
        break;
      case 'github':
        url = `//github.com/login/oauth/authorize?client_id=9d6baae4b04a23fcafa2&response_type=code&redirect_uri=${decodeURIComponent(
          callback,
        )}`;
        break;
      case 'weibo':
        url = `https://api.weibo.com/oauth2/authorize?client_id=1239507802&response_type=code&redirect_uri=${decodeURIComponent(callback)}`;
        break;
    }
    if (openType === 'window') {
      this.socialService
        .login(url, '/', {
          type: 'window',
        })
        .subscribe((res) => {
          if (res) {
            this.settingsService.setUser(res);
            this.router.navigateByUrl('/');
          }
        });
    } else {
      this.socialService.login(url, '/', {
        type: 'href',
      });
    }
  }

  // #endregion

  ngOnDestroy(): void {
    if (this.interval$) {
      clearInterval(this.interval$);
    }
  }
}
